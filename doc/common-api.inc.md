**Synopsis**: *Descriptions for the API's shared by the various tree and forest data types.*

## Type-Classes ##

```haskell
  class Refinable t where
    refine    :: (Element t -> Bool) -> t -> t
    subdivide :: t -> t

  class RefinableM t where
    refineM
      :: (Monad m, k ~ ContainerKey t, a ~ MapValue t)
      => (k -> a -> m [(k, a)]) -> t
      -> m t
```
