---
title: "The `tree-linear` Library"
author: Patrick Suggate <<patrick.suggate@gmail.com>>
date: 14 June, 2019
colorlinks: true
toc: true
toc-depth: 3
toccolor: purple
fontsize: 11pt
lof: true
biblio-title: Bibliography
---

\clearpage

# Introduction #

Linear $2^d$-trees which generalises the work on *linear quadtrees* [@gargantini1982effective].

## Exposed API Functions ##

Most of this stuff is covered in `doc/common-api.inc.md` of the `pforest` package. Briefly:

1. set-like and map-like operations;

2. refinement operations;

3. adjacency queries;

\clearpage

# Data Types and Representations #

## Linear $2^d$-Tree Representations ##

Linear $2^d-$tree implemented as a set of leaf-cell Z-indices:

```haskell
  newtype Ltree (d :: Nat)   = Ltree { _leaves :: IntSet }
```

And when the leaf-nodes contain data:

```haskell
  newtype Xtree (d :: Nat) a = Xtree { _xeaves :: IntMap a }
```

Indices into these structures are represented as:

```haskell
  newtype Key (d :: Nat) = Key { _key :: Int }
    deriving (Eq, Ord, Enum, Bounded, Num, Real, Integral, Read, Show, Generic)
```

## Linear $2^d$-Forest Representations ##

Linear $2^d-$forest implemented as a set of leaf-cell Z-indices:

```haskell
  newtype Lforest (d :: Nat)   =
    Lforest { _ltrees :: IntMap (Ltree d) }
```

And when the leaf-nodes contain data:

```haskell
  newtype Xforest (d :: Nat) a =
    Xforest { _xtrees :: IntMap (Xtree d a) }
```

Indices into these structures are represented as:

```haskell
  data Fkey (d :: Nat) = Fkey { _ktree :: !TreeID, _kindx :: !Int64 }
    deriving (Eq, Ord, Show, Generic)

  newtype TreeID = TreeID { _unTreeID :: Int32 }
    deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic, Num, Real,
              Integral, Storable)
```

<!--
\clearpage
-->

# Common API #

!include common-api.inc.md

\clearpage

# Appendix: Z-Index Representation # {#app:z-repr}

In the following diagram:

```
    63 <-- l*d -->            0
    +-+-+-     -+-+-+-+---- -+-+
    |0|    ...    |1|0 ...    0|
    +-+-+-     -+-+-+-+---- -+-+
```

so there are `l` groups of child-indices, where `l` is the *'level'* of the cell, and `d` is the *number of parametric dimensions*.

\clearpage

# Bibliography #
