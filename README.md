---
title: "README for Linear $2^d-$Trees"
author: Patrick Suggate <<patrick.suggate@gmail.com>>
date: 9 November, 2018
geometry: margin=2cm
colorlinks: true
toc: true
toc-depth: 3
toccolor: purple
lof: true
biblio-title: Bibliography
---

# Package: tree-linear #

**OBSOLETE**: Moved into the '`forest-meshes`' mono-repo.

Linear tree data structures and functions

## TODO ##

 * update documentation, and add diagrams showing the functionality of each of the operators;
 * *testing* framework
 * support for 3D (and 4D?)
 * parallelism
 * `bootstrap.sh` script that installs all dependencies
 * forests of linear $2^d-$trees
 * clean out all of the **OBSOLETE** code & dependencies

## Documentation ##

 * modify `pandoc` (or `cmark-gfm`, ...) to produce suitable `TeX` for inclusion into the PhD thesis document
 * equations

## Regression Tests ##

Run the testbenches by issuing a `make test`; i.e.,

```bash
$ make test
```

which should yield output that is similar to that shown in Figure @fig:regression-tests.

![The expected regression test output should look a bit like this, if all tests are successful.](doc/images/regression-tests.png "Regression test output"){#fig:regression-tests}

# Simple Example #

From the `tree-linear` root directory:

```bash
$ cabal new-repl -j
```

followed by

```Haskell
> :load Data/Mesh/Adaptive/InterpFn.hs
> testInterpFn
> :load Data/Mesh/Adaptive/Import.hs
> testImport
```

and this will generate an image file of the mesh, `out/escher.svg` (see Figure @fig:escher).

![Mesh.](doc/images/escher.pdf "Mesh"){#fig:escher}

The generated mesh satisfies 2:1 balance and the *"area rule"* -- these constraints reduce *"fan-out"*, increasing sparsity, when the mesh is used to numerically solve a system of equations, corresponding to the discretised PDE's of a *Finite Element Method* formulation.

---

# API Overview #

+ `Ltree`{.haskell} -- Top-level module.

+ `Ltree.Base`{.haskell} -- Core data-types and functions for building, querying, and modifying `Ltree`s.

+ `Ltree.Coords`{.haskell} -- Conversions between `Ltree` indices and coordinates, and query-functions using coordinates.

+ `Ltree.Point`{.haskell} -- Functions for working with the nodes/points of a `Ltree`.

+ `Ltree.Export`{.haskell} -- `SVG` exporters for `Ltree` objects.

+ `Ltree.Stream`{.haskell} -- Iterators for `Ltree`s that can potentially benefit from stream-fusion^[Definitely not finished, and this module needs a lot of work -- if just to decide whether streams are sensible.].

## `Ltree` Data Types ##

```Haskell
-- | Newtype for a linear $2^d-$tree implementation.
newtype Ltree (d :: Nat)   = Ltree { _getSet :: IntSet }
  deriving ( Eq, Ord, Read, Show, Monoid, Semigroup
           , SetOps, Subset, SetBuild Int, SetIndex Int, SetInsert Int
           , SetFlat Int )
```

## Smart Constructors ##

```Haskell
-- | Construct a uniform(-height) linear quadtree.
uniform :: Int -> Ltree 2
uniform h = let (w, js) = (2^h-1, [0..w])
            in  Ltree $ Set.fromList [topath h (V2 i j) | i<-js, j<-js]

-- | Mesh where the cells get smaller towards the boundary
escher :: Int -> Ltree 2
escher d =
  let es = [V2 i j | i<-[0..l], j<-[0..l], i==0 || i==l || j==0 || j==l]
      l  = 2^d-1
  in  arearule . Ltree . Set.fromList $ topath d <$> es

-- | Construct a linear quadtree with the lower left corner refined by the
--   given number of times.
corner :: Int -> Ltree 2
corner  = arearule . unit . tkey2 . V3 0 0
```

## Queries ##

```Haskell
-- | Checks to see if the given linear-tree completely covers the $[0,1]^d$
--   domain.
isComplete :: IntSet -> Bool
isComplete lt = go 1 where
  go !k | k  <?  lt = True
        | otherwise = and $ go <$> progeny k
```

## Filters ##

Linear $2^d-$trees can be *"filtered"* to keep/remove leaves that have certain properties. For example, to keep or remove *"inferred elements"* -- an *inferred element* is one that contains one or more *"inferred nodes"* (or *"hanging nodes"*, as these are called in the literature, in the context of meshes for function spaces that use *interpolating basis functions*).

```Haskell
-- | Remove all "inferred elements" from the tree.
noinferred :: Ltree 2 -> Ltree 2
noinferred lt = getSet %~ Set.filter (flip isDofPatch lt) $ lt

-- | Keep only the "inferred elements," of the given tree.
inferred :: Ltree 2 -> Ltree 2
inferred (Ltree t) = Ltree $ Set.filter (\b -> not . Set.null $ aunties b /\ t) t
```

The listings above show how to traverse a linear $2^d$ tree, while applying a keep/drop filter.

## Coherence Constraints & Tree Balance ##

The 2:1 balance constraint can be enforce at corners, along edges, or across faces (in 3D). The following implementation enforces 2:1 balance at the tree-leaf *corners*, which is the most restrictive version:

```Haskell
-- | Rebalance a tree so that it satisfies the 2:1 balance constraint.
--   NOTE: Inspired by `Algorithm 2.2`, pp.15 , from "A Distributed Memory
--     Fast Multipole Method for Volume Potentials", by Malhotra & Biros. But
--     unlike their pseudocode version, there are no non-leaf nodes referred
--     to, or required by this algorithm.
balance :: Ltree 2 -> Ltree 2
balance s =
  let go 0 _ _ b = Ltree b
      go i p t b =
        let (u, r) = Set.partition ((==i) . plen) $ _getSet t
            x  = p \/ inmates u
            b' = b \/ x
            t' = Ltree $ r \\ ancestors x
            p' = support x \\ ancestors b'
        in  go (i-1) p' t' b'
  in  go (depth s) bare s bare
```

But even 2:1 balance is not satisfactory, because this can still lead to very high *"fanouts"* when used for *IsoGeometric Analysis* (IGA). For example, when the B-spline basis functions are bicubics; i.e., their polynomial degree is three, $p = 3$ . A stricter constraint is to enforce the condition that every *"DoF node"* belongs to a *"DoF cell"* -- and a DoF cell is one where every node is a DoF node. We call this the *"area rule"*, as this requires each DoF node to be associated with some "area" enclosed by DoF nodes and edges between these. An implementation of the area rule (in 2D) is given below:

```Haskell
-- | Enforce both 2:1 balance and "area rule" constraints, while rebuilding
--   the given linear tree.
arearule :: Ltree 2 -> Ltree 2
arearule s =
  let go 0 _ _ b = Ltree b
      go i p t b =
        let (u, r) = Set.partition ((==i) . plen) $ _getSet t
            x  = inmates (peers p \/ u) \\ ancestors b
            b' = b \/ x
            t' = Ltree $ r \\ ancestors x
            p' = (peers . parents) x \\ ancestors b'
        in  go (i-1) p' t' b'
  in  go (depth s) bare s bare
```

# Import/Export #

## SVG ##

Figure @fig:escher shows the output from the SVG exporter.

## Exodus II ##

Figure @fig:arearule shows an example of Exodus II export, after being imported into *ParaView* for visualisation.

![Paraview showing an imported Exodus II representation of a linear $2^d-$tree that satisfies the *area rule* constraint.](doc/images/arearule.png "Paraview visualisation of an Exodus II representation of a linear $2^d-$tree"){#fig:arearule}

# Testbenches #

Under the subdirectory `test/`, there are a few (regression- & unit-) tests for this library. To run the tests:

```bash
$ cabal new-build -j
$ dist-newstyle/build/x86_64-linux/ghc-8.6.1/tree-linear-0.1.0.0/t/tree-test/build/tree-test/tree-test
```

# Bibliography #
