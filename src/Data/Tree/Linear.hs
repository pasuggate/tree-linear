------------------------------------------------------------------------------
-- |
-- Module      : Data.Tree.Linear
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Top-level module for linear 2^d-trees.
-- 
-- Changelog:
--  + 21/10/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Tree.Linear
  ( module Data.Index.ZCurve
  , module Data.Tree.Linear.Coords
  -- tree type-classes
  , Refinable (..)
  -- tree data types:
  , Ltree (..)
  -- constructors:
  , escher, bescher
  , corner, bcorner
  , centre, bcentre
  , uniform
  , fromList
  -- modifiers:
  , complete, balanceE, balance, arearule
  , inferred, noinferred
  -- queries:
  , isInferred
  , isDofPatch
  , isComplete
  , odds
  ) where

import Data.Index.ZCurve hiding (maxDepth2, maxWidth2)
import Data.Tree.Linear.Base
import Data.Tree.Linear.Coords
import Data.Tree.Linear.Point
-- import Data.Tree.Linear.Stream
