{-# LANGUAGE OverloadedStrings, DataKinds, FlexibleContexts #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.Tree.Linear.Export
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- SVG exporters for meshes.
-- 
-- TODO:
--  + refactor this into more modular subunits;
-- 
------------------------------------------------------------------------------

module Data.Tree.Linear.Export
  ( fromLtree
  , fromLtree'
  , fromLtreeBG
  -- * Construction helpers
  , ltreeToSVG
  , ltreesToSVG
  , doubleLtreeToSVG
  , tripleLtreeToSVG
  , renderToFile
  -- * Ltree export properties
  , colour
  , shade
  , scl
  , txt
  , svgSize
  ) where

-- import GHC.TypeNats
import Control.Lens hiding (outside)
import Data.Text as Text (Text, pack)
import Data.IntSet.Sets
import Linear
import Text.Printf
import Graphics.Svg

import Data.Bits.Handy
import Data.Tree.Linear as Ltree


-- * Convenience type-aliases
------------------------------------------------------------------------------
type Z = Int
-- type R = Double


-- * SVG generators
------------------------------------------------------------------------------
fromLtree :: Ltree 2 -> Element
fromLtree  = fromLtreeBG defColourFun 0

fromLtreeBG :: (V3 Z -> Text) -> V2 Z -> Ltree 2 -> Element
fromLtreeBG colourFun hv@(V2 h v) = maybe bg (bg <>) . fromLtree' colourFun hv
  where
    bg = rect_ [ X_ <<- txt (h+1), Y_ <<- txt (v+1), Width_ <<- txt 1022
               , Height_ <<- txt 1022, Stroke_ <<- "black"
               , Stroke_width_ <<- "1", Fill_ <<- "none" ]

fromLtree' :: (V3 Z -> Text) -> V2 Z -> Ltree 2 -> Maybe Element
fromLtree' colourFun (V2 h v) lt
  | dry lt    = Nothing
  | otherwise = Just $ mconcat cs
  where
    cs = go . scl s . lpath <$> flat lt
    s  = maxDepth (undefined :: Ltree 2) - 10
    go (V3 j i d) =
      let w = 1 <<% (10 - d)
      in  rect_ [ X_ <<- txt (h+j+1), Y_ <<- txt (v+1024-i-w+1)
                , Width_ <<- txt (w-2), Height_ <<- txt (w-2)
                , Stroke_ <<- "black", Stroke_width_ <<- "1"
                , Fill_ <<- colourFun (V3 j i d)]
--                 , Fill_ <<- shade d]

-- NOTE: (0,0) is the top, left corner.
ltreeToSVG :: Ltree 2 -> Element
-- ltreeToSVG  = svgSize 1024 1024 . fromLtree
ltreeToSVG  = svgSize 1536 1024 . mappend pic . fromLtreeBG defColourFun (V2 256 0) where
  pic = rect_ [ Width_ <<- "100%", Height_ <<- "100%", "white" ->> Fill_]

doubleLtreeToSVG :: Ltree 2 -> Ltree 2 -> Element
doubleLtreeToSVG t0 t1 =
  let e0 = fromLtreeBG defColourFun (V2   32 0) t0
      e1 = fromLtreeBG defColourFun (V2 1088 0) t1
      bg = rect_ [ Width_ <<- "100%", Height_ <<- "100%", "white" ->> Fill_]
  in  svgSize 2144 1024 $ bg <> e1 <> e0

tripleLtreeToSVG :: Ltree 2 -> Ltree 2 -> Ltree 2 -> Element
tripleLtreeToSVG t0 t1 t2 =
  let e0 = fromLtreeBG defColourFun (p0!!0) t0
      e1 = fromLtreeBG defColourFun (p0!!1) t1
      e2 = fromLtreeBG defColourFun (p0!!2) t2
      bg = rect_ [ Width_ <<- "100%", Height_ <<- "100%", "white" ->> Fill_]
      (w, s) = (1024, 64)
      p0 = take 4 [V2 x 0 | x<-[s,(w+2*s)..]]
  in  svgSize (last p0^._x) w $ bg <> e2 <> e1 <> e0

ltreesToSVG :: Z -> Z -> [Ltree 2] -> Element
ltreesToSVG n m ts =
  let es = reverse $ zipWith (fromLtreeBG defColourFun) ps ts
      (w , s ) = (1024, 64)
      (xs, ys) = (take m [s,(w+2*s)..], take n [0,(w+s)..])
      ps = [V2 x y | y<-ys, x<-xs]
      bg = rect_ [ Width_ <<- "100%", Height_ <<- "100%", "white" ->> Fill_]
--   in  bg <> mconcat es
  in  svgSize (last xs + w+s) (last ys + w) $ bg <> mconcat es


-- * Helper functions
------------------------------------------------------------------------------
-- | Realise an integer.
scl :: Z -> V3 Z -> V3 Z
scl r = _xy %~ fmap (>>% r)
{-# INLINE scl #-}

txt :: Z -> Text
txt  = Text.pack . show
{-# INLINE txt #-}

defColourFun :: V3 Z -> Text
defColourFun  = colour . (^._z)

shade :: Z -> Text
shade  = pack . go where
  go x = let s = 255 - 16*x in printf "#%02x%02x%02x" s s s

colour :: Z -> Text
colour 0 = "black"
colour 1 = "#a52a2a" -- "brown"
-- colour 2 = "#800080" -- "purple"
colour 2 = "#901090"
-- colour 3 = "#0000ff" -- "blue"
colour 3 = "#2828ff"
colour 4 = "#008000" -- "green"
-- colour 5 = "#ffff00" -- "yellow"
colour 5 = "#e0e000"
colour 6 = "orange"
colour 7 = "pink"
colour 8 = "red"
colour 9 = "white"
-- colour 10 = "white"
colour 10 = "olive"
colour i = colour $ pred i `mod` 10 + 1
-- colour i = error $ "colour: unknown " ++ show i

------------------------------------------------------------------------------
-- | Set the document size
svgSize :: Z -> Z -> Element -> Element
svgSize w h content
  = doctype <> with (svg11_ content) [ Version_ <<- "1.1"
                                     , Width_   <<- pack (printf "%d" w)
                                     , Height_  <<- pack (printf "%d" h)
                                     ]
