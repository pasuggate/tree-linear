{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FunctionalDependencies,
FlexibleContexts, FlexibleInstances, TypeSynonymInstances, InstanceSigs,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
BangPatterns, ViewPatterns, PatternSynonyms, TupleSections,
StandaloneDeriving, ScopedTypeVariables, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Tree.Linear.Coords
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Coordinate functions for the (cell-)nodes of linear 2^d-trees.
-- 
-- Changelog:
--  + 21/10/2018  --  initial file;
-- 
-- TODO:
--  + generalise to trees of 1, 2, 3, and 4 dimensions;
-- 
------------------------------------------------------------------------------

module Data.Tree.Linear.Coords
       ( BoundaryTestable (..)
       , Translatable (..)
       -- , maxDepth -- , maxWidth
       , lpos2, tkey2, lpos3, tkey3
       , belongs, insides
       ) where

import Data.Foldable (toList)
import Data.Index.ZCurve
import Linear (V3 (..), V4 (..)) -- V2 (..)


-- * Type-classes for working with coordinates
------------------------------------------------------------------------------
class BoundaryTestable t where
  -- | Does the given node-coordinate lie on the boundary, or outside, of the
  --   domain?
  ghost   :: t -> Bool
  ghost    = not . inside
  -- | Does the given node-coordinate lie on the boundary of the domain?
  border  :: t -> Bool
  border x = ghost x && closure x
  -- | Does the given node-coordinate lie strictly inside the boundary?
  inside  :: t -> Bool
  inside   = not . ghost
  -- | Does the given node-coordinate lie strictly outside the boundary?
  outside :: t -> Bool
  outside  = not . closure
  -- | Within the "closure" of the domain?
  closure :: t -> Bool
  closure  = not . outside

------------------------------------------------------------------------------
-- | For objects cabable of being translated, and in the indicated direction.
class Translatable a where
  type InDir a :: *
  translate :: a -> InDir a -> Maybe a


-- * Instances for working with coordinates
------------------------------------------------------------------------------
{-- }
instance BoundaryTestable (V4 Int) where
  ghost   (V4 x y z _) =
    x<=0 || x>=maxWidth || y<=0 || y>=maxWidth || z<=0 || z>=maxWidth
  border  (V4 x y z _) =
    x==0 || x==maxWidth || y==0 || y==maxWidth || z==0 || z==maxWidth
  inside  (V4 x y z _) =
    x> 0 && x <maxWidth && y> 0 && y <maxWidth && z> 0 && z <maxWidth
  outside (V4 x y z _) =
    x <0 || x> maxWidth || y <0 || y> maxWidth || z <0 || z> maxWidth
  closure (V4 x y z _) =
    x>=0 || x<=maxWidth || y>=0 || y<=maxWidth || z>=0 || z<=maxWidth
  {-# INLINE ghost   #-}
  {-# INLINE border  #-}
  {-# INLINE inside  #-}
  {-# INLINE outside #-}
  {-# INLINE closure #-}
--}

instance BoundaryTestable (V3 Int) where
  ghost   (V3 x y _) = x<=0 || x>=maxWidth2 || y<=0 || y>=maxWidth2
  border  (V3 x y _) = x==0 || x==maxWidth2 || y==0 || y==maxWidth2
  inside  (V3 x y _) = x> 0 && x <maxWidth2 && y> 0 && y <maxWidth2
  outside (V3 x y _) = x <0 || x> maxWidth2 || y <0 || y> maxWidth2
  closure (V3 x y _) = x>=0 || x<=maxWidth2 || y>=0 || y<=maxWidth2
  {-# INLINE ghost   #-}
  {-# INLINE border  #-}
  {-# INLINE inside  #-}
  {-# INLINE outside #-}
  {-# INLINE closure #-}

{-- }
instance BoundaryTestable (V2 Int) where
  ghost   (V2 x _) = x<=0 || x>=maxWidth1
  border  (V2 x _) = x==0 || x==maxWidth1
  inside  (V2 x _) = x> 0 && x <maxWidth1
  outside (V2 x _) = x <0 || x> maxWidth1
  closure (V2 x _) = x>=0 || x<=maxWidth1
  {-# INLINE ghost   #-}
  {-# INLINE border  #-}
  {-# INLINE inside  #-}
  {-# INLINE outside #-}
  {-# INLINE closure #-}
--}


-- * Helper & Internal functions.
-- ** Coordinate <-> Key helper-functions
------------------------------------------------------------------------------
-- | Convert between 3D keys & coordinates.
--   TODO:
lpos3 :: Key 3 -> V4 Int
lpos3  = undefined -- lpath
{-# INLINE [2] lpos3 #-}

-- TODO:
tkey3 :: V4 Int -> Key 3
tkey3  = undefined -- reify
{-# INLINE [2] tkey3 #-}

------------------------------------------------------------------------------
-- | Convert between 2D keys & coordinates.
lpos2 :: Int -> V3 Int
lpos2  = lpath . Key
{-# INLINE [2] lpos2 #-}

tkey2 :: V3 Int -> Int
tkey2  = _key . reify
{-# INLINE [2] tkey2 #-}

-- ** Queries
------------------------------------------------------------------------------
-- | Test to see if each coordinate is within the domain.
belongs :: forall f. (HasWidth (f Int), Foldable f) => f Int -> [Bool]
belongs  = map go . init . toList where
  go x = x>=0 && x<=maxWidth (undefined :: f Int)
{-# INLINE belongs #-}

-- | Test to see if each coordinate is _strictly_ within the domain.
insides :: forall f. (HasWidth (f Int), Foldable f) => f Int -> [Bool]
insides  = map go . init . toList where
  go x = x>0 && x<maxWidth (undefined :: f Int)
{-# INLINE insides #-}
