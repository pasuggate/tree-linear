{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, RankNTypes,
             TypeOperators, MultiParamTypeClasses, FlexibleContexts, GADTs,
             FlexibleInstances, DeriveGeneric, DeriveFunctor, DeriveFoldable,
             DeriveTraversable, GeneralizedNewtypeDeriving, BangPatterns,
             StandaloneDeriving, TypeSynonymInstances, InstanceSigs,
             TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Tree.Linear.Base
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Lazy-list based paths through linear (2^d-)trees
-- 
-- Changelog:
--  + 24/06/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Tree.Linear.Base
  ( module Data.Index.ZCurve
  , module Data.Tree.Linear.Coords
  -- * Data types and accessors
  , Ltree (..)
  , leaves
  , Refinable (..)
  -- * Constructors
  , uniform
  , escher, bescher
  , corner, bcorner
  , centre, bcentre
  , fromList
  , saveLtree
  , loadLtree
  -- * Queries
  , ancestors
  , children
  , offspring
  , parents
  , peers
  , touches
  , aunties
  , edgeNbrs
  , edgePeers
  -- * Traversals
  , partition
  , filter
  -- * Modification
  , cellmates
  , inmates
  , extension
  -- * Coherency
  , balance
  , balanceE
  , arearule
  , complete
  , inferred
  , noinferred
  -- ** Coherency queries
  , isInferred
  , isDofPatch
  , isComplete
  -- * Miscellaneous
  , showTree
  , testBase
  ) where

import Prelude hiding (filter)
import GHC.Generics (Generic)
import GHC.TypeNats
import Control.Arrow ((<<<), (***), (&&&))
import Control.Lens (makeLenses)
import Control.Monad.IO.Class
import Control.DeepSeq
import Data.Foldable
import Data.MonoTraversable
import Data.Containers
import Data.List (foldl1')
import qualified Data.List as List
import Data.Bits.Extra ((<<%), (>>%))
import Data.IntSet.Sets as Set
import qualified Data.IntSet as Set
-- import Data.IntMap.Maps as Map
import Data.Aeson
import qualified Data.ByteString.Lazy as Lazy
import Linear (V2 (..), V3 (..)) -- , V4 (..))

import Data.Index.ZCurve
import Data.Tree.Linear.Coords


-- * Linear 2^d type-classes
------------------------------------------------------------------------------
class Refinable t where
  refine    :: (Element t -> Bool) -> t -> t
  subdivide :: t -> t


-- * Demonstration data-types
------------------------------------------------------------------------------
-- | Linear $2^d-$tree implemented as a set of leaf-cell Z-indices.
newtype Ltree (d :: Nat)   = Ltree { _leaves :: IntSet }
  deriving (Eq, Ord, Monoid, Semigroup, Generic)

makeLenses ''Ltree

deriving instance NFData (Ltree d)


-- * And some instances
------------------------------------------------------------------------------
instance Layered (Ltree 2) where
  depth :: Ltree 2 -> Int
  depth  = Set.foldl' (\d -> max d . plen2) 0 . _leaves
  maxDepth _ = maxDepth (undefined :: Key 2)
  {-# INLINE maxDepth #-}

{-- }
-- TODO:
instance Layered (Ltree 3) where
  depth :: Ltree 3 -> Int
--   depth  = depth . lpos3 . Set.findMax . ((-1)~>) . _leaves
  depth  = Set.foldl' (\d -> max d . plen2) 0 . _leaves
  maxDepth _ = maxDepth (undefined :: Key 3)
  {-# INLINE maxDepth #-}
--}

------------------------------------------------------------------------------
instance Refinable (Ltree 2) where
  refine    = refine2
  subdivide = subdivide2
  {-# INLINE refine    #-}
  {-# INLINE subdivide #-}

-- ** Mono-traversable instances
------------------------------------------------------------------------------
type instance Element (Ltree d) = Key d

instance MonoFunctor (Ltree d) where
  omap :: (Key d -> Key d) -> Ltree d -> Ltree d
  omap f = foldl' (flip (~>)) bare . fmap f . flat

instance MonoFoldable (Ltree d) where
  ofoldMap f = foldMap f . otoList where
  ofoldr  f s = foldr  f s . otoList
  ofoldl' f s = foldl' f s . otoList
  otoList = flat
  onull   = dry
  olength = card
  ofoldr1Ex  f = foldr1 f . otoList
  ofoldl1Ex' f = uncurry (foldl' f) . (head &&& tail) . otoList
  {-# INLINE ofoldMap #-}
  {-# INLINE ofoldr   #-}
  {-# INLINE ofoldl'  #-}
  {-# INLINE otoList  #-}
  {-# INLINE onull    #-}
  {-# INLINE olength  #-}

instance GrowingAppend   (Ltree d)

-- ** DataContainers instances
------------------------------------------------------------------------------
instance SetContainer (Ltree d) where
  type ContainerKey (Ltree d) = Key d
  member    = (<?)
  notMember = (>?)
  keys      = fmap Key . Set.toList . _leaves
  Ltree ls `union`        Ltree ys = Ltree $ ls `union`        ys
  Ltree ls `difference`   Ltree ys = Ltree $ ls `difference`   ys
  Ltree ls `intersection` Ltree ys = Ltree $ ls `intersection` ys
  {-# INLINE member       #-}
  {-# INLINE notMember    #-}
  {-# INLINE union        #-}
  {-# INLINE difference   #-}
  {-# INLINE intersection #-}
  {-# INLINE keys         #-}

-- ** JSON import/export
------------------------------------------------------------------------------
instance ToJSON   (Ltree 2)
instance FromJSON (Ltree 2)

instance ToJSON   (Ltree 3)
instance FromJSON (Ltree 3)

-- ** Set-line instances
------------------------------------------------------------------------------
deriving instance SetOps  (Ltree d)
deriving instance SetSize (Ltree d)

instance SetInsert (Key d) (Ltree d) where
  Key k ~> Ltree ls = Ltree $ k ~> ls
  {-# INLINE (~>) #-}

instance SetIndex (Key d) (Ltree d) where
  Key k <? Ltree ls = k <? ls
  Key k <\ Ltree ls = Ltree $ k <\ ls
  {-# INLINE (<?) #-}
  {-# INLINE (<\) #-}

instance SetBuild (Key d) (Ltree d) where
  bare = Ltree bare
  unit = Ltree . unit . _key
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetFlat (Key d) (Ltree d) where
  flat = fmap Key . flat . _leaves
  {-# INLINE flat #-}


-- ** Dump to text output
------------------------------------------------------------------------------
instance Show (Ltree 2) where
  show = showTree
  {-# INLINE show #-}

showTree :: Ltree 2 -> String
showTree (Ltree ls) = "Ltree {_leaves = " ++ show (lpos2 <$> flat ls) ++ "}"


-- * Constructors
------------------------------------------------------------------------------
-- | Construct a uniform(-height) linear quadtree.
uniform :: Int -> Ltree 2
uniform h = let (w, js) = (2^h-1, [0..w])
            in  Ltree $ Set.fromList [topath2 h (V2 i j) | i<-js, j<-js]

------------------------------------------------------------------------------
-- | Mesh where the cells get smaller towards the boundary
escher :: Int -> Ltree 2
escher d =
  let es = [V2 i j | i<-[0..l], j<-[0..l], i==0 || i==l || j==0 || j==l]
      l  = 2^d-1
  in  arearule . Ltree . Set.fromList $ topath2 d <$> es

-- | Mesh where the cells get smaller towards the boundary
bescher :: Int -> Ltree 2
bescher d =
  let es = [V2 i j | i<-[0..l], j<-[0..l], i==0 || i==l || j==0 || j==l]
      l  = 2^d-1
  in  balance . Ltree . Set.fromList $ topath2 d <$> es

------------------------------------------------------------------------------
-- | Construct a linear quadtree with the lower left corner refined by the
--   given number of times.
corner :: Int -> Ltree 2
corner  = arearule . unit . reify . V3 0 0

-- | Construct a linear quadtree with the lower left corner refined by the
--   given number of times.
bcorner :: Int -> Ltree 2
bcorner  = balance . unit . reify . V3 0 0

------------------------------------------------------------------------------
-- | Construct a linear quadtree with the centre of the mesh having refinement
--   depth given as input.
centre :: Int -> Ltree 2
centre d =
  let (c, s) = (1 <<% pred maxDepth2, 1 <<% (maxDepth2 - d))
      ps = [V3 x y d | y<-[c-s,c], x<-[c-s,c]]
  in  arearule . fromList $ fmap reify ps

-- | Construct a linear quadtree with the centre of the mesh having refinement
--   depth given as input.
bcentre :: Int -> Ltree 2
bcentre d =
  let (c, s) = (1 <<% pred maxDepth2, 1 <<% (maxDepth2 - d))
      ps = [V3 x y d | y<-[c-s,c], x<-[c-s,c]]
  in  balance . fromList $ fmap reify ps

------------------------------------------------------------------------------
-- | Build a linear quadtree from a list of leaves.
fromList :: [Key d] -> Ltree d
fromList  = Ltree . Set.fromList . fmap _key
{-# INLINE fromList #-}

-- ** JSON save/load
------------------------------------------------------------------------------
saveLtree :: (MonadIO m, ToJSON (Ltree d)) =>
  FilePath -> Ltree d -> m ()
saveLtree fp = liftIO . Lazy.writeFile fp . encode

loadLtree :: (MonadIO m, FromJSON (Ltree d)) =>
  FilePath -> m (Maybe (Ltree d))
loadLtree  = liftIO . fmap decode . Lazy.readFile


-- * Queries
------------------------------------------------------------------------------
-- | Compute all of the ancestors for all of the given nodes.
ancestors :: Ltree 2 -> Ltree 2
ancestors  = foldl' (\js -> go js . reverse . lineage) bare . flat where
  go !js (p:ps) | p  <?  js = js
                | otherwise = go (p~>js) ps
  go !js     _  = js

-- | Calculate a set of all the offspring of the given subset of nodes.
children :: Ltree 2 -> Ltree 2
children  = foldl' (\xs i -> xs \/ offspring i) bare . flat

-- | Construct a set of the \(2^d\) progeny of the given tree node/leaf.
offspring :: Key 2 -> Ltree 2
offspring  = fromList . progeny

------------------------------------------------------------------------------
-- | Compute the set of all parents for the given set of children.
parents :: Ltree 2 -> Ltree 2
parents  = foldl' (\xs -> maybe xs (~>xs) . parent) bare . flat

------------------------------------------------------------------------------
-- | Compute the set of indices that are adjacent to any of the given input
--   indices.
peers :: Ltree 2 -> Ltree 2
-- peers  = foldl' (\xs i -> xs \/ touches i) bare . flat
peers  = foldl' go bare . flat where
  go js = foldl' (flip (~>)) js . neighbourhood
  {-# INLINE go #-}

-- | Calculates the set of nodes that touch the given node.
touches :: Key 2 -> Ltree 2
touches  = fromList . adjacent

------------------------------------------------------------------------------
-- | Compute the set of cells of potential neighbours, if the given cell index
--   was an "inferred element," in the @Qtree@ representation.
aunties :: Key 2 -> Ltree 2
aunties b = maybe bare (<\ (parents . touches) b) $ parent b 

-- ** Leaf-node (patch) type queries
------------------------------------------------------------------------------
-- | Inferred patches have CP's that are "hanging" nodes, and need their
--   values to be computed via interpolation.
--   NOTE: only valid for spline-degree, \(p \in {1, 3}\)?
isInferred :: Key 2 -> Ltree 2 -> Bool
isInferred k = not . isDofPatch k

-- | All CP's of the patch are "DoF" nodes.
--   NOTE: only valid for spline-degree, \(p \in {1, 3}\)?
isDofPatch :: Key 2 -> Ltree 2 -> Bool
isDofPatch k = dry . (aunties k /\)
{-# INLINEABLE[2] isDofPatch #-}

-- FIXME: wrong!?
isDofPatch' :: Key 2 -> Ltree 2 -> Bool
isDofPatch' k = dry . (ns /\) where
  ns = foldl1' (\/) $ touches <$> progeny k

-- ** Shape-checks
------------------------------------------------------------------------------
-- | Checks to see if the given linear-tree completely covers the \([0,1]^d\)
--   domain.
isComplete :: Ltree 2 -> Bool
isComplete lt = go (reify lroot) where
  go !k | k  <?  lt = True
        | otherwise = and $ go <$> progeny k


-- * Traversals
------------------------------------------------------------------------------
-- | Partition the given linear quadtree into two subtrees, using the given
--   predicate.
partition :: (Key 2 -> Bool) -> Ltree 2 -> (Ltree 2, Ltree 2)
partition f = Ltree *** Ltree <<< Set.partition (f . Key) . _leaves

-- | Keep only the linear quadtree leaves that satisfy the given predicate.
filter :: (Key 2 -> Bool) -> Ltree 2 -> Ltree 2
filter f = Ltree . Set.filter (f . Key) . _leaves


-- * Composite operations
------------------------------------------------------------------------------
-- | Compute all nodes that belong to the same parent cell.
--   
--   TODO: is this the desired "failure" behaviour for root-cells?
cellmates :: Key 2 -> Ltree 2
cellmates  = maybe bare offspring . parent
{-# INLINE[1] cellmates #-}

-- | Compute the entire set of cellmates of the given prisoners.
inmates :: Ltree 2 -> Ltree 2
-- inmates  = foldl' (\xs -> (xs \/) . cellmates) bare . flat
inmates = foldl' go bare . flat where
  go js p | p  <?  js = js
          | otherwise = js \/ cellmates p
  {-# INLINE go #-}

------------------------------------------------------------------------------
-- | Compute the set of previous generation of elders.
--   TODO: can this fail if the input set contains nodes that don't have
--     grandparents?
extension :: Ltree 2 -> Ltree 2
extension  = children . parents . peers . parents
{-# INLINE[1] extension #-}


-- * Tree shape modification
------------------------------------------------------------------------------
-- | Rebalance a tree so that it satisfies the 2:1 balance constraint.
--   NOTE: Inspired by `Algorithm 2.2`, pp.15 , from "A Distributed Memory
--     Fast Multipole Method for Volume Potentials", by Malhotra & Biros. But
--     unlike their pseudocode version, there are no non-leaf nodes referred
--     to, or required by this algorithm.
--   
--   TODO: needs better termination conditions, as `extension` may fail?
balance :: Ltree 2 -> Ltree 2
balance s =
  let go 0 _ _ b = b
      go i p t b =
        let (u, r) = partition ((==i) . depth) t
            x  = (p \/ inmates u) \\ ancestors b
            b' = b \/ x
            t' = r \\ ancestors x
            p' = extension x \\ ancestors x
--             p' = extension x \\ ancestors b' -- TODO: expensive?
        in  go (i-1) p' t' b'
  in  go (depth s) bare s bare

edgeNbrs :: Key 2 -> [Key 2]
edgeNbrs  = map reify . orthos . lpath where
  orthos (V3 x y d) =
    let s  = maxWidth2>>%d
        ps = [V3 (x-s) y d, V3 (x+s) y d, V3 x (y-s) d, V3 x (y+s) d]
        go (V3 i j _) = j>=0 && j<maxWidth2 && i>=0 && i<maxWidth2
    in  List.filter go ps

edgePeers :: Ltree 2 -> Ltree 2
edgePeers  = foldl' (flip (~>)) bare . concat . map edgeNbrs . flat . parents

balanceE :: Ltree 2 -> Ltree 2
balanceE s =
  let go 0 _ _ b = b
      go i p t b =
        let (u, r) = partition ((==i) . depth) t
            x  = inmates (p \/ u) \\ ancestors b
            b' = b \/ x
            t' = r \\ ancestors x
            p' = edgePeers x \\ ancestors x
        in  go (i-1) p' t' b'
  in  go (depth s) bare s bare

------------------------------------------------------------------------------
-- | Enforce both 2:1 balance and "area rule" constraints, while rebuilding
--   the given linear tree.
arearule :: Ltree 2 -> Ltree 2
arearule s =
  let go 0 _ _ b = b
      go i p t b =
        let (u, r) = partition ((==i) . depth) t
            x  = inmates (peers p \/ u) \\ ancestors b
            b' = b \/ x
            t' = r \\ ancestors x
            p' = (peers . parents) x \\ ancestors b'
        in  go (i-1) p' t' b'
  in  go (depth s) bare s bare

------------------------------------------------------------------------------
-- | Just fills in any incomplete cells.
complete :: Ltree 2 -> Ltree 2
complete s =
  let go 0 _ b = b
      go i t b =
        let (u, r) = partition ((==i) . depth) t
            x  = inmates u \\ ancestors b
            b' = b \/ x
            t' = r \\ ancestors x
        in  go (i-1) t' b'
  in  go (depth s) s bare


-- * Refinement & subdivision functions
------------------------------------------------------------------------------
-- | Recursively refine the given linear quadtree, and subdivide each leaf-
--   node whenever the predicate evaluates to `True`.
--   TODO: depth-checks?
refine2 :: (Key 2 -> Bool) -> Ltree 2 -> Ltree 2
refine2 p = go bare . flat where
  go r (n:ns)
    | p n       = go r $ progeny n ++ ns
    | otherwise = go (n~>r) ns
  go r     _    = r
{-# INLINE refine2 #-}

-- | Subdivide each leaf-node into four leaves.
subdivide2 :: Ltree 2 -> Ltree 2
subdivide2 t
  | depth t < maxDepth2 = let p = (<?t) in refine2 p t
  | otherwise           = error "Lsubd.subdivide: subdivided tree too high"
{-# INLINE subdivide2 #-}


-- ** FEA & export helpers
------------------------------------------------------------------------------
-- | Remove all "inferred elements" from the tree.
noinferred :: Ltree 2 -> Ltree 2
noinferred lt = filter (flip isDofPatch lt) lt

-- | Keep only the "inferred elements," of the given tree.
inferred :: Ltree 2 -> Ltree 2
inferred lt = filter (\b -> not . dry $ aunties b /\ lt) lt


-- * Testing & examples
------------------------------------------------------------------------------
testBase :: IO ()
testBase  = do
  let lt  = fromList (reify <$> [V3 4096 8192 4, V3 16384 16384 4]) :: Ltree 2
      lt' = balance lt
  print lt
  print lt'
