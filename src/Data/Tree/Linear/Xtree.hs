{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, RankNTypes,
             TypeOperators, MultiParamTypeClasses, FlexibleContexts, GADTs,
             FlexibleInstances, DeriveGeneric, DeriveFunctor, DeriveFoldable,
             DeriveTraversable, GeneralizedNewtypeDeriving, BangPatterns,
             StandaloneDeriving, TypeSynonymInstances, InstanceSigs,
             TemplateHaskell, ScopedTypeVariables
  #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.Tree.Linear.Xtree
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Basic functionality for linear (2^d-)trees, that also store values in the
-- leaf-cells.
-- 
-- Changelog:
--  + 24/04/2019  --  initial file;
-- 
-- TODO:
--  + are there many useful additional operations that I can define in a
--    general way, now that the leaves have data?
-- 
------------------------------------------------------------------------------

module Data.Tree.Linear.Xtree
  ( module Data.Index.ZCurve
  , module Data.Tree.Linear.Xtree
  , module Data.Tree.Linear.Coords
  ) where

import GHC.Generics (Generic)
import GHC.TypeNats
import GHC.Types (SPEC(..))
import Control.Arrow (first, (&&&)) -- , (<<<), (***))
import Control.Lens (makeLenses)
import Control.Monad.IO.Class
import Control.DeepSeq
import Data.Foldable (foldl')
import Data.MonoTraversable
import Data.Containers
import Data.IntSet.Sets as Set
import qualified Data.IntSet as Set
import Data.IntMap.Maps as Map
import qualified Data.IntMap as Map
import Data.Aeson
import qualified Data.ByteString.Lazy as Lazy
import Linear (V2 (..)) -- , V3 (..), V4 (..))

import Data.Index.ZCurve
import Data.Tree.Linear.Coords
import Data.Tree.Linear.Base (Ltree)
import qualified Data.Tree.Linear.Base as Ltree


-- * Linear 2^d type-classes
------------------------------------------------------------------------------
class RefinableM t where
  refineM
    :: Monad m
    => (k ~ ContainerKey t, a ~ MapValue t)
    => (k -> a -> m [(k, a)]) -> t
    -> m t


-- * Demonstration data-types
------------------------------------------------------------------------------
-- | Linear $2^d-$tree implemented as a set of leaf-cell Z-indices.
newtype Xtree (d :: Nat) a = Xtree { _xeaves :: IntMap a }
  deriving (Eq, Ord, Monoid, Semigroup, Generic)

makeLenses ''Xtree

deriving instance NFData a => NFData (Xtree d a)
deriving instance Functor     (Xtree d)
deriving instance Foldable    (Xtree d)
deriving instance Traversable (Xtree d)


-- * And some instances
------------------------------------------------------------------------------
instance Layered (Xtree 2 a) where
  depth :: Xtree 2 a -> Int
  depth  = Set.foldl' (\d -> max d . plen2) 0 . Map.keysSet . _xeaves
  maxDepth _ = maxDepth (undefined :: Key 2)
  {-# INLINE maxDepth #-}

instance RefinableM (Xtree d a) where
  refineM :: forall m. Monad m =>
             (Key d -> a -> m [(Key d, a)]) -> Xtree d a -> m (Xtree d a)
  refineM f = go SPEC bare . flat where
    go :: SPEC -> Xtree d a -> [(Key d, a)] -> m (Xtree d a)
    go !_ r ((k, x):xs) = do
      r' <- f k x >>= \ys -> case ys of
        [ly] -> pure $ ly ~> r
        _    -> go SPEC r ys
      r' `seq` go SPEC r' xs
--       go SPEC r' xs
    go !_ r          _  = pure r

{-- }
--     let go r ((k, x):xs) = foldl' (flip (~>)) r <$> f k x

-- TODO:
instance Layered (Xtree 3 a) where
  depth :: Xtree 3 a -> Int
  depth  = Set.foldl' (\d -> max d . plen2) 0 . Map.keysSet . _xeaves
  maxDepth _ = maxDepth (undefined :: Key 2)
  {-# INLINE maxDepth #-}
--}

-- ** Standard instances
------------------------------------------------------------------------------
{-- }
instance Semigroup (Xtree d a) where
  Xtree xs <> Xtree ys = Xtree $ xs <> ys
  {-# INLINE (<>) #-}

instance Monoid (Xtree d a) where
  mempty = Xtree mempty
  {-# INLINE mempty #-}
--}

-- ** MonoTraversable instances
------------------------------------------------------------------------------
type instance Element (Xtree d a) = a

instance MonoFunctor     (Xtree d a)
instance MonoFoldable    (Xtree d a)
instance MonoTraversable (Xtree d a)
instance GrowingAppend   (Xtree d a)

-- ** DataContainers instances
------------------------------------------------------------------------------
instance SetContainer (Xtree d a) where
  type ContainerKey (Xtree d a) = Key d
  member    = (<?)
  notMember = (>?)
  keys      = fmap Key . Map.keys . _xeaves
  Xtree xs `union`        Xtree ys = Xtree $ xs `union`        ys
  Xtree xs `difference`   Xtree ys = Xtree $ xs `difference`   ys
  Xtree xs `intersection` Xtree ys = Xtree $ xs `intersection` ys
  {-# INLINE member       #-}
  {-# INLINE notMember    #-}
  {-# INLINE union        #-}
  {-# INLINE difference   #-}
  {-# INLINE intersection #-}
  {-# INLINE keys         #-}

------------------------------------------------------------------------------
instance IsMap (Xtree d a) where
  type MapValue (Xtree d a) = a
  lookup              = (<~)
  insertMap       k x = ((k, x) ~>)
  deleteMap           = (<\)
  singletonMap        = curry unit
  mapFromList         = flip (~>) `foldl'` bare
  mapToList           = flat
  insertWith    f k x = alterMap (Just . maybe x (f   x)) k
  insertWithKey f k x = alterMap (Just . maybe x (f k x)) k
  adjustMap     f     = alterMap (fmap f)
  adjustWithKey f k   = alterMap (fmap $ f k) k
  updateMap     f     = alterMap (>>= f)
  updateWithKey f k   = alterMap (>>= f k) k
  -- | Explicit instance for @alterMap@ as this function can be used to
  --   implement many others.
  alterMap :: (Maybe a -> Maybe a) -> Key d -> Xtree d a -> Xtree d a
  alterMap f (Key k) = Xtree . Map.alter f k . _xeaves
  filterMap f = Xtree . Map.filter f . _xeaves
  {-# INLINE lookup    #-}
  {-# INLINE insertMap #-}
  {-# INLINE deleteMap #-}
  {-# INLINE mapToList #-}
  {-# INLINE alterMap  #-}
  {-# INLINE filterMap #-}

------------------------------------------------------------------------------
-- | Polymorphic differences and intersections of linear trees.
--   
instance PolyMap (Xtree d) where
  differenceMap :: Xtree d a -> Xtree d b -> Xtree d a
  Xtree xs `differenceMap` Xtree ys = Xtree $ xs `Map.difference` ys
  intersectionMap :: Xtree d a -> Xtree d b -> Xtree d a
  Xtree xs `intersectionMap` Xtree ys = Xtree $ xs `Map.intersection` ys
  intersectionWithMap ::
    (a -> b -> c) -> Xtree d a -> Xtree d b -> Xtree d c
  intersectionWithMap f (Xtree xs) (Xtree ys) =
    Xtree $ Map.intersectionWith f xs ys
  {-# INLINE differenceMap       #-}
  {-# INLINE intersectionMap     #-}
  {-# INLINE intersectionWithMap #-}

-- ** JSON import/export
------------------------------------------------------------------------------
instance ToJSON   a => ToJSON   (Xtree 2 a)
instance FromJSON a => FromJSON (Xtree 2 a)

instance ToJSON   a => ToJSON   (Xtree 3 a)
instance FromJSON a => FromJSON (Xtree 3 a)

-- ** Set-line instances
------------------------------------------------------------------------------
deriving instance SetOps  (Xtree d a)
deriving instance SetSize (Xtree d a)

instance SetInsert (Key d, a) (Xtree d a) where
  (Key k, a) ~> Xtree ls = Xtree $ (k, a) ~> ls
  {-# INLINE (~>) #-}

instance SetIndex (Key d) (Xtree d a) where
  Key k <? Xtree ls = k <? ls
  Key k <\ Xtree ls = Xtree $ k <\ ls
  {-# INLINE (<?) #-}
  {-# INLINE (<\) #-}

instance SetBuild (Key d, a) (Xtree d a) where
  bare = Xtree bare
  unit = Xtree . unit . first _key
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetFlat (Key d, a) (Xtree d a) where
  flat = fmap (first Key) . flat . _xeaves
  {-# INLINE flat #-}

instance SetElems (Key d) (Maybe a) (Xtree d a) where
  Key k <~ Xtree ls = Map.lookup k ls
  {-# INLINE (<~) #-}


-- ** Dump to text output
------------------------------------------------------------------------------
instance Show a => Show (Xtree 2 a) where
  show = showXtree
  {-# INLINE show #-}

showXtree :: Show a => Xtree 2 a -> String
showXtree (Xtree ls) = "Xtree {_xeaves = " ++ show xs ++ "}" where
  xs = first lpos2 <$> flat ls
{-# INLINEABLE showXtree #-}


-- * Constructors
------------------------------------------------------------------------------
-- | Construct a uniform(-height) linear quadtree using the given fuction to
--   initialise each leaf-value.
uniformWith :: (Key 2 -> a) -> Int -> Xtree 2 a
uniformWith f h = fromList $ (id &&& f) <$> ks where
  (w, ks) = (2^h-1, Key <$> [topath2 h (V2 i j) | i<-[0..w], j<-[0..w]])
{-# INLINEABLE uniformWith #-}

-- | Construct a uniform(-height) linear quadtree.
uniform :: a -> Int -> Xtree 2 a
uniform x = uniformWith (const x)
{-# INLINE uniform #-}

-- ** Conversions
------------------------------------------------------------------------------
-- | Convert from an 'Ltree' using the given function to compute values from
--   keys.
fromLtreeWith :: (Key d -> a) -> Ltree d -> Xtree d a
fromLtreeWith f = fromList . fmap (id &&& f) . flat
{-# INLINEABLE fromLtreeWith #-}

fromLtree :: a -> Ltree d -> Xtree d a
fromLtree x = fromList . fmap (id &&& const x) . flat
{-# INLINEABLE fromLtree #-}

-- | Drop all of the values from the leaf-cells.
toLtree :: Xtree d a -> Ltree d
toLtree  = Ltree.Ltree . Map.keysSet . _xeaves
{-# INLINE toLtree #-}

-- | Build a linear quadtree from a list of leaves.
fromList :: [(Key d, a)] -> Xtree d a
fromList  = Xtree . Map.fromList . fmap (first _key)
{-# INLINE fromList #-}

-- ** JSON save/load
------------------------------------------------------------------------------
saveXtree :: (MonadIO m, ToJSON (Xtree d a)) =>
  FilePath -> Xtree d a -> m ()
saveXtree fp = liftIO . Lazy.writeFile fp . encode

loadXtree :: (MonadIO m, FromJSON (Xtree d a)) =>
  FilePath -> m (Maybe (Xtree d a))
loadXtree  = liftIO . fmap decode . Lazy.readFile


-- * Tree shape modification
------------------------------------------------------------------------------
{-- }
-- | Just fills in any incomplete cells.
completeWith :: (Key 2 -> a) -> Xtree 2 a -> Xtree 2 a
completeWith f s =
  let go 0 _ b = b
      go i t b =
        let (u, r) = partition ((==i) . depth) t
            x  = inmates u \\ ancestors b
            b' = b \/ x
            t' = r \\ ancestors x
        in  go (i-1) t' b'
  in  go (depth s) s bare


-- * Queries
------------------------------------------------------------------------------
-- | Compute all of the ancestors for all of the given nodes.
ancestors :: Xtree 2 a -> Xtree 2 a
ancestors  = foldl' (\js -> go js . reverse . lineage) bare . flat where
  go !js (p:ps) | p  <?  js = js
                | otherwise = go (p~>js) ps
  go !js     _  = js

-- | Calculate a set of all the offspring of the given subset of nodes.
children :: Xtree 2 a -> Xtree 2 a
children  = foldl' (\xs i -> xs \/ offspring i) bare . flat

-- | Construct a set of the \(2^d\) progeny of the given tree node/leaf.
offspring :: Key 2 -> Xtree 2 a
offspring  = fromList . progeny

------------------------------------------------------------------------------
-- | Compute the set of all parents for the given set of children.
parents :: Xtree 2 a -> Xtree 2 a
parents  = foldl' (\xs i -> parent i ~> xs) bare . flat

------------------------------------------------------------------------------
-- | Compute the set of indices that are adjacent to any of the given input
--   indices.
peers :: Xtree 2 a -> Xtree 2 a
-- peers  = foldl' (\xs i -> xs \/ touches i) bare . flat
peers  = foldl' go bare . flat where
  go js = foldl' (flip (~>)) js . neighbourhood
  {-# INLINE go #-}

-- | Calculates the set of nodes that touch the given node.
touches :: Key 2 -> Xtree 2 a
touches  = fromList . adjacent

------------------------------------------------------------------------------
-- | Compute the set of cells of potential neighbours, if the given cell index
--   was an "inferred element," in the @Qtree@ representation.
aunties :: Key 2 -> Xtree 2 a
aunties b = parent b <\ (parents . touches) b


-- ** Leaf-node (patch) type queries
------------------------------------------------------------------------------
-- | Inferred patches have CP's that are "hanging" nodes, and need their
--   values to be computed via interpolation.
--   NOTE: only valid for spline-degree, \(p \in {1, 3}\)?
isInferred :: Key 2 -> Xtree 2 a -> Bool
isInferred k = not . isDofPatch k

-- | All CP's of the patch are "DoF" nodes.
--   NOTE: only valid for spline-degree, \(p \in {1, 3}\)?
isDofPatch :: Key 2 -> Xtree 2 a -> Bool
isDofPatch k = dry . (aunties k /\)
{-# INLINEABLE[2] isDofPatch #-}

-- FIXME: wrong!?
isDofPatch' :: Key 2 -> Xtree 2 a -> Bool
isDofPatch' k = dry . (ns /\) where
  ns = foldl1' (\/) $ touches <$> progeny k

-- ** Shape-checks
------------------------------------------------------------------------------
-- | Checks to see if the given linear-tree completely covers the \([0,1]^d\)
--   domain.
isComplete :: Xtree 2 a -> Bool
isComplete lt = go (reify lroot) where
  go !k | k  <?  lt = True
        | otherwise = and $ go <$> progeny k


-- * Traversals
------------------------------------------------------------------------------
-- | Partition the given linear quadtree into two subtrees, using the given
--   predicate.
partition :: (Key 2 -> Bool) -> Xtree 2 a -> (Xtree 2 a, Xtree 2 a)
partition f = Xtree *** Xtree <<< Set.partition (f . Key) . _xeaves

-- | Keep only the linear quadtree leaves that satisfy the given predicate.
filter :: (Key 2 -> Bool) -> Xtree 2 a -> Xtree 2 a
filter f = Xtree . Set.filter (f . Key) . _xeaves


-- * Composite operations
------------------------------------------------------------------------------
-- | Compute all nodes that belong to the same parent cell.
cellmates :: Key 2 -> Xtree 2 a
cellmates  = offspring . parent
{-# INLINE[1] cellmates #-}

-- | Compute the entire set of cellmates of the given prisoners.
inmates :: Xtree 2 a -> Xtree 2 a
-- inmates  = foldl' (\xs -> (xs \/) . cellmates) bare . flat
inmates = foldl' go bare . flat where
  go js p | p  <?  js = js
          | otherwise = js \/ cellmates p
  {-# INLINE go #-}

------------------------------------------------------------------------------
-- | Compute the set of previous generation of elders.
--   TODO: can this fail if the input set contains nodes that don't have
--     grandparents?
extension :: Xtree 2 a -> Xtree 2 a
extension  = children . parents . peers . parents
{-# INLINE[1] extension #-}


-- * Tree shape modification
------------------------------------------------------------------------------
-- | Rebalance a tree so that it satisfies the 2:1 balance constraint.
--   NOTE: Inspired by `Algorithm 2.2`, pp.15 , from "A Distributed Memory
--     Fast Multipole Method for Volume Potentials", by Malhotra & Biros. But
--     unlike their pseudocode version, there are no non-leaf nodes referred
--     to, or required by this algorithm.
--   
--   TODO: needs better termination conditions, as `extension` may fail?
balance :: Xtree 2 a -> Xtree 2 a
balance s =
  let go 0 _ _ b = b
      go i p t b =
        let (u, r) = partition ((==i) . depth) t
            x  = (p \/ inmates u) \\ ancestors b
            b' = b \/ x
            t' = r \\ ancestors x
            p' = extension x \\ ancestors x
--             p' = extension x \\ ancestors b' -- TODO: expensive?
        in  go (i-1) p' t' b'
  in  go (depth s) bare s bare

edgeNbrs :: Key 2 -> [Key 2]
edgeNbrs  = map reify . orthos . lpath where
  orthos (V3 x y d) =
    let s  = maxWidth>>%d
        ps = [V3 (x-s) y d, V3 (x+s) y d, V3 x (y-s) d, V3 x (y+s) d]
        go (V3 i j _) = j>=0 && j<maxWidth && i>=0 && i<maxWidth
    in  List.filter go ps

edgePeers :: Xtree 2 a -> Xtree 2 a
edgePeers  = foldl' (flip (~>)) bare . concat . map edgeNbrs . flat . parents

balanceE :: Xtree 2 a -> Xtree 2 a
balanceE s =
  let go 0 _ _ b = b
      go i p t b =
        let (u, r) = partition ((==i) . depth) t
            x  = inmates (p \/ u) \\ ancestors b
            b' = b \/ x
            t' = r \\ ancestors x
            p' = edgePeers x \\ ancestors x
        in  go (i-1) p' t' b'
  in  go (depth s) bare s bare

------------------------------------------------------------------------------
-- | Enforce both 2:1 balance and "area rule" constraints, while rebuilding
--   the given linear tree.
arearule :: Xtree 2 a -> Xtree 2 a
arearule s =
  let go 0 _ _ b = b
      go i p t b =
        let (u, r) = partition ((==i) . depth) t
            x  = inmates (peers p \/ u) \\ ancestors b
            b' = b \/ x
            t' = r \\ ancestors x
            p' = (peers . parents) x \\ ancestors b'
        in  go (i-1) p' t' b'
  in  go (depth s) bare s bare

------------------------------------------------------------------------------
-- | Just fills in any incomplete cells.
complete :: Xtree 2 a -> Xtree 2 a
complete s =
  let go 0 _ b = b
      go i t b =
        let (u, r) = partition ((==i) . depth) t
            x  = inmates u \\ ancestors b
            b' = b \/ x
            t' = r \\ ancestors x
        in  go (i-1) t' b'
  in  go (depth s) s bare


-- ** FEA & export helpers
------------------------------------------------------------------------------
-- | Remove all "inferred elements" from the tree.
noinferred :: Xtree 2 a -> Xtree 2 a
noinferred lt = filter (flip isDofPatch lt) lt

-- | Keep only the "inferred elements," of the given tree.
inferred :: Xtree 2 a -> Xtree 2 a
inferred lt = filter (\b -> not . dry $ aunties b /\ lt) lt


-- * Testing & examples
------------------------------------------------------------------------------
testBase :: IO ()
testBase  = do
  let lt  = fromList (reify <$> [V3 4096 8192 4, V3 16384 16384 4]) :: Xtree 2 a
      lt' = balance lt
  print lt
  print lt'
--}
