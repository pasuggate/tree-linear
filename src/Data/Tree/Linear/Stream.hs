{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
             TypeOperators, MultiParamTypeClasses, FunctionalDependencies,
             FlexibleContexts, FlexibleInstances, TypeSynonymInstances,
             InstanceSigs, ScopedTypeVariables, ViewPatterns, PatternSynonyms,
             DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
             StandaloneDeriving, BangPatterns, TupleSections, TemplateHaskell
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Tree.Linear.Stream
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Stream-fusion functionality, for linear trees.
-- 
-- Changelog:
--  + 21/10/2018  --  initial file;
-- 
-- TODO:
--  + currently this module is incomplete, and its function do not fuse;
--  + generalise to trees of 1, 2, 3, and 4 dimensions;
-- 
------------------------------------------------------------------------------

module Data.Tree.Linear.Stream
       ( Streamable (..)
       , runStream, unstream', tostream'
       , elaborate
       ) where

import GHC.Types (SPEC(..))
import Data.Set.Sets
import Data.IntSet.Sets
import qualified Data.IntSet as IntSet
import Data.Vector.Fusion.Stream.Monadic (Stream(..), Step(..))
import Linear (V3 (..)) -- , V4 (..))

import Data.Tree.Linear.Base


-- * Streaming classes
------------------------------------------------------------------------------
-- | Stream elements, of type `e`, from/to the indicated container-type.
class Streamable s e | s -> e where
  tostream :: forall m. Monad m => s -> Stream m e
  unstream :: forall m. Monad m => Stream m e -> m s


-- ** Streamable instances
------------------------------------------------------------------------------
-- | General-purpose instance.
instance Streamable IntSet Int where
  tostream = Stream step where
    step = return . maybe Done (uncurry Yield) . IntSet.minView
    {-# INLINE step #-}
  unstream (Stream step t) =
    let go !_ p s = step s >>= \r -> case r of
          Yield x s' -> go SPEC (x~>p) s'
          Skip    s' -> go SPEC p s'
          Done       -> return p
    in  go SPEC bare t
  {-# INLINE[1] tostream #-}
  {-# INLINE[1] unstream #-}

------------------------------------------------------------------------------
-- | Instances for streaming and containing 2D linear trees.
instance Streamable (Ltree 2) (V3 Int) where
  tostream = fmap lpos2 . tostream . _leaves
  unstream = fmap Ltree . unstream . fmap tkey2
  {-# INLINE[1] tostream #-}
  {-# INLINE[1] unstream #-}

{-- }
-- | Instances for streaming and containing 3D linear trees.
instance Streamable (Ltree 3) (V4 Int) where
  tostream = fmap lpos3 . tostream . _leaves
  unstream = fmap Ltree . unstream . fmap tkey3
  {-# INLINE[1] tostream #-}
  {-# INLINE[1] unstream #-}
--}


-- * Stream evaluation.
------------------------------------------------------------------------------
-- | Evaluate to a list.
runStream :: Monad m => Stream m a -> m [a]
runStream (Stream step t) = go SPEC t
  where
    go !_ s = step s >>= \r -> case r of
      Yield x s' -> (x:) <$> go SPEC s'
      Skip    s' -> go SPEC s'
      Done       -> return []
{-# INLINE[1] runStream #-}

unstream' :: (Ord a, Monad m) => Stream m a -> m (Set a)
unstream' (Stream step t) = do
  let go !_ p s = step s >>= \r -> case r of
        Yield x s' -> go SPEC (x~>p) s'
        Skip    s' -> go SPEC p s'
        Done       -> return p
  go SPEC bare t
{-# INLINE[1] unstream' #-}

tostream' :: (SetFlat e s, Monad m) => s -> Stream m e
tostream' s = Stream step $ flat s
  where
    step    []  = return $ Done
    step (x:xs) = return $ Yield x xs
{-# INLINE[1] tostream' #-}

------------------------------------------------------------------------------
-- | Given a function that produces multiple outputs, "restream" those values
--   as individual elements.
--   
--   TODO: generalise over the "elaborated" container-type?
elaborate :: Monad m => (a -> [b]) -> Stream m a -> Stream m b
elaborate f (Stream step t) = 
  let next (s, []) = step s >>= \r -> case r of
        Yield x s' -> let y:ys = f x
                      in  return $ Yield y (s', ys)
        Skip    s' -> return $ Skip (s', [])
        Done       -> return $ Done
      next (s,y:ys) = return $ Yield y (s, ys)
  in  Stream next (t, [])
{-# INLINEABLE elaborate #-}
