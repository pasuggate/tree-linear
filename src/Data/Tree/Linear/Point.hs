{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FunctionalDependencies,
FlexibleContexts, FlexibleInstances, TypeSynonymInstances, InstanceSigs,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
BangPatterns, ViewPatterns, PatternSynonyms, TupleSections,
StandaloneDeriving, ScopedTypeVariables, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Tree.Linear.Point
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- The vertices of a linear \(2^d-\)tree are the locations of B-spline
-- control-points. This module contains functions for working with these.
-- 
-- Changelog:
--  + 18/01/2019  --  initial file;
-- 
-- TODO:
--  + generalise to trees of 1, 2, 3, and 4 dimensions;
-- 
------------------------------------------------------------------------------

module Data.Tree.Linear.Point
       ( odds
       , finer
       , incident
       , findIncident
       , refineAt
       ) where

import Math.Sets
import Data.Foldable (foldl', toList)
import Data.List as List
import Data.Bits
import Data.Bits.Extra ((<<%))
-- import Data.IntSet.Sets
import Linear (V3 (..), V4 (..))

import Data.Index.ZCurve
import Data.Tree.Linear.Base
-- import Data.Tree.Linear.Coords


-- * Control-point coordinate queries
------------------------------------------------------------------------------
-- class Incidence t where
--   incident :: Point t -> t -> 

------------------------------------------------------------------------------
-- | Returns the coordinate of a point that is one level more refined.
finer :: V3 Int -> V3 Int
finer (V3 j i d)
  | d<maxDepth2 = V3 j i (d+1)
  | otherwise   = error "Point.finer: can't go finer, already at `maxDepth`"

-- ** Coordinate-alignment functions
------------------------------------------------------------------------------
-- | Is the given ordinate "even" or "odd", for the indicated refinement
--   level, `d`?
_even :: Int -> Int -> Bool
_even d x =
  let m = (2<<%maxDepth2) - (2<<%(maxDepth2-d))
  in  x .&. m == x
{-# INLINE _even #-}

-- | Compute whether each axis-coordinate has even or odd alignment.
--   NOTE: works as long as the last ordinate is the refinement level.
odds :: Foldable f => f Int -> [Bool]
odds xi = let ev = _even (last xs)
              xs = toList xi
          in  not . ev <$> init xs
{-# SPECIALIZE odds :: V3 Int -> [Bool] #-}
{-# SPECIALIZE odds :: V4 Int -> [Bool] #-}

------------------------------------------------------------------------------
-- | Calculate the Z-indices of any leaves that have a vertex at the given
--   point.
incident :: V3 Int -> [Key 2]
incident (V3 j i d) =
  let ds = [-(1<<%(maxDepth2-d)), 0]
      w  = 1<<%maxDepth2
      go (V3 x y l) = x>=0 && x<w && y>=0 && y<w && l>=0 && l<=maxDepth2
  in  reify <$> List.filter go [V3 (j+x) (i+y) d | x <- ds, y <- ds]

-- | Find any leaves that have a vertex at the given point.
findIncident :: V3 Int -> Ltree 2 -> [Key 2]
findIncident p ls = (<?ls) `List.filter` incident p

{-- }
support :: V3 Int -> Ltree 2 -> [Key 2]
support p ls =
--}


-- * Tree-modification at selected points
------------------------------------------------------------------------------
-- | Refine at the given CP coordinate, if it exists within the tree.
--   NOTE: successful application of this function will create "holes" in the
--     tree, so after all refinements have been performed, the tree should be
--     rebuilt; e.g., using `arearule`.
refineAt :: Ltree 2 -> V3 Int -> Ltree 2
refineAt ls p
  | null ps   = ls
  | otherwise = foldl' (flip (~>)) lt qs
--   | otherwise = reify (finer p)~>(reify p<\ls)
  where
    ps = findIncident p ls
    qs = incident (finer p)
    lt = foldl' (flip (<\)) ls ps
