{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, GADTs, TypeOperators,
             OverloadedLists, TupleSections, BangPatterns, InstanceSigs,
             MultiParamTypeClasses, TypeFamilies, FunctionalDependencies,
             FlexibleContexts, FlexibleInstances, TypeSynonymInstances,
             DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
             StandaloneDeriving, GeneralizedNewtypeDeriving,
             ViewPatterns, PatternSynonyms, ScopedTypeVariables,
             TemplateHaskell
  #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.Mesh.Equation
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Equation builder for pairing maps and spaces
-- 
-- Changelog:
--  + 17/11/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Mesh.Equation where

import GHC.TypeNats
import Control.Lens (makeLenses, makePrisms, (^.), (.~))
import Data.VectorSpace
import Data.Vector.Helpers
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import Linear (dot, V1 (..), V2 (..), V3 (..), V4 (..))

import Math.Calculus.PDE
import Numeric.Quadrature
import Data.VectorSpace.Orphans
import Data.BSpline.Uniform
import Data.Mesh.Element


-- * Map/equation data-types
------------------------------------------------------------------------------
-- | Abstract representation for a map.
--   NOTE: parameterised by the number of (parametric) dimensions and the
--     index/label type.
type Eqn d = PDE () d R

type instance IndexOf (PDE m d a) = m -> Vector a


-- * Equation evaluation
------------------------------------------------------------------------------
{-- }
evaluate :: Patch2D -> Equation 2 Z -> 
  (KnownNat d, KnownNat p, VectorSpace a) =>
  Patch d p a -> Equation 2 Z -> a
evaluate p e =
  let qp = qpoints p 3
  in  undefined
--}


{-- }
-- * Integration instances
-- ** Integrate a function to compute an LHS matrix
------------------------------------------------------------------------------
instance GalerkinIntegrable (BSplinePatch R) (CCSMat R) where
  galerkinIntegrate f p = undefined

instance GalerkinIntegrable (Mesh R) (CCSMat R) where
  galerkinIntegrate f m = undefined

-- ** Integrate a function to compute an RHS vector
------------------------------------------------------------------------------
instance GalerkinIntegrable (BSplinePatch R) (SpV R) where
  galerkinIntegrate f p = undefined

instance GalerkinIntegrable (Mesh R) (Vector R) where
  galerkinIntegrate f m = undefined
--}


-- ** Additional instances for working with PDE's
------------------------------------------------------------------------------
instance Continuity (PDE m 2 R) where
  continuity (Const    _) = (0, maxBound)
  continuity (Field    _) = (3, 2)
  continuity  Basis       = (3, 2)
  continuity (Plus   a b) = let (pa, ca) = continuity a
                                (pb, cb) = continuity b
                            in  (max pa pb, min ca cb)
  continuity (Scale  k p) = let (pk, ck) = continuity k
                                (pp, cp) = continuity p
                            in  (pk+pp, min ck cp)
  continuity (Dot    a b) = let (pa, ca) = continuity a
                                (pb, cb) = continuity b
                            in  (pa+pb, min ca cb)
  continuity (Times  a b) = let (pa, ca) = continuity a
                                (pb, cb) = continuity b
                            in  (pa+pb, min ca cb)
  continuity           _  = error "continuity: invalid"

instance Continuity (PDE m 2 (V2 R)) where
  continuity (Const    _) = (0, maxBound)
  continuity (Field    _) = (3, 2)
  continuity (Gradient p) = let (pp, cp) = continuity p
                            in  (pp-1, cp-1)
  continuity (Plus   a b) = let (pa, ca) = continuity a
                                (pb, cb) = continuity b
                            in  (max pa pb, min ca cb)
  continuity (Scale  k p) = let (pk, ck) = continuity k
                                (pp, cp) = continuity p
                            in  (pk+pp, min ck cp)
  continuity (Times  a b) = let (pa, ca) = continuity a
                                (pb, cb) = continuity b
                            in  (pa+pb, min ca cb)
  continuity           _  = error "continuity: invalid"

-- ** Compilation of abstract representations of PDE's
------------------------------------------------------------------------------
-- | Compile a PDE.
--   TODO:
instance Compilable (PDE () 2 R) where
  type Compiled (PDE () 2 R) = (V2 R -> Vector R)
  compile (Const   x) = const (rep 4 (x/4))
  compile  Basis      = basisfun 3
  compile (Plus  a b) = \uv -> Vec.zipWith (+) (compile a uv) (compile b uv)
  compile (Scale a b) = \uv -> Vec.zipWith (*) (compile a uv) (compile b uv)
  compile (Times a b) = \uv -> Vec.zipWith (*) (compile a uv) (compile b uv)
  compile (Dot   a b) = \uv -> Vec.zipWith dot (compile a uv) (compile b uv)
  compile          _  = error "compile_R: cannot compile"

instance Compilable (PDE () 2 (V2 R)) where
  type Compiled (PDE () 2 (V2 R)) = (V2 R -> Vector (V2 R))
  compile (Const   x) = const (rep 4 (x^/4))
  compile (Gradient Basis) = basisder 3
  compile (Plus  a b) = \uv -> Vec.zipWith (^+^) (compile a uv) (compile b uv)
  compile (Scale a b) = \uv -> Vec.zipWith (*^)  (compile a uv) (compile b uv)
  compile          _  = error "compile_V2_R: cannot compile"


-- * Testing & examples
------------------------------------------------------------------------------
testEquation :: IO ()
testEquation  = do
  let pde = basis *^ Const 3 `Scale` grd :: PDE () 2 (V2 R)
      lap = grd `Dot` grd :: PDE () 2 R
      grd = gradient basis :: PDE () 2 (V2 R)
  putStrLn "testEquation"
