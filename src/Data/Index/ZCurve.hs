{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
             TypeOperators, MultiParamTypeClasses, FlexibleContexts,
             FlexibleInstances, DeriveGeneric, DeriveFunctor, DeriveFoldable,
             DeriveTraversable, StandaloneDeriving,
             GeneralizedNewtypeDeriving, InstanceSigs,
             FunctionalDependencies, TypeSynonymInstances, TemplateHaskell
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Index.ZCurve
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Type-functions and type-classes for working with linear trees, paths, and
-- other recursive data types
-- 
-- Changelog   :
-- 
--  * 24-06-2018  --  initial file;
-- 
-- TODO        :
-- 
--  * need an ordered-container for @Word64@s -- so I don't have to right-
--    shift by one, to avoid negative indices;
-- 
------------------------------------------------------------------------------

module Data.Index.ZCurve
  ( Relations (..)
  , Layered (..)
  , Lindex (..)
  , HasWidth (..)
  , HasCoords (..)
  , DimsOf
  -- constants:
  , maxDepth2
  , maxWidth2
  -- key data-types and functions:
  , Key (..)
  , key
  -- 2D-helpers for keys and coords:
  , plen2
  , topath2
  , unpath2
  ) where

import GHC.TypeNats
import GHC.Generics (Generic)
-- import GHC.Word
-- import Control.Category ((<<<))
import Control.Lens (makeLenses, (^.))
import Linear (V2 (..), V3 (..), _z, V4 (..), _w) -- , _xy, _xyz)
import Data.Bits.Extra
import Data.Bits.Extras (w64, Ranked (lsb))


-- * Coordinate type-families
------------------------------------------------------------------------------
type family   DimsOf  t      :: Nat
type instance DimsOf (Key d)  = d


-- * Parameterised data types for working with /d/-dimensional keys
------------------------------------------------------------------------------
-- | Keys are the (Z-index) encoded coordinates.
newtype Key (d :: Nat) = Key { _key :: Int }
  deriving (Eq, Ord, Enum, Bounded, Num, Real, Integral, Read, Show, Generic)

makeLenses ''Key


-- * Constants
------------------------------------------------------------------------------
-- | When using Z-indices, and 2D coordinates, the maximum depth possible when
--   using 64-bit @Int@s is 31 .
--   NOTE: maximum depth is set to 15 initially.
maxDepth2 :: Int
maxDepth2  = maxDepth (undefined :: Key 2)
{-# INLINE maxDepth2 #-}

-- | Fixed-point width of the domain.
maxWidth2 :: Int
maxWidth2  = 1 <<% maxDepth2
{-# INLINE maxWidth2 #-}


-- * Type-classes for working with Z-indices
------------------------------------------------------------------------------
class Relations t where
  -- | Calculate the parent of the given cell, if it exists.
  parent        :: t -> Maybe t
  -- | Compute the progeny (or, $2^d$ descendants) of the given node.
  progeny       :: t -> [t]
  -- | Compute lineage (i.e., all of the ancestors) of the given node.
  lineage       :: t -> [t]
  -- | Calculates the indices of the nodes that touch the given node.
  --   NOTE: excludes the given node.
  adjacent      :: t -> [t]
  -- | Calculates the indices of all nodes that touch the given node.
  --   NOTE: includes the given node.
  neighbourhood :: t -> [t]

-- ** Classes of functions for working with paths
------------------------------------------------------------------------------
-- | Generate and convert linear \(2^d-\) tree indices.
class Lindex t k | t->k, k->t where
  reify :: t -> k
  lpath :: k -> t
  lroot :: t

------------------------------------------------------------------------------
-- | For data-types that have layers.
class Layered t where
  maxDepth :: t -> Int
  depth    :: t -> Int

-- | Get the width of the cell described by the given value.
class HasWidth t where
  maxWidth :: t -> Int
  width    :: t -> Int

-- | For data-types that have coordinates.
--   NOTE: excludes tree -level/-depth
class HasCoords t where
  type CoordVec t :: *
  coords :: t -> CoordVec t


-- * Instance declarations
------------------------------------------------------------------------------
-- | Generate and convert Z-curve indices for linear quadtrees.
instance Lindex (V3 Int) (Key 2) where
  reify :: V3 Int -> Key 2
  reify (V3 x y d) = let s = maxDepth2 - d
                     in  Key $ topath2 d ((>>% s) <$> V2 y x)
  lpath :: Key 2 -> V3 Int
  lpath (Key p) = let d = plen2 p
                      V2 y x = maxDepth2 `unpath2` p
                  in  V3 x y d
  lroot = lpath $ Key $ minBound >>% 1
  {-# INLINE lroot #-}

------------------------------------------------------------------------------
-- | Get the depth from a quadtree leaf-node coordinate.
instance Layered (V3 Int) where
  maxDepth _ = error "maxDepth_V3_Int: please do not use, as unsafe"
  depth = (^._z)
  {-# INLINE depth #-}

-- | Get the depth from a octree leaf-node coordinate.
instance Layered (V4 Int) where
  maxDepth _ = error "maxDepth_V4_Int: please do not use, as unsafe"
  depth = (^._w)
  {-# INLINE depth #-}

instance Layered (Key 2) where
--   maxDepth _ = 15 -- TODO:
  maxDepth _ = 30 -- TODO:
  depth = plen2 . _key
  {-# INLINE maxDepth #-}
  {-# INLINE depth #-}

instance HasWidth (Key 2) where
  maxWidth = const maxWidth2
  width = (1 <<%) . (maxDepth2 -) . depth
  {-# INLINE maxWidth #-}
  {-# INLINE width #-}

{-- }
------------------------------------------------------------------------------
instance HasCoords (V3 Int) where
  type CoordVec (V3 Int) = V2 Int
  coords = (^._xy)

instance HasCoords (V4 Int) where
  type CoordVec (V4 Int) = V3 Int
  coords = (^._xyz)
--}

------------------------------------------------------------------------------
instance Relations (Key 2) where
  parent        = fmap Key . parent2   . _key
  progeny       = fmap Key . progeny2  . _key
  lineage       = fmap Key . lineage2  . _key
  adjacent      = fmap Key . adjacent2 . _key
  neighbourhood = fmap Key . neighbourhood2 . _key


-- * Internal Functions
------------------------------------------------------------------------------

-- ** 2D parent/child/adjacent functions
------------------------------------------------------------------------------
-- | Calculate the index of the node that is the parent-node, for the given
--   node index.
parent2  :: Int -> Maybe Int
parent2 i | plen2 i < 1 = Nothing
          | otherwise   = Just $ parent2' i

parent2' :: Int -> Int
parent2' i = let t = 4 <<% lsb (w64 i)
                 m = minBound `xor` complement (t-1)
             in  i .&. m .|. t

-- | Compute the progeny (or, $2^d$ descendants) of the given node.
progeny2 :: Int -> [Int]
progeny2 i
  | d<maxDepth2 = let os = [j<<%s | j <- [1,3,5,7]]
                      s  = lsb (w64 i) - 2
                      i' = i .&. complement (7<<%s)
                  in  (i' .|.) <$> os
  | otherwise   = error "ZCurve.progeny2: can't go deeper"
  where d = depth (Key i :: Key 2)

-- | Compute lineage (i.e., all of the ancestors) of the given node.
lineage2 :: Int -> [Int]
lineage2 p = let go m t | p.&.(t-1) == 0 = []
                        | otherwise      = (p .&. m .|. t):go (m>>%2) (t>>%2)
             in  go minBound (1<<%62)
{-# INLINE lineage2 #-}

------------------------------------------------------------------------------
-- | Calculates the indices of the nodes that touch the given node.
--   NOTE: excludes the given node.
adjacent2 :: Int -> [Int]
adjacent2 k =
  let V3 j i d = lpath (Key k)
      (w, s)   = (1<<%(maxDepth2-d), 1<<%maxDepth2)
      ts = [V3 x y d | y<-[i-w,i,i+w], x<-[j-w,j,j+w], y/=i || x/=j]
  in  _key . reify <$> filter (\(V3 a b _) -> a>=0 && a<s && b>=0 && b<s) ts

-- | Calculates the indices of all nodes that touch the given node.
--   NOTE: includes the given node.
neighbourhood2 :: Int -> [Int]
neighbourhood2 k =
  let V3 j i d = lpath (Key k)
      (w, s)   = (1<<%(maxDepth2-d), 1<<%maxDepth2)
      ts = [V3 x y d | y<-[i-w,i,i+w], x<-[j-w,j,j+w]]
  in  _key . reify <$> filter (\(V3 a b _) -> a>=0 && a<s && b>=0 && b<s) ts


-- * Path Helpers
------------------------------------------------------------------------------
-- | Convert a path into @(i, j)@ coordinates.
--   NOTE: Needs to know the maximum depth, so that shorter paths are scaled
--     accordingly.
unpath2 :: Int -> Int -> V2 Int
unpath2 d q = fi . (<<% s) <$> V2 (x >>% 32) (x .&. 0x0FFFFFFFF)
  where
    p = w64 q
    x = unshuffle $ p >>% succ n
    s = (n - 62) >>% 1 + d
    n = lsb p

-- | Construct a path from the given row and column indices.
--   NOTE: Needs to know the coordinate depth, to resolve ambiguities.
topath2 :: Int -> V2 Int -> Int
topath2 0 _  = 1 <<% 62
topath2 d ij = fi $ ((t .|. shuffle x) .&. m) >>% 1
  where
    V2 i j = w64 <$> ij
    s = 32 - d
    t = 1 <<% (63 - (d+d))
    m = complement $ t - 1
    x = i <<% (32+s) .|. j<<%s .&. 0x0FFFFFFFF

------------------------------------------------------------------------------
-- | Path length (in bits).
plen2' :: Int -> Int
plen2'  = (-) 62 . lsb . w64
{-# INLINE[2] plen2' #-}

-- | Path length, in tree nodes.
plen2 :: Int -> Int
plen2  = (>>% 1) . plen2'
{-# INLINE[1] plen2 #-}
