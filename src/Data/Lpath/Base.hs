{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
StandaloneDeriving,
TypeSynonymInstances, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Lpath.Base
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Type-functions and type-classes for working with linear trees, paths, and
-- other recursive data types
-- 
-- Changelog:
--  + 24/06/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Lpath.Base where

import GHC.TypeNats
import GHC.Word
import Linear (V2 (..))
import Data.Bits.Extra
import Data.Bits.Extras (msb, w64)


-- * Type-level functions for working with `d`-dimensional representations
------------------------------------------------------------------------------
-- | Stewpids?
type family   Lstep (d :: Nat) :: *
type instance Lstep  1  = Word8
type instance Lstep  2  = Word8
type instance Lstep  3  = Word8
type instance Lstep  4  = Word8


-- * Classes of functions for working with paths
------------------------------------------------------------------------------
class Lindex t where
  reify :: t -> Int
  lpath :: Int -> t
  lroot :: t

------------------------------------------------------------------------------
-- | For data-types that have layers.
class Layered t where
  depth :: t -> Int


-- unpath _ 0 = error "Lpath.Base.unpath: not a valid `Lkey`"
-- unpath _ 1 = L2 15


-- * Path Helpers
------------------------------------------------------------------------------
-- | Convert a path into `(i, j)` coordinates.
--   NOTE: Needs to know the maximum depth, so that shorter paths are scaled
--     accordingly.
unpath :: Int -> Int -> V2 Int
unpath _ 1 = 0
unpath d p = fi . (<<% s) <$> V2 (x >>% 32) (x .&. 0x0FFFFFFFF)
  where
    x = unshuffle $ p .&. (1 <<% n - 1)
    s = d - n >>% 1
    n = msb (w64 p)

-- | Construct a path from the given row and column indices.
--   NOTE: Needs to know the coordinate depth, to resolve ambiguities.
topath :: Int -> V2 Int -> Int
topath 0 _  = 1
topath d ij = fromIntegral $ 1 <<% (d+d) .|. shuffle x
  where
    V2 i j = w64 <$> ij
    x = i <<% 32 .|. j .&. 0x0FFFFFFFF

------------------------------------------------------------------------------
-- | Path length (in bits).
plen' :: Int -> Int
plen'  = msb . w64
{-# INLINE[2] plen' #-}

-- | Path length, in tree nodes.
plen :: Int -> Int
plen  = (>>% 1) . plen'
{-# INLINE[1] plen #-}
