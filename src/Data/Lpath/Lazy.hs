{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
StandaloneDeriving, InstanceSigs,
TypeSynonymInstances, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Lpath.Lazy
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Lazy-list based paths through linear (2^d-)trees
-- 
-- Changelog:
--  + 23/06/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Lpath.Lazy
--   ( module Data.Lpath.Base
  ( module Data.Index.ZCurve
  , module Data.Lpath.Lazy
  ) where

import GHC.TypeNats
import Data.Bits.Extra

-- import Data.Lpath.Base
import Data.Index.ZCurve


-- * Path datatypes
------------------------------------------------------------------------------
-- | Stores a list of each quadrant/octant/etc. that the path passes through
newtype Lpath (d :: Nat) = Lpath { lsteps :: [Lstep d] }


-- * Instances for paths
------------------------------------------------------------------------------
instance Eq (Lpath 1) where
  Lpath xs == Lpath ys = xs == ys

instance Eq (Lpath 2) where
  Lpath xs == Lpath ys = xs == ys

instance Eq (Lpath 3) where
  Lpath xs == Lpath ys = xs == ys

instance Eq (Lpath 4) where
  Lpath xs == Lpath ys = xs == ys

------------------------------------------------------------------------------
-- | Convert to/from `Int` representations
instance Lindex (Lpath 2) where
  reify  (Lpath xs) =
    let go p (y:ys) = let p' = (p <<% 2) .|. fromIntegral (y .&. 3)
                      in  go p' ys
        go p     _  = p
    in  go 1    xs
  lpath      = 
    let go x
          | x  <  4   = []
          | otherwise = fromIntegral (x .&. 3) : go (x >>% 2)
    in  Lpath . reverse . go    -- TODO
--   lroot :: Lpath d
  lroot  = Lpath []

instance Layered (Lpath d) where
  depth :: Lpath d -> Int
  depth (Lpath xs) = length xs

-- ** Standard instances
------------------------------------------------------------------------------
instance Show (Lpath 2) where
  show (Lpath xs) = "L:" ++ show xs


-- * Paths functions
------------------------------------------------------------------------------


-- ** Path queries
------------------------------------------------------------------------------
