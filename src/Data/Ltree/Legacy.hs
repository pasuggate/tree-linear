import Data.Lpath.Lazy

-- | Aliases for the types used by this linear quadtree implementation.
type Lnode = Lpath 2
type Ldemo = IntSet

pattern Lnode :: forall (d :: Nat). [Lstep d] -> Lpath d
pattern Lnode xs = Lpath xs

------------------------------------------------------------------------------
-- | Compute a `Lnode` from a Z-index.
lnode :: Int -> Lnode
lnode  = lpath
{-# INLINE lnode #-}

-- | Reify a `Lnode` to a Z-index.
reefy :: Lnode -> Int
reefy  = reify
{-# INLINE reefy #-}

ancestors' :: IntSet -> IntSet
ancestors'  =
  let go xs i = xs \/ Set.fromList (reefy <$> lineage' i)
  in  foldl go bare . fmap lnode . flat

lineage' :: Lnode -> [Lnode]
lineage' (Lpath xs) = init $ Lnode <$> inits xs

-- TODO: make generic?
progeny' :: Int -> [Int]
progeny' p = [(p<<%2) .|. i | i <- [0..3]]

-- TODO: more efficient to use `Int`-repr. operations
offspring' :: Int -> IntSet
offspring' i =
  let Lnode xs = lnode i
      ps = reefy . Lnode . (xs++) <$> [[0], [1], [2], [3] :: [Word8]]
  in  Set.fromList ps

parent' :: Int -> Int
parent'  = reefy . Lnode . init . lsteps . lnode

------------------------------------------------------------------------------
-- | Reorder the system of equations to be ready for solving.
rearrange :: V3 Z -> Box (Z `Either` V3 Z) -> Matrix R ->
             (Box (Z `Either` V3 Z), Matrix R)
rearrange p@(V3 _ _ h) ns m =
  let -- ^ move the pivot to the top-left position:
      k   = fromJust $ Box.findIndex (Right p==) ns -- ^ find pivot
      ns' = ns Box.// [(0,ns!k), (k,ns!0)]
      -- ^ drop rows corresponding to (non-pivot) fine inodes:
      js  = 0:[j | j <- [1..H.cols m-1], j>=H.rows m || isLeft (ns'!j)]
      ks  = 0:[i | i <- [1..H.rows m-1], isLeft (ns'!i)]
      m'  = pivot k m ? ks ¿ js
      nx  = ns' `Box.backpermute` Box.fromList js
      -- ^ for all rows of the diagonal variables, and that have support from
      --   coarse inodes which also support the pivot, swap columns:
      (c,r) = (H.cols m', H.rows m')
      go j x l | j == c        = x              -- end of row
               | isLeft (nx!j) = go (j+1) x l   -- dnodes stay on RHS
               | h' < h-1      = go (j+1) x l   -- too coarse
               | a/=0 && b/=0  = swapCols j l x -- move coarse inode
               | otherwise     = go (j+1) x l   -- try next column
               where (a, b) = (x `H.atIndex` (0, j), x `H.atIndex` (l, j))
                     h' = either (const h) (^._z) $ nx!j
  in  (nx,) $ foldl' (go 0) m' $ [0..r-1]

------------------------------------------------------------------------------
-- | Compute the neighbourhood interpolation function (NIF) for the given
--   basis-function coordinate (and, typically, a basis function of a patch).
--   
--   TODO: recursive resolution of "unodes" (inodes from coarse dnodes).
interpfn :: V3 Z -> Dnodes a -> (Box (Z `Either` V3 Z), Vector R)
interpfn p ds =
  let ns = Box.fromList $ p `patchNbrs` ds
      m  = echelon $ odds p
  in  second (head . H.toRows . rref) $ rearrange p ns m

-- | Recursive version of the above.
subdMatRec :: [Bool] -> Matrix R
subdMatRec  =
  let ex = H.vector [0.125, 0.75, 0.125]
      o0 = H.vector [0.5, 0.5, 0]
      ox = H.vector [0.5, 0.5]
      o1 = H.vector [0, 0.5, 0.5]
      go    []  = [H.vector [1]]
      go (o:os) = let x = case o of
                            False -> [o0, ex, o1]
                            True  -> [ox]
                  in  H.flatten <$> (H.outer <$> go os <*> x)
  in  H.fromRows . go


-- * Row & column operations
------------------------------------------------------------------------------
-- | Exchange the columns corresponding to the `j` & `k` indices.
swapCols :: Element t => Int -> Int -> Matrix t -> Matrix t
swapCols j k m =
  let js = Vec.enumFromN 0 (H.cols m) // [(j,k), (k,j)]
  in  m ¿ Vec.toList js

-- | Exchange the rows corresponding to the `j` & `k` indices.
swapRows :: Element t => Int -> Int -> Matrix t -> Matrix t
swapRows j k m =
  let js = Vec.enumFromN 0 (H.rows m) // [(j,k), (k,j)]
  in  m ? Vec.toList js

------------------------------------------------------------------------------
-- | Move "variable `i`" to the top-left position, within the matrix.
pivot :: Element t => Z -> Matrix t -> Matrix t
pivot i = swapCols 0 i . swapRows 0 i

------------------------------------------------------------------------------
-- | Move the indicated column to the end(-column position) of the matrix.
colToEnd :: Element t => Z -> Matrix t -> Matrix t
colToEnd j m = let cs = Box.fromList $ H.toColumns m
                   js = Box.fromList $ [k | k <- [0..H.cols m-1], k/=j] ++ [j]
  in  H.fromColumns $ Box.toList $ cs `Box.backpermute` js

-- | Move the indicated row to the end(-row position) of the matrix.
rowToEnd :: Element t => Z -> Matrix t -> Matrix t
rowToEnd i m = let rs = Box.fromList $ H.toRows m
                   ks = Box.fromList $ [k | k <- [0..H.rows m-1], k/=i] ++ [i]
  in  H.fromRows $ Box.toList $ rs `Box.backpermute` ks

{-- }
-- TODO:
------------------------------------------------------------------------------
-- | Move the indicated column to the end(-column position) of the matrix.
moveColTo :: Element t => Z -> Z -> Matrix t -> Matrix t
moveColTo j k m = let cs = Box.fromList $ H.toColumns m
                      js = Box.fromList $ [k | k <- [0..H.cols m-1], k/=j] ++ [j]
  in  H.fromColumns $ Box.toList $ cs `Box.backpermute` js
--}

rearrange' :: V3 Z -> Box Pnode -> Matrix R -> (Box Pnode, Matrix R)
rearrange' p ns m =
  let -- ^ move the pivot to the top-left position:
      k   = fromJust $ Box.findIndex (Pivot p==) ns -- ^ find pivot
      ns' = ns Box.// [(0,ns!k), (k,ns!0)]
      -- ^ drop rows corresponding to (non-pivot) fine inodes:
      js  = 0:[j | j <- [1..H.cols m-1], not (xtest (ns'!j))]
      ks  = 0:[i | i <- [1..H.rows m-1], not (xtest (ns'!i))]
      m'  = pivot k m ? ks ¿ js
      nx  = ns' `Box.backpermute` Box.fromList js
      -- ^ for all rows of the diagonal variables, and that have support from
      --   coarse inodes which also support the pivot, swap columns:
      (c,r) = (H.cols m', H.rows m')
      go j x l | j == c       = x              -- end of row
               | dtest (nx!j) = go (j+1) x l   -- dnodes stay on RHS
               | itest (nx!j) = go (j+1) x l   -- too coarse
               | a/=0 && b/=0 = swapCols j l x -- move coarse inode
               | otherwise    = go (j+1) x l   -- try next column
               where (a, b) = (x `H.atIndex` (0, j), x `H.atIndex` (l, j))
  in  (nx,) $ foldl' (go 0) m' $ [0..r-1]

interpFn' :: V3 Z -> PatchBuilder a -> (Box Pnode, Vector R)
interpFn' p pb =
  let (ns, pb') = first Box.fromList $ p `patchNodes` pb
      m  = echelon $ odds p
      (ps, cs) = second (head . H.toRows . rref) $ rearrangeO p ns m
      ps' = Box.ifilter (\i _ -> cs!i /= 0) ps
      cs' = Vec.filter (/= 0) cs
      (qs, ks) = (tal ps', Vec.map negate $ tal cs')
  in  (ps', cs')


-- * Testing & examples
------------------------------------------------------------------------------
testLmask :: IO ()
testLmask  = do
  let mato = echelon [True, False]
      mat  = snd $ rearrange p cps mato
      (p, h, s) = (V3 32 96 11, p^._z, 1 <<% (maxDepth-h))
      cps  = Box.fromList [ Right (p ^-^ V3 0 s 0), Right p, Left 42
                          , Left 7, Left 6, Left 4, Right (p ^-^ V3 s 0 2)
                          , Right (p^-^V3 (2*s) (-s) 1), Right (p^+^V3 (2*s) s (-1))]
      mat' = rref mat
  putStrLn "\nOriginal matrix:"
  putStrLn $ H.dispf 2 $ H.cmap (*16) mato
  putStrLn "\nRearranged matrix:"
  putStrLn $ H.dispf 2 $ H.cmap (*16) mat
  putStrLn "\nRREF matrix:"
  putStrLn $ H.dispf 3 $ H.cmap (*16) mat'

-- | Compute the row & column permutation vectors, prior to calculating the
--   RREF of the interpolation matrix.
reorder :: Z -> Box Pnode -> ([Z], [Z])
reorder r ps =
  let k  = fromJust $ Box.findIndex (not . isn't _Pivot) ps -- ^ find pivot
      -- ^ permutation vector for rows
      ks = k:[i | i <- [0..r-1], isn't _Pivot (ps!i) && isn't _Xnode (ps!i)]
      -- ^ permutation vector for columns
      go  Xnode    = False -- drop Xnodes and Pivots
      go (Pivot _) = False
      go (Bnode _) = False -- bdys and DoF's are moved to the end
      go (Dnode _) = False
      go (Inode _) = False -- here, inodes are equivalent to dnodes
      go        _  = True
      fo  Xnode    = False
      fo (Pivot _) = False
      fo (Vnode _) = False
      fo        _  = True
      js = [0..] `zip` toList ps
      ji = filter (go . snd) js
      jf = filter (fo . snd) js
  in  (ks, (k:) $ fst <$> ji ++ jf)


-- * Some convenience aliases
------------------------------------------------------------------------------
-- | A neighbour can either be a dof-node, or an inferred node.
type Nbr = Either Int (V3 Int)

------------------------------------------------------------------------------
-- | Stores the neighbourhood for some (inferred or dof) CP.
data Nbrhd =
  Nbrhd { cpcoord :: V3 Int
        , bdytest :: [Bool] -- TODO:
        , coarses :: [Bool]
--         , nbrtype :: Ntype
        , nbrlist :: [Nbr]
--         , nbrlist :: [(Bool, Nbr)]
        } deriving (Eq, Show)


------------------------------------------------------------------------------
-- | Compute the neighbourhood of the given node.
nbrhd :: V3 Int -> Lnodes -> Nbrhd
nbrhd xy ls =
  let bs = bros xy
      fs = flos xy
      cs = map (const False) fs ++ map (const True) bs
      ps = posmap ls
      go p = maybe (Right p) Left $ get3d p ps
  in  Nbrhd xy (belongs xy) cs $ go <$> fs ++ bs

------------------------------------------------------------------------------
-- | Compute the neighourhoods for each node of a patch.
--   NOTE: the order of the nodes are fine nodes, and then coarse nodes.
--   NOTE: (out of) boundary nodes receive their negated Z-index, and their
--     level must always be that of the patch's.
patchNbrs :: V3 Z -> Dnodes a -> [Z `Either` V3 Z]
patchNbrs p ds
  | p <? ds   = pure . Left . fst . fromJust $ p <~ ds
  | otherwise = fs ++ cs
  where
    (cs, fs) = (ho . go <$> bros p, bo . go <$> flos p)
    ho = either Left (Right . flip coarsen ds)
    bo = either Left (\p -> Right p `bool` Left (bnode p) $ ghost p)
    go p = maybe (Right p) (Left . fst) $ p <~ ds


-- * Pnode queries
------------------------------------------------------------------------------
ptest :: Pnode -> Bool
ptest (Pivot _) = True
ptest        _  = False

xtest :: Pnode -> Bool
xtest  Xnode    = True
xtest        _  = False

vtest :: Pnode -> Bool
vtest (Vnode _) = True
vtest        _  = False

dtest :: Pnode -> Bool
dtest (Dnode _) = True
dtest        _  = False

itest :: Pnode -> Bool
itest (Inode _) = True
itest        _  = False

{-- }
  Box.ifoldl' (\(qf, pp) i p -> case p of
                  Xnode   -> (qf, pp)
                  Dnode j -> ((j, ks!i) ~> qf, pp)
                  Vnode v -> let (q, pp') = bool (p, pp) (go p pp) $ isInode o v (pp^.dnodes)
                             in  case q of
                                   Vnode _ -> (qf, pp)
                                   Inode g -> (Map.unionWith (+) qf $ Map.map (*ks!i) g, pp')
                                   _ -> error "resolveInode: unknown"
                  Pivot _ -> (qf, pp)
                  Bnode j -> ((j, ks!i) ~> qf, pp)
                  Inode g -> (Map.unionWith (+) qf $ Map.map (*ks!i) g, pp)
              ) (bare, pb) ps
  where
    go :: Pnode -> PatchBuilder a -> (Pnode, PatchBuilder a)
    go p pp = maybe (p, pp) (flip resolveInode pp) $ p^?_Vnode
--}
