{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms, ScopedTypeVariables, TupleSections,
StandaloneDeriving,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Bnode
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Boundary-nodes data-structures and functions, for linear 2^d-trees
-- 
-- Changelog:
--  + 29/07/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Bnode where

import GHC.Generics (Generic)
import GHC.TypeNats
import Control.Arrow (second, (&&&), (***))
import Control.Lens (makeLenses)
import Data.Foldable (toList, foldl')
import Data.Tuple (swap)
-- import Data.Maybe (fromJust)
import Data.IntSet.Sets as Set
import qualified Data.IntSet as Set
import Data.IntMap.Maps as Map
import qualified Data.IntMap as Map
import Data.IntMap.ZMaps
import Linear (V3 (..))
-- import Data.Vector.Storable (Vector)
import Data.Vector.Helpers

import Data.Bits.Extra
import Data.Lpath.Lazy
import Data.Ltree.Lbase
import Data.Ltree.Dnode


-- * Some convenience aliases
------------------------------------------------------------------------------
-- | Stores the boundary-node, and out-of-domain data for non-DoF nodes/CP's.
--   TODO: use `d`
data Bnodes (d :: Nat) =
  Bnodes { _bdymap :: ZMap   (d+1) Z
         , _bidmap :: IntMap (ZVec (d+1), ([Bool], Btype d))
         , _bcount :: Z
         }

-- deriving instance Eq   (ZMap 3 Z) => Eq   (Bnodes 2)
-- deriving instance Show (ZMap 3 Z) => Show (Bnodes 2)

deriving instance Eq   (Bnodes 1)
deriving instance Eq   (Bnodes 2)
deriving instance Eq   (Bnodes 3)

deriving instance Show (Bnodes 1)
deriving instance Show (Bnodes 2)
deriving instance Show (Bnodes 3)

------------------------------------------------------------------------------
-- | Enumerated type for each supported boundary-condition type.
--   TODO: functions & coefficients for the boundaries?
data Btype (d :: Nat)
  = Dirichlet
  | Neummann
--   | Robin   a a a
--   | Mixed
--   | Cauchy  a a a
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic)

makeLenses ''Bnodes


-- * Bnode instances
------------------------------------------------------------------------------

-- ** Set-like instances
------------------------------------------------------------------------------
-- | Bnode-set insertion function.
--   TODO:
instance SetInsert (V3 Int) (Bnodes 2) where
  p ~> Bnodes ps bs bc | p <? ps   = Bnodes ps  bs  bc
                       | ghost p   = Bnodes ps' bs' bc'
                       | otherwise = Bnodes ps  bs  bc
    where ps' = (p, bc) ~> ps
          bs' = (bc, (p, (insides p, Dirichlet))) ~> bs
          bc' = pred bc

-- | Bnode-set query and remove functions.
instance SetIndex (V3 Int) (Bnodes 2) where
  p <? Bnodes ps  _  _ = p <? ps
  p <\ Bnodes ps bs bc = case p <~ ps of
    Nothing -> Bnodes     ps      bs  bc
    Just b  -> Bnodes (p<\ps) (b<\bs) bc

-- | Construct initial `Bnodes`.
instance SetBuild (V3 Int) (Bnodes 2) where
  bare = Bnodes bare bare (-1)
  unit p | ghost p   = p ~> bare
         | otherwise = bare

-- | Fetch bnodes (by coordinate) from the set of bnodes.
instance SetElems (V3 Int) (Maybe Int) (Bnodes 2) where
  p <~ Bnodes ps _ _ = p <~ ps


-- * Node-map constructors
------------------------------------------------------------------------------
makeBnodes :: Ltree 2 -> Bnodes 2
makeBnodes (Ltree t) =
  let qs = filter ghost $ concatMap (toList . qnodes) ts
      ts = flat $ Set.filter bcell t :: [Z]
  in  foldl' (\bx q -> q ~> bx) bare qs

{--}
makeBnodes' :: IntSet -> Bnodes 2
makeBnodes' t = let t' = Ltree $ Set.filter bcell t
                    (bc, bs) = negate *** fmap negate $ coeffsWith ghost 1 t'
                    go = second (id &&& (,Dirichlet) . insides) . swap
                    bi = Map.fromList $ go <$> flat bs
                in  Bnodes bs bi bc
--}

-- ** Constructor helpers
------------------------------------------------------------------------------
bnodesList :: [V3 Z] -> [V3 Z]
bnodesList  = filter ghost

-- FIXME: negative coordinates will cause problems!?
-- bnode :: V3 Z -> Int
-- bnode  = negate . reify

-- | Is the given cell a boundary-cell?
bcell :: Int -> Bool
bcell q = u == 0 || v == 0 || u == m || v == m
  where
    (V3 u v h) = lpath q
    m = (2^h-1) <<% (maxDepth-h)
