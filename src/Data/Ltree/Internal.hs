{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Internal
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Lazy-list based paths through linear (2^d-)trees
-- 
-- Changelog:
--  + 24/06/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Internal where

import Type.Natural
import GHC.Word
import Data.IntMap as IntMap
import Linear

import Data.Lpath.Lazy


-- * Linear tree data types
------------------------------------------------------------------------------
-- type family   Lbox (d :: Nat) :: * -> *
-- type instance Lbox  2 = V4
-- data Lcell d a = Lcell  (Lbox  d a)
-- type Ltree d a = IntMap (Lcell d a)


data Lcell2 a = Lcell  { lpos :: Lpath 2, ldat :: a }
type Ltree2 a = IntMap (Lcell2 a)
type Lpath2   = Lpath 2


-- * Linear tree queries
------------------------------------------------------------------------------
-- | Compute the layer of the deepest leaf-node, of the (upside-down) tree
instance Layered (Ltree2 a) where
  depth :: Ltree2 a -> Int
  depth  = let p x = lpath x :: Lpath 2
           in  maybe (-1) (depth . p . fst) . IntMap.lookupMax


------------------------------------------------------------------------------
-- | Return the list of adjacent tree leaves (nodes/octants/etc?), and that
--   are on the same level.
peers :: Lpath2 -> Ltree2 a -> [Lpath2]
peers p t = error "peers: unimplemented"

-- | Returns `True` for non-leaf nodes as well.
--   NOTE: Tests if the prefix (computed from `p`) exists within the linear
--     tree.
--   TODO: may be difficult to implement, when using `IntMap's?
contains :: Ltree2 a -> Lpath2 -> Bool
contains t p = error "contains: STUB"

-- | Find any touching cells, and return them.
--   TODO: quite hard to implement?
adjacent :: Lpath2 -> Ltree2 a -> [Lpath2]
adjacent p t = error "adjacent: not yet implemented"
