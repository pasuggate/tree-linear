{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Ldemo
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Lazy-list based paths through linear (2^d-)trees
-- 
-- Changelog:
--  + 24/06/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Ldemo where

import GHC.Word
import Control.Monad (when)
import Control.Arrow (first, second)
import Control.Lens ((^.), (.~), (%~))
import Data.Foldable (foldl', toList)
import Data.Bool (bool)
import Data.Maybe (fromJust, catMaybes)
import Data.List (intersperse)
import Data.IntSet.Sets as Set
import qualified Data.IntSet as Set
import Data.IntMap.Maps as Map
import qualified Data.IntMap as Map
import Data.List (inits, nub)
import Linear (_xy, V2 (..), V3 (..), V4 (..), (*^))
import Data.Bits.Extras (msb, w64)
import qualified Data.Vector as Box
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import Data.Vector.Helpers
import Numeric.LinearAlgebra (Element, Matrix, (?), (¿), (|||)) -- ^ HMatrix
import qualified Numeric.LinearAlgebra as H
import Data.Matrix.Dense
import Data.CCSMat (CCSMat (..))
import qualified Data.CCSMat as CCS
import qualified Data.CCSMat.Stream as CCS
import Text.Printf

import Data.Bits.Extra
import Data.Lpath.Lazy
import Data.Ltree.Lbase
import Data.Ltree.Dnode
import Data.Ltree.Inode
import Data.Ltree.Bnode
import Data.Ltree.Lelem
import Data.Ltree.Lnbrs
import Data.Ltree.Lmask
import Data.Ltree.Patch


-- * Testing & examples
------------------------------------------------------------------------------
-- | Mesh where the cells get smaller towards the boundary
escher :: Int -> Ltree 2
escher d =
  let es = [V2 i j | i<-[0..l], j<-[0..l], i==0 || i==l || j==0 || j==l]
      l  = 2^d-1
  in  arearule . Ltree . Set.fromList $ topath d <$> es

-- | Generate a refined-mesh that has peak-refinement at the centre.
peak :: Int -> Ltree 2
peak d =
  let es = [V3 x y d | y <- [c-s,c], x <- [c-s,c]]
      (s, c) = (2^(maxDepth-d), 2^(maxDepth-1))
  in  arearule . Ltree . Set.fromList $ fmap qpath es

fp :: V3 Int -> V2 Double
fp  = fmap ((*2^^(-maxDepth)) . fromIntegral) . (^._xy)

fx :: Z -> V3 Z -> V3 Z
fx s = _xy %~ fmap (>>% s)

-- meq :: (Ord a, Fractional a, Storable a) => a -> CCSMat a -> CCSMat a -> Bool
meq :: R -> CCSMat R -> CCSMat R -> Bool
meq eps (CCS an ai ac ax) (CCS bn bi bc bx)
  | an /= bn = error $ printf "meq: rows differ (%d /= %d)\n" an bn
  | ai /= bi = error $ printf "meq: rows indices differ (%s /= %s)\n" (show ai) (show bi)
  | ac /= bc = error $ printf "meq: col-pointers differ (%s /= %s)\n" (show ac) (show bc)
  | ssr > eps = error $ printf "meq: error to great (%g)\n%s\n\n%s\n\n%s\n" ssr (show res) (show ax) (show bx)
  | otherwise = ssr < eps
  where
    res = Vec.zipWith (\a b -> (a-b)^2) ax bx
    ssr = (*recip (fromIntegral $ len ax)) $ Vec.sum res

------------------------------------------------------------------------------
-- | Build the initial testing dataset
testSetup :: Bool -> Z -> (Ltree 2, PatchBuilder 2 ())
testSetup e l = (lt, newBuilder ds bs)
  where
    lt = uniform l `bool` escher l $ e
    ds = makeLnodes lt
    bs = makeBnodes lt

showEchelon :: IO ()
showEchelon  = do
  let pp = snd $ testSetup True 6
      p0 = V3 28160 28160 6
      mm = echelon $ odds p0
  print mm

dumpSetup :: IO ()
dumpSetup  = do
--   let (lt, pp) = testSetup True 5
  let (lt, pp) = testSetup False 2
      (lx, px) = buildPatches lt pp
  print lt
--   print lx

-- | Builds a simple SMURCCS mesh (initially represented as an `Ltree`), and
--   then does some stuff with it.
testLdemo :: IO ()
testLdemo  = do
--   let lt = escher 6
--   let lt = escher 5
  let lt = uniform 2
      ds = makeLnodes lt
      bs = makeBnodes lt
--       bt = makeBnodes' lt
      ps = rawPatches lt
      p  = fromJust $ 268 <~ ps
--       p0 = last p
--       p0 = last (init p)
--       p0 = V3 8192 8192 4
--       p0 = V3 512 512 7
--       p0 = V3 1024 1024 6
--       p0 = V3 768 512 7
--       p0 = V3 31232 31232 6
--       p0 = V3 31744 31744 6
      p0 = V3 28160 28160 6
      mato = echelon $ odds p0
      pp  = newBuilder ds bs
      cps = Box.fromList . fst $ p0 `patchNodes` pp
      (qs, pb) = p0 `interpFn` pp
--       di = pb^.dnodes^.idxmap
--       bi = pb^.bnodes^.bidmap
--       (nx,mat) = second rref $ rearrange p0 cps mato

--   printf "\nBnodes:\n%s\n" (show bs)
  printf "\nBnodes:\n%s\n" (unlines $ show <$> flat (bs^.bidmap))
--   print $ bs == bt

  printf "\nDnodes:\n%s\n" (unlines $ show <$> flat (ds^.idxmap))

{-- }
  putStrLn "\nBoundaries:"
--   print $ coord2list $ bdymap bs
  print $ Map.keysSet $ bidmap bs
  putStrLn "\nNeighbourhood:"
--   print $ either Left (Right . fx 10) <$> ns
  putStrLn "\nPnodes:"
  print p0
  putStrLn " + patchNodes:"
  printf "\tflos: %s\n" (show $ flos p0)
  printf "\tbros: %s\n" (show $ bros p0)
  print cps
  putStrLn " + reordered:"
  print $ reorder (H.rows mato) cps
  print qs
  mapM_ `flip` flat qs $ \(i, c) -> do
    let x = maybe (fst $ fromJust $ i <~ bi) id $ i <~ di
    printf " + coord:\t%d \t(%s \t= %f)\n" i (show x) (c*4096)
  putStrLn "\n\nInodes:"
  mapM_ `flip` (coord2list . refmap $ pb^.inodes) $ \(p, x) -> do
    printf "%s\t\t%s\n" (show p) (show x)
--   putStrLn "\nRearranged matrix:"
--   putStrLn $ H.dispf 2 $ H.cmap (*64) mat
--}

  -- ^ construct patch transforms:
--   let (uv, q0) = fromJust $ Map.lookupMax ps
  let (uv, q0) = (qpath $ V3 28672 28672 5, fromJust $ uv <~ ps)
  let ((ls, im), pb') = patchTransform q0 pp
      (lm, pq) = foldr (\qp (xx, qq) -> (:xx) `first` patchTransform qp qq
--                        ) ([], pp) $ take 18 $ Map.elems ps
                       ) ([], pp) $ Map.elems ps
      (vs, ms) = unzip lm
      ns = fst $ foldl' (\(xs, qq) p -> let (x, qq') = patchNodes p qq
                                        in  (xs++[(p,x)], qq')) ([], pp) q0
{-- }
  putStrLn "\n\nTransform:"
  let hi = im `CCS.multiply` CCS.unsafeTranspose im
--   let hi = im `CCS.mulS` CCS.unsafeTranspose im
--   let hi = CCS.unsafeTranspose im `CCS.multiply` im
  CCS.printCCS hi "I"
  print hi
  printf " + coarse?:\t%s\n" $ show $ isCoarse uv lt
  print q0
  mapM_ `flip` ns $ \(q,xs) -> do
    print $ codeps q ++ aboves q
    print $ fst $ pnode q q pp
    printf "> %s:\t%s\n" (show q) (show xs)

  print $ qnodes uv
  printf " + patch ID:\t%d\t(loc = %s)\n" uv $ show $ coord uv
  print ls
  CCS.printCCS im "A"
--}

  print `mapM_` ps
  print `mapM_` vs

{-- }
  mapM_ `flip` zip [0..] ms $ \(i, m) -> do
    CCS.printCCS m $ printf "Q[%d]" (i :: Z)
    when (CCS.getNNZ m == 0) $ print (vs!!i)
  printf "\n\nInodes:\n%s\n" $ concat $ intersperse "\n" $ show <$> flat (pq^.inodes^.refmap)
  let (vz, mz) = unzip $ fst . flip patchTransform pp <$> Map.elems ps
--       ts = zipWith (==) ms mz
      ts = zipWith (meq 1e-10) ms mz
  mapM_ `flip` zip [0..] mz $ \(i, m) -> do
    CCS.printCCS m $ printf "R[%d]" (i :: Z)

  mapM_ `flip` zip [0..] ts $ \(i, b) -> when (not b) $ do
    printf "\nMatrix[%d]:\n" i
    CCS.printCCS (ms!!i) $ printf "Q[%d]" (i :: Z)
    print (ms!!i)
    CCS.printCCS (mz!!i) $ printf "R[%d]" (i :: Z)
    print (mz!!i)

  printf "\nBnodes: pre = %d, post = %d\n" (negate $ pp^.bnodes^.bcount) (negate $ pq^.bnodes^.bcount)
  printf "\nMatching Interp-Mats: %d (failing: %d)\n"
    (length $ filter id ts) (length $ filter not ts)
--}

--   print (toVec ds :: Maybe (Box ()))
