{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, GADTs, TypeOperators,
MultiParamTypeClasses, TypeFamilies, FunctionalDependencies,
FlexibleContexts, FlexibleInstances, InstanceSigs, TypeSynonymInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms, ScopedTypeVariables, TupleSections,
TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Lelem
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Node maps for linear 2^d-trees
-- 
-- Changelog:
--  + 21/07/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Lelem where

-- import GHC.Word
-- import Data.Foldable (toList)
import Data.Maybe (fromJust)
import Data.Bool (bool)
import Data.IntSet.Sets as Set
import qualified Data.IntSet as Set
import Data.IntMap.Maps as Map
-- import qualified Data.IntMap as Map
-- import Data.List (inits, nub)
import Linear (V3 (..), V4 (..))
-- import Data.Bits.Extras (msb, w64)
-- import Data.Vector.Helpers

import Data.Bits.Extra
import Data.Lpath.Lazy
import Data.Ltree.Lbase
import Data.Ltree.Dnode


-- * Element & patch data-types
------------------------------------------------------------------------------
-- | Data-type for linear-tree patch-types, where:
--    + `Normal` patches have (p+1)^d CP's; and
--    + `Coarse` patches have (p+2)^d CP's,
--   because `Coarse` patches are adjacent to finer patches, and so are
--   subdivided prior to sampling/quadrature.
data Ptype = Normal | Coarse
  deriving (Eq, Ord, Enum, Bounded, Read, Show)


class HasPtype p t | p -> t, t -> p where
  ptype :: p -> t -> Ptype


------------------------------------------------------------------------------
-- | Does a patch have to be integrated at a finer level?
instance HasPtype Int (Ltree 2) where
  ptype :: Int -> Ltree 2 -> Ptype
  ptype p = bool Normal Coarse . isCoarse p


-- * Convenience functions
------------------------------------------------------------------------------
-- | Convert a Z-index for a quadtree node/leaf to a `(x,y,d)`-coordinate.
coord :: Int -> V3 Int
coord  = lpath
{-# INLINE[2] coord #-}

-- | Convert a `(x,y,d)`-coordinate for a quadtree node/leaf to a Z-index.
qpath :: V3 Int -> Int
qpath  = reify
{-# INLINE[2] qpath #-}


-- * Constructors
------------------------------------------------------------------------------
-- | Compute the set of quadrilateral elements from the linear quadtree.
elems :: Ltree 2 -> [V4 Int]
elems t =
  let idx p = fromJust $ p <~ coeffs t
  in  reverse $ foldl (\es -> (:es) . fmap idx . qnodes) [] $ flat $ _unLtree t

-- ** Constructor helper-functions
------------------------------------------------------------------------------
rawPatches :: Ltree 2 -> IntMap [V3 Int]
rawPatches lt =
  let go = flip rawPatch lt
  in  Set.foldl' (\m q -> (q, go q) ~> m) bare $ _unLtree lt

rawPatch :: Int -> Ltree 2 -> [V3 Int]
rawPatch p lt | isCoarse p lt = coarse p
              | otherwise     = normal p
{-# INLINE[1] rawPatch #-}


-- * Patch operations
------------------------------------------------------------------------------
-- | Compute the coordinates of the CP's of a normal patch.
--   NOTE: row-major ordering (and not Z-ordering).
--   NOTE: no boundary clipping.
normal :: Int -> [V3 Int]
normal p =
  let V3 x y d = coord p
      s = 2^(maxDepth-d)
  in  [V3 a b d | b <- [y-s,y..y+2*s], a <- [x-s,x..x+2*s]]

-- | Compute the coordinates of the fine CP's of a coarse patch.
--   NOTE: row-major ordering (and not Z-ordering).
--   NOTE: no boundary clipping.
coarse :: Int -> [V3 Int]
coarse p =
  let V3 x _ d = coord p
      s = 2^(maxDepth-d-1)
  in  [V3 a b (d+1) | b <- [x-s,x..x+3*s], a <- [x-s,x..x+3*s]]


-- * Queries
------------------------------------------------------------------------------
-- | Checks to see if the given quadtree node is a "coarse" leaf node.
--   NOTE: coarse nodes need to be subdivided prior to sampling/quadrature.
--   NOTE: see notes @23/07/2018 for an explanation.
isCoarse :: Int -> Ltree 2 -> Bool
isCoarse p t =
  let V3 x y d = coord p
      (dw, ds) = (2^(maxDepth-d-1), 2^(maxDepth-d))
      qs = [V3 a b (d+1) | b <- (y+) <$> [-dw,0,ds], b>=0 && b<2^maxDepth
                         , a <- (x+) <$> [-dw,0,ds], a>=0 && a<2^maxDepth
                         , not (a==x && b==y)]
  in  any ((<?t) . qpath) qs

-- | Does the Z-index correspond to an element that is adjacent to the
--   boundary?
isBorder :: Int -> Bool
isBorder p = let V3 x y h = coord p
                 w = 1 <<% maxDepth
                 s = w >>% h
             in  x == 0 || y == 0 || x+s == w || y+s == w


-- * Testing & examples
------------------------------------------------------------------------------
-- FIXME: update to using correct indices
testLelem :: IO ()
testLelem  = do
  let lt = Ltree $ Set.fromList [ 4, 6, 7, 20, 21, 22, 23 ]
  print $ arearule lt
  putStrLn "\nCoords:"
  print $ coord <$> flat (_unLtree lt)
  putStrLn "\nCoarse:"
  print $ flip isCoarse lt <$> [4, 6, 7]
