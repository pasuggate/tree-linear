{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms, ScopedTypeVariables, TupleSections,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Patch
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Patch-extraction for linear 2^d-tree representations, of SMURCCS meshes.
-- 
-- Changelog:
--  + 29/07/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Patch
  ( module Data.Ltree.Patch
  , module Data.Ltree.Patch.Pbase
  , module Data.Ltree.Patch.Interp
  ) where

import GHC.Exts (IsList (..))
import Data.Foldable (foldl')
import Data.Bool (bool)
import Data.IntSet.Sets as Set
import qualified Data.IntMap as Map
import Linear (V3 (..))
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
import Text.Printf

import Data.Vector.Helpers
import Data.CCSMat (CCSMat)
import qualified Data.CCSMat as CCS
import qualified Data.CCSMat.Vector as CCS
import qualified Data.Vector.Sparse as SpV

import Data.Ltree.Lbase
import Data.Ltree.Dnode
import Data.Ltree.Inode
import Data.Ltree.Bnode
import Data.Ltree.Lelem
import Data.Ltree.Patch.Pbase
import Data.Ltree.Patch.Interp


-- * Constructors
------------------------------------------------------------------------------
-- | Construct a mesh of patches, from the given linear 2^d-tree.
makePatches :: Ltree 2 -> (Lmesh 2 (Patch 2), PatchBuilder 2 ())
makePatches lt =
  let ds = makeLnodes lt
      bs = makeBnodes lt
      pb = newBuilder ds bs
  in  buildPatches lt pb

-- ** Constructor helpers
------------------------------------------------------------------------------
-- | Build the initial `PatchBuilder` state-variable, from the given DoF nodes
--   data.
--   TODO: OBSOLETE?
makePatchBuilder :: Dnodes 2 a -> Ltree 2 -> PatchBuilder 2 a
makePatchBuilder xs lt =
  let rs = rawPatches lt
      go = fromList . inodesList . concat . Map.elems . (makeNodes xs <$>)
      vs = Inodes $ go rs
      bs = bare
      hx = bare
  in  PatchBuilder xs vs bs hx
{-# DEPRECATED makePatchBuilder "use `makePatches` or `newBuilder`" #-}

------------------------------------------------------------------------------
-- | Constructs a linear 2^d-tree containing the element patches at the leaf-
--   nodes.
buildPatches :: Ltree 2 -> PatchBuilder 2 a ->
  (Lmesh 2 (Patch 2), PatchBuilder 2 a)
buildPatches lt pb = (Lmesh px, pp)
  where
    rs = rawPatches lt
    (px, pp) = foldl' go (bare, pb) $ flat rs
    go (qx, pq) (i, ps) =
      let pt = bool Normal Coarse $ isCoarse i lt
          xy = coord i
          bc = bool Nothing (Just Dirichlet) $ isBorder i
          ((ls, im), pq') = patchTransform ps pq
      in  ((i, Patch xy ls im bc pt) ~> qx, pq')

------------------------------------------------------------------------------
-- | Use the given dnodes to build a patch-builder data-structure.
--   TODO:
newBuilder :: Dnodes 2 a -> Bnodes 2 -> PatchBuilder 2 a
newBuilder ds bs = PatchBuilder ds bare bs bare

-- ** Patch-transform constructors
------------------------------------------------------------------------------
-- | Build the gather/scatter matrix, along with the DoF-indices, for the
--   given patch (and patch-builder).
patchTransform ::
  [V3 Z] -> PatchBuilder 2 a -> ((Vector Z, CCSMat R), PatchBuilder 2 a)
patchTransform ps pb =
  let (fs, pb') = patchFun ps pb
      (ds, ns ) = (labelSet fs, Map.fromList $ flat ds `zip` [0..])
      (ls, rn ) = (Vec.fromList $ flat ds, len ls)
--       go = Map.foldlWithKey (\xs k x -> xs `maybe` ((~>xs) . (,x)) $ k<~ns)
      go = Map.foldlWithKey (\xs k x -> (per k `maybe` id $ k<~ns, x)~>xs)
      cs = SpV.map2vec rn . go bare <$> fs
      per k = error $ printf "patchTransform: node %d missing from neighbourhood" k
  in  ((ls, CCS.transpose $ CCS.fromSparseCols cs), pb')

-- | Generates the NIF's in the correct order, and preserves the patch-node
--   ordering.
patchFun ::  [V3 Z] -> PatchBuilder 2 a -> ([InterpFn], PatchBuilder 2 a)
patchFun ps pb = go ps [] pb
  where
    go    []  fs pp = (fs, pp)
    go (p:qs) fs pp = let (f, pp') = interpFn p pp
                          (gs, qq) = go qs fs pp'
                      in  (f:gs, qq)
