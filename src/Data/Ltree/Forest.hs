{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
StandaloneDeriving,
FunctionalDependencies, ViewPatterns, PatternSynonyms,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Forest
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Implementation of forests of linear trees (inspired partly by `p4est`)
-- 
-- Changelog:
--  + 06/10/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Forest where

import GHC.TypeNats
import GHC.Word
import Control.Lens (makeLenses)
import Data.Vector (Vector)
import Data.Vector.Helpers
import Linear (V2 (..), V4 (..))
import Linear.V

import Data.Ltree.Lbase (Ltree)


-- * Forest (of linear trees) data structures
------------------------------------------------------------------------------
-- | Connectivity-list (or Cell-list) data structure for each of the faces of
--   a cell.
type family   Clist (d :: Nat) a :: *
type instance Clist  1         a = V2  a
type instance Clist  2         a = V4  a
type instance Clist  3         a = V 6 a
type instance Clist  4         a = V 8 a

-- | Geometry-map cell with `d` parametric dimensions, and representing a B-
--   spline (or Bezier) patch with degree `p`.
--   NOTE: the patch has `(p+1)^d` control-points.
newtype Gcell d p a = Gcell { _gcpoints :: Vector a } deriving (Eq, Show)

------------------------------------------------------------------------------
-- | Geometry-map for isogeometric/isoparametric finite element analysis
data GeoMap (d :: Nat) (p :: Nat) a =
  GeoMap { _gcells :: Vector (Gcell d p a)
         , _gconns :: Vector (Clist d Z)
         , _gfaces :: Vector (Clist d Word8)
         }

-- ** Forest data-type
------------------------------------------------------------------------------
-- | Geometry-map for isogeometric/isoparametric finite element analysis
data Forest (d :: Nat) =
  Forest { _ftrees :: Vector (Ltree d)
         , _geomap :: Vector Z
         , _fconns :: Vector (Clist d Z)
         , _ffaces :: Vector (Clist d Word8)
         , _fnodes :: Vector (V d R)
         , _corners :: Vector (Maybe Z)
         }

deriving instance Eq   (Forest 1)
deriving instance Eq   (Forest 2)
deriving instance Eq   (Forest 3)
deriving instance Eq   (Forest 4)
deriving instance Show (Forest 1)
deriving instance Show (Forest 2)
deriving instance Show (Forest 3)
deriving instance Show (Forest 4)
-- deriving instance (Eq (Clist d Z), Eq (Clist d Word8)) => Eq (Forest d)

makeLenses ''Gcell
makeLenses ''GeoMap
makeLenses ''Forest
