{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms, ScopedTypeVariables, TupleSections,
StandaloneDeriving, TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Inode
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Inferred-nodes data-structures and functions, for linear 2^d-trees
-- 
-- Changelog:
--  + 29/07/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Inode where

import GHC.Exts (IsList (..))
import GHC.TypeNats
import Control.Arrow ((>>>))
import Control.Lens (makeLenses)
import Data.IntSet.Sets as Set
import Data.IntMap.Maps as Map
import qualified Data.IntMap as Map
import Data.IntMap.ZMaps
import Linear (V3 (..), V4 (..))
import Data.Vector.Helpers

import Data.Ltree.Dnode
import Data.Ltree.Lbase
import Data.Ltree.Lelem


-- * Some convenience aliases
------------------------------------------------------------------------------
-- | The refined CP is a weighted sum of the given DoF-nodes.
--   TODO: how to handle out-of-domain nodes?
type InterpFn = IntMap R -- (Vector Z, Vector R)

------------------------------------------------------------------------------
-- | Stores the boundary & interpolation data for inferred-nodes.
--   TODO: use `d`
newtype Inodes (d :: Nat) =
  Inodes { _refmap :: ZMap (d+1) InterpFn }

deriving instance Eq   (Inodes 2)
deriving instance Show (Inodes 2)
deriving instance Eq   (Inodes 3)
deriving instance Show (Inodes 3)

makeLenses ''Inodes


-- * Inode instances
------------------------------------------------------------------------------

-- ** Map-like instances
------------------------------------------------------------------------------
instance IsList (Inodes 2) where
  type Item (Inodes 2) = (V3 Z, InterpFn)
  fromList    = Inodes . fromList
  fromListN _ = Inodes . fromList
  toList      = toList . _refmap
  {-# INLINE[2] fromList  #-}
  {-# INLINE[2] fromListN #-}
  {-# INLINE[2] toList    #-}

instance IsList (Inodes 3) where
  type Item (Inodes 3) = (V4 Z, InterpFn)
  fromList    = Inodes . fromList
  fromListN _ = Inodes . fromList
  toList      = toList . _refmap
  {-# INLINE[2] fromList  #-}
  {-# INLINE[2] fromListN #-}
  {-# INLINE[2] toList    #-}

-- ** Set-like instances
------------------------------------------------------------------------------
-- | Inode-set insertion function.
instance SetInsert (V3 Int, InterpFn) (Inodes 2) where
  (p, cs) ~> Inodes ps | ghost p   = Inodes ps
                       | otherwise = Inodes $ (p, cs) ~> ps

-- | Inode-set query and remove functions.
instance SetIndex (V3 Int) (Inodes 2) where
  p <? Inodes ps = p <? ps
  p <\ Inodes ps = Inodes (p <\ ps)

-- | Construct initial `Inodes`.
instance SetBuild (V3 Int, InterpFn) (Inodes 2) where
  bare = Inodes bare
  unit (p, x) | ghost p   = bare
              | otherwise = Inodes $ unit (p, x)

-- | Fetch inodes (by coordinate) from the set of inodes.
instance SetElems (V3 Int) (Maybe InterpFn) (Inodes 2) where
  p <~ Inodes ps = p <~ ps


-- * Constructors
------------------------------------------------------------------------------
-- | Using the input DoF-nodes and linear tree information, compute the
--   inferred-nodes for the mesh.
makeInodes :: Dnodes 2 a -> Ltree 2 -> Inodes 2
makeInodes ds = Inodes . fromList . inodesList . concat . Map.elems .
  (makeNodes ds <$>) . rawPatches

-- ** Constructor helpers
------------------------------------------------------------------------------
inodesList :: [Z `Either` V3 Z] -> [(V3 Z, InterpFn)]
inodesList  = stripDnodes >>> filter inside >>> zip `flip` repeat bare
