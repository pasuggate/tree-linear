{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
FunctionalDependencies, ViewPatterns, PatternSynonyms,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Lnbrs
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Neighbourhood queries for linear 2^d-trees
-- 
-- Changelog:
--  + 21/07/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Lnbrs where

import Control.Lens ((^.), (.~))
import Data.IntSet.Sets as Set
-- import qualified Data.IntSet as Set
-- import Data.IntMap.ZMaps
import Linear (_yx, _yxz, (^+^), V2 (..), V3 (..))

import Data.Bits.Extra
import Data.Lpath.Lazy
-- import Data.Ltree.Lbase
import Data.Ltree.Dnode
-- import Data.Ltree.Bnode


-- * Function families for computing coordinates of parents and neighbours
------------------------------------------------------------------------------
-- | A (patch/inferred) CP can have dependencies from coarser mesh-levels (the
--   `aboves`) or from the same mesh-level (its `codeps`).
class PointHasDeps p where
  aboves :: p -> [p]
  codeps :: p -> [p]

class CanHashPoint p t | t -> p where
  tohash :: p -> t -> Int


-- * Instances for neighbourhood dependencies
------------------------------------------------------------------------------
instance PointHasDeps (V3 Int) where
  aboves = aboves2
  codeps = codeps2
  {-# INLINE[2] aboves #-}
  {-# INLINE[2] codeps #-}

instance CanHashPoint (V3 Int) (Dnodes 2 a) where
  tohash = tohash2
  {-# INLINE[2] tohash #-}


-- * Interpolation neighbourhoods
------------------------------------------------------------------------------
-- | Compute the coarse CP-coordinates for the given inode.
--   NOTE: the generated list is of all the coordinates of the coarse nodes
--     that would be required when subdividing; i.e., the input node's CP
--     dependencies.
aboves2 :: V3 Int -> [V3 Int]
aboves2 xy@(V3 _ _ d) =
  let ts = odds xy
      (s, t) = (2^(maxDepth-d), s <<% 1)
      go False = [-t, 0, t]
      go True  = [-s, s]
      [xs, ys] = map go ts
  in  fmap (\a -> xy ^+^ (_yx .~ a $ (-1))) $ V2 <$> ys <*> xs

-- | Compute the adjacent (fine) CP-coordinates for the given inode.
--   NOTE: these are the coordinates of all nodes that can be computed from
--     the input node's dependencies.
codeps2 :: V3 Int -> [V3 Int]
codeps2 xy@(V3 _ _ d) =
  let ts = odds xy
      s  = 2^(maxDepth-d)
      go False = [-s, 0, s]
      go True  = [0]
      [xs, ys] = map go ts
--   in  fmap (\a -> xy ^+^ (_yx .~ a $ 0)) $ V2 <$> ys <*> xs
  in  fmap ((xy ^+^) . (^._yxz)) $ V3 <$> ys <*> xs <*> [0]

-- ** Neighbourhood hashing
------------------------------------------------------------------------------
-- | Convert a coordinate into a hash-value for looking-up the interpolation
--   function for the neighbourhood.
tohash2 :: V3 Int -> Dnodes 2 a -> Int
tohash2 xy l = let bs = fromEnum <$> hash2' xy l
               in  foldl (\x b -> (x <<% 1) .|. b) 0 bs

-- NOTE: the hash value is determined by the node-alignment and by the fine
--   nodes of the neighbourhood.
hash2' :: V3 Int -> Dnodes 2 a -> [Bool]
hash2' xy ds
  | xy <? ds  = []
  | otherwise = True:ts ++ reverse cs
  where
    (ts, ps)  = (odds xy, ds^.posmap)
    cs = False `maybe` const True <$> [p <~ ps | p <- codeps xy]
