{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms, ScopedTypeVariables, TupleSections,
StandaloneDeriving, TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Dnode
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Boundary-nodes data-structures and functions, for linear 2^d-trees
-- 
-- Changelog:
--  + 29/07/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Dnode where

-- import GHC.Word
import GHC.TypeNats
import Control.Lens (makeLenses, (^.), (%~))
import Data.Foldable (toList, foldl')
import Data.Bool (bool)
import Data.Tuple (swap)
import Data.Maybe (catMaybes)
import Data.IntSet.Sets as Set
-- import qualified Data.IntSet as Set
import Data.IntMap.Maps as Map
import qualified Data.IntMap as Map
import Data.IntMap.ZMaps
-- import Data.List (inits, nub)
import Linear (_x, _y, _z, V2 (..), V3 (..), V4 (..))
-- import Data.Bits.Extras (msb, w64)
import qualified Data.Vector.Generic as G
import Data.Vector.Storable (Storable, Vector)
import Data.Vector.Helpers

import Data.Bits.Extra
import Data.Lpath.Lazy
import Data.Ltree.Lbase


-- * Node data types
------------------------------------------------------------------------------
-- | Stores the DoF-nodes (or, CP's) of a SMURCCS mesh.
--   TODO: use `d`
data Dnodes (d :: Nat) a =
  Dnodes { _posmap :: ZMap (d+1) Int
         , _datmap :: IntMap a
         , _idxmap :: NodeMap (d+1)
         , _dcount :: Int
         }

deriving instance Eq   a => Eq   (Dnodes 2 a)
deriving instance Show a => Show (Dnodes 2 a)


-- ** Some convenience aliases
------------------------------------------------------------------------------
type PosMap a = IntMap (IntMap (IntMap a))
type DofMap a = IntMap (IntMap a)
-- type NodeMap  = IntMap (V3 Int)

type family   NodeMap (d :: Nat) :: *
type instance NodeMap  2          = IntMap (V2 Int)
type instance NodeMap  3          = IntMap (V3 Int)
type instance NodeMap  4          = IntMap (V4 Int)


------------------------------------------------------------------------------
-- | DoF-node structure only.
type Lnodes   = Dnodes 2 ()

makeLenses ''Dnodes


-- * Node type-class instances
------------------------------------------------------------------------------
-- | Convert between coordinate+depth representation to Z-indices, and back.
{-- }
instance Lindex (V3 Int) where
  reify :: V3 Int -> Int
  reify (V3 x y d) = let s = maxDepth - d
                     in  topath d ((>>% s) <$> V2 y x)
  lpath :: Int -> V3 Int
  lpath p = let d = plen p
                V2 y x = unpath maxDepth p
            in  V3 x y d
--}

-- ** Set-like instances
------------------------------------------------------------------------------
-- | Dnode-set insertion function.
instance SetInsert (V3 Int, a) (Dnodes 2 a) where
  (p,x) ~> Dnodes ps xs cs c | ghost p   = Dnodes ps  xs  cs  c
                             | otherwise = Dnodes ps' xs' cs' c'
    where ps' = (p,c) ~> ps
          xs' = (c,x) ~> xs
          cs' = (c,p) ~> cs
          c'  = succ c

-- | Dnode-set query and remove functions.
instance SetIndex (V3 Int) (Dnodes 2 a) where
  p <? Dnodes ps  _  _ _ = p <? ps
  p <\ Dnodes ps xs cs c = case p <~ ps of
    Nothing -> Dnodes     ps      xs      cs  c
    Just b  -> Dnodes (p<\ps) (b<\xs) (b<\cs) c

-- | Construct initial `Dnodes`.
instance SetBuild (V3 Int, a) (Dnodes 2 a) where
  bare = Dnodes bare bare bare 0
  unit (p,x) | ghost p   = bare
             | otherwise = (p,x) ~> bare

-- | Fetch dnodes (by coordinate) from the set of dnodes.
instance SetElems (V3 Int) (Maybe (Int, a)) (Dnodes 2 a) where
  p <~ Dnodes ps xs _ _ = maybe Nothing (\b -> (b,) <$> (b<~xs)) $ p <~ ps


-- * Node-map constructors
------------------------------------------------------------------------------
-- | Build bidirectional node-lookup maps.
makeLnodes :: Ltree 2 -> Lnodes
makeLnodes t =
  let (dc, ps) = coeffsWith inside 1 $ noinferred t
      ix = coord2index ps
  in  Dnodes ps (const () <$> ix) ix dc

-- | Build `Dnodes` from the map from DoF-coords to DoF-values.
makeDnodes :: ZMap 3 a -> Dnodes 2 a
makeDnodes xs =
  let (dc, ps, ds, js) = foldl' upd (0, bare, bare, bare) $ flat xs
      upd (c, qs, ns, ks) (p, x) = (c+1, (p,c)~>qs, (c,x)~>ns, (c,p)~>ks)
  in  Dnodes ps ds js dc

-- ** Constructor helpers
------------------------------------------------------------------------------
-- | Remove any DoF-node indices from the given list.
stripDnodes :: [Z `Either` V3 Z] -> [V3 Z]
stripDnodes  = catMaybes . fmap (const Nothing `either` Just)

-- | Traverse the input list of node-coordinates and see if they correspond
--   to DoF-nodes. The output list is either DoF-node indices, or node-
--   coordinates for non-DoF nodes (i.e., ghost and/or inferred nodes).
makeNodes :: Functor f => Dnodes 2 a -> f (V3 Z) -> f (Z `Either` V3 Z)
makeNodes ds =
  let go p = maybe (Right p) (Left . fst) $ p <~ ds
  in  fmap go


-- * Linear tree elements & nodes
------------------------------------------------------------------------------
-- | Compute node coordinates from quadtree leaves.
nodes :: Ltree 2 -> [(Z, V3 Z)]
nodes  = flat . coord2index . coeffs

-- | Build a map from node-ID's to node coordinates.
--   NOTE: contains only DoF's
nodeMap :: Ltree 2 -> IntMap (V3 Z)
nodeMap  = Map.fromList . nodes . noinferred


-- ** Linear 2^d tree, node-coordinate functions
------------------------------------------------------------------------------
-- | Build a map to node-ID's, and with lookup ordering `X -> Y -> Z -> ID`
coeffs :: Ltree 2 -> ZMap 3 Z
coeffs  = snd . coeffsWith (const True) 1

-- | Build the map using a keep/drop predicate, and with indices starting from
--   `s`.
coeffsWith :: (V3 Z -> Bool) -> Z -> Ltree 2 -> (Z, ZMap 3 Z)
coeffsWith f s (Ltree t) =
  let upd = foldl (\(i, xs) p -> if p <? xs || not (f p) then (i, xs)
                                 else (i+1, (p, i) ~> xs))
  in  foldl (\(i, xs) q -> upd (i, xs) (qnodes q)) (s, bare) $ flat t

------------------------------------------------------------------------------
-- | Convert a map from positions to node-ID's into its inverse map.
coord2index :: ZMap 3 Int -> NodeMap 3
coord2index  = Map.fromList . fmap swap . flat


-- ** Queries
--    TODO: move to somewhere else, as this stuff isn't Dnode-specific?
--    TODO: to support _sparse_ linear trees, the following membership tests
--      should also be given the relevant tree/forest?
------------------------------------------------------------------------------
-- | Does the given node-coordinate lie on the boundary, or outside, of the
--   domain?
--   TODO: treat the boundary, and out-of-domain, cases differently?
ghost :: V3 Int -> Bool
ghost (V3 x y _) = let w = 1 <<% maxDepth in x<=0 || x>=w || y<=0 || y>=w

-- | Does the given node-coordinate lie on the boundary of the domain?
border :: V3 Int -> Bool
border (V3 x y _) = let w = 1 <<% maxDepth in x==0 || x==w || y==0 || y==w

-- | Does the given node-coordinate lie strictly inside the boundary?
inside :: V3 Int -> Bool
inside (V3 x y _) = let w = 1 <<% maxDepth in x>0 && x<w && y>0 && y<w

-- | Does the given node-coordinate lie strictly outside the boundary?
outside :: V3 Int -> Bool
outside (V3 x y _) = let w = 1 <<% maxDepth in x<0 || x>w || y<0 || y>w

------------------------------------------------------------------------------
-- | Test to see if each coordinate is within the domain.
belongs :: V3 Int -> [Bool]
belongs (V3 x y _) = let w = 2^maxDepth in [x>=0 && x <=w, y>=0 && y<=w]

-- | Test to see if each coordinate is _strictly_ within the domain.
insides :: V3 Int -> [Bool]
insides (V3 x y _) = let w = 2^maxDepth in [x>0 && x <w, y>0 && y<w]

------------------------------------------------------------------------------
{-- }
-- | Is the `x` coordinate "even"?
xeven :: V3 Int -> Bool
xeven (V3 x _ d) =
  let m = (2 <<% maxDepth) - (2 <<% (maxDepth - d))
  in  x .&. m == x
      
-- | Is the `y` coordinate "even"?
yeven :: V3 Int -> Bool
yeven (V3 _ y d) =
  let m = (2 <<% maxDepth) - (2 <<% (maxDepth - d))
  in  y .&. m == y
--}

_even :: Z -> Z -> Bool
_even d x =
  let m = (2<<%maxDepth) - (2<<%(maxDepth-d))
  in  x .&. m == x
{-# INLINE _even #-}

-- | Is the `x` coordinate "even"?
xeven :: V3 Z -> Bool
xeven p = let d = p^._z in _even d (p^._x)

-- | Is the `y` coordinate "even"?
yeven :: V3 Z -> Bool
yeven p = let d = p^._z in _even d (p^._y)

------------------------------------------------------------------------------
-- | Compute whether each axis-coordinate has even or odd alignment.
--   NOTE: works as long as the last ordinate is the refinement level.
odds :: Foldable f => f Z -> [Bool]
odds xi = let ev = _even (last xs)
              xs = toList xi
          in  not . ev <$> init xs
{-# SPECIALIZE odds :: V3 Z -> [Bool] #-}
{-# SPECIALIZE odds :: V4 Z -> [Bool] #-}

-- --   TODO: generalise.
-- odds :: V3 Int -> [Bool]
-- odds xy = not <$> [xeven xy, yeven xy]

-- | Is the given node even-aligned for both axes?
isEven :: V3 Int -> Bool
isEven  = and . fmap not . odds

------------------------------------------------------------------------------
hasCoarser :: Dnodes 2 a -> V3 Z -> Bool
hasCoarser ds = maybe False (<? ds) . coarser

hasFiner :: Dnodes 2 a -> V3 Z -> Bool
hasFiner ds = maybe False (<? ds) . finer


-- * Node modification functions
------------------------------------------------------------------------------

-- ** Node coordinate modification functions
------------------------------------------------------------------------------
-- | Compute the coordinates of a node that are one level coarser than the
--   node corresponding to input coordinate, if possible.
coarser :: V3 Z -> Maybe (V3 Z)
coarser p | isEven p && p^._z > 0 = Just $ _z %~ pred $ p
          | otherwise             = Nothing

-- | Compute the coordinates of a node that are one level finer than the node
--   corresponding to input coordinate, if possible.
finer :: V3 Z -> Maybe (V3 Z)
finer p | p^._z < maxDepth = Just $ _z %~ succ $ p
        | otherwise        = Nothing

------------------------------------------------------------------------------
-- | Does the node-coordinate have a coarser dnode? If so, return the node-
--   coordinate of this dnode.
--   TODO: name-clashes with the mesh-coarsen functions?
coarsen :: V3 Int -> Dnodes 2 a -> V3 Int
coarsen p ds = maybe p (\q -> bool p q $ q<?ds) $ coarser p

-- ** Node helper-functions
------------------------------------------------------------------------------
-- | Compute set of four node coordinates for the given quadtree leaf.
qnodes :: Int -> V4 (V3 Int)
qnodes q =
  let V2 y x = unpath maxDepth q
      dx = 2^(maxDepth-z)
      z  = plen q
      p0 = V3  x      y     z
      p1 = V3 (x+dx)  y     z
      p2 = V3 (x+dx) (y+dx) z
      p3 = V3  x     (y+dx) z
  in  V4 p0 p1 p2 p3


-- * Export/import functions
------------------------------------------------------------------------------
--   TODO: require that indices be within the range `[0,n)`?
--   TODO: make inlinable, or use type-classes, so that these functions are
--     properly-specialised?
toVec :: G.Vector v a => Dnodes d a -> Maybe (v a)
toVec ds = do
  let xs = ds^.datmap
      n  = card xs
      es = Just $ G.fromList $ Map.elems xs
  i <- fst <$> Map.lookupMin xs
  j <- fst <$> Map.lookupMax xs
  bool Nothing es $ j-i+1 == n
{-# SPECIALIZE toVec :: Storable a => Dnodes d a -> Maybe (Vector a) #-}
