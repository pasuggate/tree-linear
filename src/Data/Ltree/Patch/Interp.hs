{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms, ScopedTypeVariables, TupleSections,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Patch.Interp
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Patch interpolation-matrix calculation functions.
-- 
-- Changelog:
--  + 21/10/2018  --  initial file (refactored from `Patch`);
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Patch.Interp (interpFn, patchNodes) where

import Control.Lens (isn't, (^.), (.~), (%~))
import Control.Arrow (first, second)
import Data.List (intersperse)
import Data.IntSet.Sets as Set
import qualified Data.IntMap as Map
import Linear (_z, V3 (..))
import qualified Data.Vector as Box
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
import qualified Numeric.LinearAlgebra as H
import Text.Printf

import Data.Vector.Helpers
import Data.Matrix.Dense

import Data.Ltree.Dnode
import Data.Ltree.Inode
-- import Data.Ltree.Bnode
import Data.Ltree.Lnbrs
import Data.Ltree.Lmask
import Data.Ltree.Patch.Pbase


-- * Node Interpolation Function (NIF) generators
------------------------------------------------------------------------------
-- | Compute the neighbourhood interpolation function (NIF) for the given
--   basis-function coordinate (and, typically, a basis function of a patch).
--   
--   TODO: recursive resolution of "unodes" (inodes from coarse dnodes).
--   TODO: handle a `Bnode` similarly to a `Dnode` -- and this will also make
--     it easier to implement support for forests?
interpFn :: V3 Z -> PatchBuilder 2 a -> (InterpFn, PatchBuilder 2 a)
interpFn p pb
  | p <? pb^.dnodes = (unit $ (,1) $ fst $ erfM "Dnode" p id $ p <~ pb^.dnodes, pb)
  | ghost p         = bnodeFn p pb
  | p <? pb^.inodes = (erfM "Inode" p id $ p <~ pb^.inodes, pb)
  | sum (Map.elems pf) /= 1 = error $ printf "interpFn: row does not sum to one"
  | otherwise       = (pf, inodes %~ ((p, pf) ~>) $ pp)
  where
    (ns, pb') = first Box.fromList $ p `patchNodes` pb
    m  = echelon $ odds p
    (ps, cs) = second (head . H.toRows . rref) $ rearrange p ns m
    qs = tal $ Box.ifilter (\i _ -> cs!i /= 0) ps
    ks = Vec.map negate $ tal $ Vec.filter (/= 0) cs
    (pf, pp) = toInterp p qs ks pb'

-- ** Generators for the various subtypes of patch-node
------------------------------------------------------------------------------
-- | Calculate/look-up the bnode interpolation function.
--   NOTE: if there is no boundary-/ghost- node at the given position, it
--     allocates one, and sets its scale-factor to one.
--
--   TODO: current approach makes it harder to apply boundary conditions,
--     as eliminating a boundary-/ghost- node is easier when it has the same
--     level as its corresponding DoF node(s).
--   TODO: change to checking for coarser boundary-/ghost- parents, and then
--     computing an appropriate NIF.
bnodeFn :: V3 Z -> PatchBuilder 2 a -> (InterpFn, PatchBuilder 2 a)
bnodeFn p pb
  | inside p  = error $ printf "bnodeFn: `%s` is not a boundary node" (show p)
  | otherwise = go $ p <~ bs
  where
    (bs, pp) = (pb^.bnodes, bnodes %~ (p ~>) $ pb)
    go = first (unit . (,1)) . maybe (erfM "Bnode" p id $ p <~ pp^.bnodes, pp) (,pb)

------------------------------------------------------------------------------
-- | If a dependency of an interpolation-function calculation is itself an
--   inferred-node, then compute the interpolation-function for this node so
--   that it can be merged into the solution.
--   NOTE: updates `PatchBuilder` with the new inode.
resolveInode :: V3 Z -> PatchBuilder 2 a -> (Pnode, PatchBuilder 2 a)
resolveInode o pb
  | o <? pb^.inodes = (Inode . erfM "Inode" o id $ o <~ pb^.inodes, pb)
  | otherwise       = Inode `first` interpFn o pb

------------------------------------------------------------------------------
-- | Construct an interpolation function using the input pnodes and coeffs.
toInterp :: V3 Z -> Box Pnode -> Vector R -> PatchBuilder 2 a ->
            (InterpFn, PatchBuilder 2 a)
toInterp o ps ks pb = Box.ifoldl' (mergeFn o ks) (bare, pb) ps

-- | Merges the recursive inode into the interpolation-function.
mergeFn :: V3 Z -> Vector R -> (InterpFn, PatchBuilder 2 a) -> Z -> Pnode ->
           (InterpFn, PatchBuilder 2 a)
-- mergeFn o ks (qf, pb) i  Xnode    = (qf, pb)
-- mergeFn o ks (qf, pb) i (Pivot _) = (qf, pb)
mergeFn _ ks (qf, pb) i (Dnode j) = ((j, ks!i) ~> qf, pb)
mergeFn _ ks (qf, pb) i (Bnode j) = ((j, ks!i) ~> qf, pb)
-- mergeFn _ ks (qf, pb) i (Inode g) = (Map.unionWith (+) qf $ Map.map (*ks!i) g, pb)
mergeFn _ ks (qf, pb) i (Inode g) = (Map.unionWith (+) qf $ (*ks!i) <$> g, pb)
mergeFn o ks (qf, pb) i (Vnode p)
  | ghost p     = let b = maybe ber id $ p <~ pb^.bnodes
                  in  ((b, ks!i) ~> qf, pb)
  | card rf > 3 = (Map.unionWith (+) qf $ Map.map (*ks!i) rf, pp)
  | otherwise   = error $ printf msg (show p) (show o) si
  where
    (rf, pp) = interpFn p pb
    msg = "mergeFn: unexpected node: %s (origin: %s)\nInodes:\n%s\n"
    si  = concat $ intersperse "\n" $ show <$> flat (pb^.inodes^.refmap)
    ber = error $ printf "pnode: Bnode `%s` found (origin: %s), but has no data" (show p) (show o)
mergeFn o  _ ( _, pb) _        p  =
  error $ printf "mergeFn: unexpected node: %s (origin: %s)\nInodes:\n%s\n" (show p) (show o) si
  where
    si = concat $ intersperse "\n" $ show <$> flat (pb^.inodes^.refmap)


-- * Patch helper-functions
------------------------------------------------------------------------------
-- | Compute the neighourhoods for each node of a patch.
--   NOTE: coordinate `o` is the patch "origin" coordinate.
--   NOTE: the order of the nodes are fine nodes, and then coarse nodes.
--   NOTE: (out of) boundary nodes receive their negated Z-index, and their
--     level must always be that of the patch's.
--   
--   TODO: to ease with applying boundary conditions, change to using the
--     level of the corresponding "inner" CP, for boundary & ghost nodes.
patchNodes :: V3 Z -> PatchBuilder 2 a -> ([Pnode], PatchBuilder 2 a)
patchNodes o pb@(PatchBuilder ds _ _ _)
  | o <? ds   = erfM "Dnode" o ((,pb) . pure . Dnode . fst) $ o <~ ds
  | otherwise = foldr go ([], pb) $ codeps o ++ aboves o
  where
    go p (ps, pp) = let (q, pp') = pnode o p pp in (q:ps, pp')

-- | Compute the patch-node (type & value) for a node of a patch.
--   NOTE: coordinate `o` is the patch "origin" coordinate.
--   
--   TODO: what if an appropriate inode/unode for the pivot already exists?
--   TODO: what if fine bnodes need to be calculated from coarse bnodes?
pnode :: V3 Z -> V3 Z -> PatchBuilder 2 a -> (Pnode, PatchBuilder 2 a)
pnode o p pb@(PatchBuilder ds _ bs _)
  | o == p         = (Pivot o, pb)
  | p <? ds        = erfM "Dnode" p ((,pb) . Dnode . fst) $ p <~ ds
  | ghost p && lvl = (maybe ger Bnode $ p <~ bs', bnodes .~ bs' $ pb)
  | lvl            = (Xnode  , pb)  -- unneeded node
  | p <? bs        = (erfM "Dnode" p Bnode $ p <~ bs, pb)
-- --   | p <? ix        = maybe ier ((,pb) . Inode) $ p <~ ix
--   | isInode o p ds = resolveInode p pb
--   | otherwise      = (Vnode p, pb)  -- equations can still be used
  | isn't _Inode ip = (Vnode p, pb)  -- equations can still be used
  | otherwise      = (ip, pb')
  where
    (ip, pb') = resolveInode p pb
    bs' = p ~> bs
    lvl = o^._z == p^._z
    ger = error $ printf "pnode: Bnode `%s` added, but has no data" (show p)


-- * Error message functions
------------------------------------------------------------------------------
erfM :: Show a => String -> a -> (b -> c) -> Maybe b -> c
erfM t p f = erf t p `maybe` f

erf :: Show a => String -> a -> c
erf t p = error $ printf "pnode: %s `%s` found, but has no data" t (show p)
