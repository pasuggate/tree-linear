{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms, ScopedTypeVariables, TupleSections,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Patch
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Patch-extraction for linear 2^d-tree representations, of SMURCCS meshes.
-- 
-- Changelog:
--  + 29/07/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Patch.Pbase
  ( module Data.Ltree.Patch.Ptype
  , module Data.Ltree.Patch.Pbase
  ) where

import GHC.Exts (IsList (..))
import Control.Lens (isn't, (^.))
import Data.Foldable (foldl')
import Data.List (partition)
import Data.IntSet.Sets as Set
import Data.IntMap.Maps as Map
import qualified Data.IntMap as Map
import Linear (_z, V3 (..))
import qualified Data.Vector as Box
import Data.Vector.Storable (Vector, Storable)
import Numeric.LinearAlgebra (Matrix, (?), (¿)) -- ^ HMatrix
import qualified Numeric.LinearAlgebra as H
import Text.Printf

import Data.Vector.Helpers
import Data.Matrix.Dense (Box)
import Data.CCSMat (CCSMat)
import qualified Data.CCSMat as CCS
-- import qualified Data.CCSMat.Vector as CCS
-- import qualified Data.Vector.Sparse as SpV
-- import Data.Vector.Sparse (SparseVector (..))

import Data.Ltree.Dnode
import Data.Ltree.Patch.Ptype


-- ** Patch-transform constructors
------------------------------------------------------------------------------
-- TODO: see notes @26/08/2018
-- FIXME: The boundary-operator needs to be applied "outside" the gather/
--   scatter operator?
applyBoundary :: (Storable a, Num a) => Vector Z -> CCSMat a
applyBoundary (len -> 16) = CCS.eye 16
applyBoundary _ = CCS.eye 25

-- | Gathers all of the keys, from all of the input `IntMap's, and stores
--   them in a (sorted) set.
labelSet :: Foldable f => f (IntMap a) -> IntSet
labelSet  = foldl' (\ks -> foldl' (flip (~>)) ks . Map.keys) bare

-- ** Patch-transform constructors
------------------------------------------------------------------------------
-- | is the `p` coordinate of another "inferred" node, in this context?
isInode :: V3 Z -> V3 Z -> Dnodes 2 a -> Bool
isInode o p ds = o^._z == p^._z+1 && hasCoarser ds p


-- * Interpolation matrix functions
------------------------------------------------------------------------------
-- | Reorder the system of equations to be ready for solving.
rearrange :: V3 Z -> Box Pnode -> Matrix R -> (Box Pnode, Matrix R)
-- rearrange p ps m =
rearrange _ ps m =
  let (ks, js) = reorder (H.rows m) ps
  in  (ps `Box.backpermute` Box.fromList js, m ? ks ¿ js)

-- | Compute the row & column permutation vectors, prior to calculating the
--   RREF of the interpolation matrix.
reorder :: Z -> Box Pnode -> ([Z], [Z])
reorder r ps =
  let k  = maybe per id $ Box.findIndex (not . isn't _Pivot) ps -- ^ find pivot
      -- ^ permutation vector for rows
      ks = k:[i | i <- [0..r-1], isn't _Pivot (ps!i) && isn't _Xnode (ps!i)]
      -- ^ permutation vector for columns
      go p = isn't _Xnode p && isn't _Pivot p
      js = filter (go . snd) $ [0..] `zip` toList ps
      (jf, ji) = partition (isn't _Vnode . snd) js
      per = error $ printf "reorder: pivot node not found in %s" (show ps)
  in  (ks, (k:) $ fst <$> ji ++ jf)
