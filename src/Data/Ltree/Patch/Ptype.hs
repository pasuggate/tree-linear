{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms, ScopedTypeVariables, TupleSections,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Patch.Ptype
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Patch-extraction for linear 2^d-tree representations, of SMURCCS meshes.
-- 
-- Changelog:
--  + 29/07/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Patch.Ptype
  ( module Data.Ltree.Patch.Ptype
  ) where

import GHC.TypeNats
import Control.Lens (makeLenses, makePrisms)
import Data.IntMap.Maps as Map
import Linear (V3 (..))
import Data.Vector.Storable (Vector)
import Data.Vector.Helpers
import Data.CCSMat (CCSMat)

import Data.Ltree.Dnode
import Data.Ltree.Inode
import Data.Ltree.Bnode
import Data.Ltree.Lelem


-- * Patch data-types
------------------------------------------------------------------------------
-- | Local, uniform, B-spline patch for FEA.
--   NOTE: CCSMat indices are "local" indices, and the `dofnodes` and
--     `bdynodes` maps are for translating these.
--   
--   TODO: what about edges, as boundary-conditions may require these?
data PatchT (d :: Nat) a =
  PatchT { _patchloc :: V3 Z
         , _dofnodes :: Vector Z
         , _interpop :: CCSMat R
         , _bdyconds :: Maybe (Btype d)
         , _celltype :: Ptype
         , _celldata :: a
         } deriving (Eq, Show)

------------------------------------------------------------------------------
-- | Patch node-types
data Pnode
  = Pivot (V3 Z)
  | Vnode (V3 Z)
  | Dnode     Z
  | Bnode     Z
  | Inode InterpFn
  | Xnode
  deriving (Eq, Show)

-- ** Patch-builder data-types
------------------------------------------------------------------------------
-- | Data structure that contains the state-variables that are needed when
--   extracting the local, uniform, B-spline patches (from a linear tree
--   representation of a SMURCCS mesh).
data PatchBuilder (d :: Nat) a =
  PatchBuilder { _dnodes :: Dnodes 2 a
               , _inodes :: Inodes 2
               , _bnodes :: Bnodes 2
               , _nifmap :: IntMap InterpFn
               } deriving (Eq, Show)

-- ** Lenses and prisms for the patch data types
------------------------------------------------------------------------------
makeLenses ''PatchT
makePrisms ''Pnode
makeLenses ''PatchBuilder

-- ** Convenience patterns and aliases
------------------------------------------------------------------------------
type    Patch (d :: Nat) = PatchT d ()

pattern Patch :: forall (d :: Nat).
  V3 Z -> Vector Z -> CCSMat R -> Maybe (Btype d) -> Ptype -> PatchT d ()
pattern Patch l d o b t  = PatchT l d o b t ()

-- | Map from Z-indices to each `Patch`.
type PatchMap d = IntMap (Patch d)
