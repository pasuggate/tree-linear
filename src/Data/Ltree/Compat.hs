{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms, ScopedTypeVariables, TupleSections,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Compat
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Backwards compatibility with `SUBMesh`
-- 
-- Changelog:
--  + 18/08/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Compat where

import GHC.Word
import GHC.TypeNats
import Control.Lens (makeLenses, makePrisms, isn't, (^?), (^.), (.~), (%~))
import Control.Arrow (first, second, (&&&), (>>>))
import Data.Foldable (foldl', toList)
import Data.List (partition, intersperse)
import Data.Bool (bool)
import Data.Maybe (isJust, fromMaybe, catMaybes)
import Data.IntSet.Sets as Set
import qualified Data.IntSet as Set
import Data.IntMap.Maps as Map
import qualified Data.IntMap as Map
import Linear (_z, V2 (..), V3 (..), V4 (..), (*^))
import Data.Bits.Extra
import Data.Bits.Extras (msb, w64)
import qualified Data.Vector as Box
import Data.Vector.Storable (Storable, Vector)
import qualified Data.Vector.Storable as Vec
import Numeric.LinearAlgebra (Element, Matrix, (?), (¿), (|||)) -- ^ HMatrix
import qualified Numeric.LinearAlgebra as H
import Text.Printf

import Data.Vector.Helpers
import Data.Matrix.Dense
import Data.CCSMat (CCSMat)
import qualified Data.CCSMat as CCS
import qualified Data.CCSMat.Vector as CCS
import qualified Data.Vector.Sparse as SpV
import Data.Vector.Sparse (SparseVector (..))

import Data.Lpath.Lazy
import Data.Ltree.Lbase
import Data.Ltree.Dnode
import Data.Ltree.Inode
import Data.Ltree.Bnode
import Data.Ltree.Lelem
import Data.Ltree.Patch

import Data.Subdivision.SUBMesh.Base (SUB' (..), Elem' (..), SUB, Elem)
import qualified Data.Subdivision.SUBMesh.Base as S


-- * Exporters
------------------------------------------------------------------------------
-- | Convert to a `SUBMesh`.
--   TODO: check that DoF-node indices are in the range `[0,n)`?
--   TODO: are [5x5] patches correctly handled by `SUBMesh`?
makeSUB :: Dnodes 2 (V2 R) -> Ltree 2 ->
           Maybe (SUB (V2 R), PatchBuilder 2 (V2 R))
makeSUB xs lt =
  let pb = makePatches xs lt
      ps = flat $ rawPatches $ lt^.unLtree
      go (q,uv) px = let V3 u v h = coord q
                         (w, s) = (2^^negate h, 2^^maxDepth)
                         xy = (*recip s) . fromIntegral <$> V2 u v
                         ((ix, tx), px') = patchTransform uv px
                     in  (Elem xy w tx ix, px')
      -- TODO: strictness annotations?
      loop (q:rs) px = let (e,  px' ) = go q px
                           (es, px'') = loop rs px'
                       in  (e:es, px'')
      (es, pb') = loop ps pb
  in  (,pb') . flip SUB es <$> toVec (pb'^.dnodes)
