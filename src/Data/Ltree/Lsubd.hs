{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, GADTs, TypeOperators,
MultiParamTypeClasses, TypeFamilies, FunctionalDependencies,
FlexibleContexts, FlexibleInstances, InstanceSigs, TypeSynonymInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms, ScopedTypeVariables, TupleSections,
TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Lsubd
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Subdivision & refinement for linear 2^d-trees
-- 
-- Changelog:
--  + 25/07/2018  --  initial file;
-- 
-- TODO:
--  + parameterise these functions by path/index and tree-type?
-- 
------------------------------------------------------------------------------

module Data.Ltree.Lsubd where

import Control.Lens ((%~))
import Data.IntSet.Sets as Set
-- import qualified Data.IntSet as Set
import Linear (V3 (..))

import Data.Lpath.Lazy
import Data.Ltree.Lbase
import Data.Ltree.Dnode ()


-- * Refinement & subdivision functions
------------------------------------------------------------------------------
-- | Recursively refine the given linear quadtree, and subdivide each leaf-
--   node whenever the predicate evaluates to `True`.
--   TODO: depth-checks?
refine :: (Int -> Bool) -> Ltree 2 -> Ltree 2
refine p = unLtree %~ go bare . flat
  where
    go r (n:ns)
      | p n       = go r $ progeny n ++ ns
      | otherwise = go (n~>r) ns
    go r     _    = r

-- | Subdivide each leaf-node into four leaves.
subdivide :: Ltree 2 -> Ltree 2
subdivide t
  | depth t < maxDepth = let p = (<?t) in refine p t
  | otherwise          = error "Lsubd.subdivide: subdivided tree too high"


-- * Testing & examples
------------------------------------------------------------------------------
testLsubd :: IO ()
testLsubd  = do
  let lt = uniform 1
      lf = lpath :: Int -> V3 Int
      rf = \p -> let V3 j i k = lf p
                 in  j==0 && i==0 && k<3
      l' = refine rf lt
  print lt
  mapM_ (print . lf) $ flat (_unLtree l')
