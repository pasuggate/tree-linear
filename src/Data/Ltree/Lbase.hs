{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
GeneralizedNewtypeDeriving, RankNTypes, BangPatterns,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Lbase
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Lazy-list based paths through linear (2^d-)trees
-- 
-- Changelog:
--  + 24/06/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Lbase where

import GHC.TypeNats
import Control.Lens (makeLenses, (%~))
import Data.IntSet.Sets as Set
import qualified Data.IntSet as Set
import Data.IntMap.Maps as Map
import Linear (V2 (..), V3 (..))

import Data.Index.ZCurve


-- * Demonstration data-types
------------------------------------------------------------------------------
-- | Newtypse for this linear quadtree implementation.
newtype Ltree (d :: Nat)   = Ltree { _unLtree :: IntSet }
  deriving (Eq, Show, SetIndex Int)

newtype Lmesh (d :: Nat) a = Lmesh { _unLmesh :: IntMap a }
  deriving (Eq, Show)

makeLenses ''Ltree
makeLenses ''Lmesh


-- * And some instances
------------------------------------------------------------------------------
instance Layered (Ltree 2) where
  depth :: Ltree 2 -> Int
  depth  = depth . lpos2 . Set.findMax . ((-1)~>) . _unLtree

lpos2 :: Int -> V3 Int
lpos2  = lpath
{-# INLINE [2] lpos2 #-}


-- * Constructors
------------------------------------------------------------------------------
-- | Construct a uniform(-height) linear quadtree.
uniform :: Int -> Ltree 2
uniform h = let (w, js) = (2^h-1, [0..w])
            in  Ltree $ Set.fromList [topath h (V2 i j) | i<-js, j<-js]


-- * Queries
------------------------------------------------------------------------------
-- | Compute all of the ancestors for all of the given nodes.
ancestors :: IntSet -> IntSet
ancestors  =
  let go xs i = xs \/ Set.fromList (lineage i)
  in  foldl go bare . flat

children :: IntSet -> IntSet
children  = foldl (\xs i -> xs \/ offspring i) bare . flat

offspring :: Key 2 -> IntSet
offspring  = Set.fromList . progeny

------------------------------------------------------------------------------
-- | Compute the set of all parents for the given set of children.
parents :: IntSet -> IntSet
parents  = Set.foldl (\xs i -> parent i ~> xs) bare

------------------------------------------------------------------------------
-- | Compute the set of indices that are adjacent to any of the given input
--   indices.
peers :: IntSet -> IntSet
peers  = foldl (\xs i -> xs \/ touches i) bare . flat

-- | Calculates the set of nodes that touch the given node.
touches :: Key 2 -> IntSet
touches  = Set.fromList . incident

------------------------------------------------------------------------------
-- | Compute the set of cells of potential neighbours, if the given cell index
--   was an "inferred element," in the `Qtree` representation.
aunties :: Key 2 -> IntSet
aunties b = parent b <\ (parents . touches) b


-- ** Leaf-node (patch) type queries
------------------------------------------------------------------------------
-- | Inferred patches have CP's that are "hanging" nodes, and need their
--   values to be computed via interpolation.
--   NOTE: only valid for spline-degree, `p \in {1, 3}`?
isInferred :: Key 2 -> Ltree 2 -> Bool
isInferred k = not . isDofPatch k

-- | All CP's of the patch are "DoF" nodes.
--   NOTE: only valid for spline-degree, `p \in {1, 3}`?
isDofPatch :: Key 2 -> Ltree 2 -> Bool
isDofPatch k = Set.null . (aunties k /\) . _unLtree
{-# INLINEABLE[2] isDofPatch #-}

-- ** Shape-checks
------------------------------------------------------------------------------
-- | Checks to see if the given linear-tree completely covers the [0,1]^2
--   domain.
isComplete :: IntSet -> Bool
isComplete lt = go 1
  where
    go !k | k  <?  lt = True
          | otherwise = and $ go <$> progeny k


-- * Composite operations
------------------------------------------------------------------------------
-- | Compute all nodes that belong to the same parent cell.
cellmates :: Key 2 -> IntSet
cellmates  = offspring . parent
{-# INLINE[1] cellmates #-}

-- | Compute the set entire set of inmates.
inmates :: IntSet -> IntSet
inmates  = foldl (\xs i -> xs \/ cellmates i) bare . flat

------------------------------------------------------------------------------
-- | Compute the set of previous generation of elders.
--   TODO: can this fail if the input set contains nodes that don't have
--     grandparents?
support :: IntSet -> IntSet
support  = children . parents . peers . parents
{-# INLINE[1] support #-}

-- * Tree shape modification
------------------------------------------------------------------------------
-- | Rebalance a tree so that it satisfies the 2:1 balance constraint.
--   NOTE: Inspired by `Algorithm 2.2`, pp.15 , from "A Distributed Memory
--     Fast Multipole Method for Volume Potentials", by Malhotra & Biros. But
--     unlike their pseudocode version, there are no non-leaf nodes referred
--     to, or required by this algorithm.
--   
--   TODO: needs better termination conditions, as `support` may fail?
balance :: Ltree 2 -> Ltree 2
balance s =
  let go 0 _ _ b = Ltree b
      go i p t b =
        let (u, r) = Set.partition ((==i) . plen) $ _unLtree t
            x  = p \/ inmates u
            b' = b \/ x
            t' = Ltree $ r \\ ancestors x
--             p' = support x \\ ancestors x
            p' = support x \\ ancestors b' -- TODO: expensive?
        in  go (i-1) p' t' b'
  in  go (depth s) bare s bare

------------------------------------------------------------------------------
-- | Enforce both 2:1 balance and "area rule" constraints, while rebuilding
--   the given linear tree.
arearule :: Ltree 2 -> Ltree 2
arearule s =
  let go 0 _ _ b = Ltree b
      go i p t b =
        let (u, r) = Set.partition ((==i) . plen) $ _unLtree t
            x  = inmates (peers p \/ u) \\ ancestors b
            b' = b \/ x
            t' = Ltree $ r \\ ancestors x
            p' = (peers . parents) x \\ ancestors b'
        in  go (i-1) p' t' b'
  in  go (depth s) bare s bare


-- ** FEA & export helpers
------------------------------------------------------------------------------
-- | Remove all "inferred elements" from the tree.
noinferred :: Ltree 2 -> Ltree 2
noinferred lt = unLtree %~ Set.filter (flip isDofPatch lt) $ lt

-- | Keep only the "inferred elements," of the given tree.
inferred :: Ltree 2 -> Ltree 2
inferred (Ltree t) = Ltree $ Set.filter (\b -> not . Set.null $ aunties b /\ t) t
