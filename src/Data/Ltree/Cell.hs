{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
TypeOperators, MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
ViewPatterns, PatternSynonyms, ScopedTypeVariables, TupleSections,
TypeSynonymInstances, InstanceSigs, TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Ltree.Cell
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Cell data-structures for the leaf-nodes of the linear trees
-- 
-- Changelog:
--  + 07/10/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Ltree.Cell where

import GHC.TypeNats
import Control.Lens (makeLenses)
import Data.Vector.Helpers


type family   IPos (d :: Nat) a :: *
type instance IPos  1         a  = V2 a
type instance IPos  2         a  = V3 a
type instance IPos  3         a  = V4 a
-- type instance IPos  4         a  = V2 a


data CellT (d :: Nat) a =
  CellT { _cellpos :: IPos d Z
        , _celldat :: a
        }

