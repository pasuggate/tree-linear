{-# LANGUAGE PolyKinds, DataKinds, TypeOperators, RankNTypes, InstanceSigs,
             OverloadedStrings, TupleSections, PatternSynonyms, ViewPatterns,
             GeneralizedNewtypeDeriving, DeriveGeneric, DeriveFunctor,
             StandaloneDeriving, TypeFamilies, FlexibleInstances,
             MultiParamTypeClasses, FlexibleContexts, DefaultSignatures,
             TemplateHaskell, DeriveFoldable, DeriveTraversable
  #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.TreeMap
-- Copyright   : (C) Patrick Suggate 2020
-- License     : BSD3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Maps parametric coordinates to their owning tree, if possible.
-- 
-- Changelog:
--  + 29/03/2020  --  initial file;
-- 
------------------------------------------------------------------------------

module Data.Forest.TreeMap
  ( TCoord (..)
  , TreeMap (..)
  , treeMap
  ) where

import GHC.Generics (Generic)
import GHC.TypeNats
import Control.Arrow (first)
import Control.Lens (makeLenses, (^.))
import Control.DeepSeq
import Math.Sets
-- import Data.Foldable (foldl1)
import Data.MonoTraversable
-- import Data.Containers
import Data.Maybe (catMaybes)
import Data.Bits.Handy ((>>%), (.&.))
import Data.Aeson
import Linear.Handy hiding (unit)

import Data.Index.ZCurve
import Data.IntMap.ZMaps
import Data.Forest.Linear
import Data.Forest.Node


-- * Type-classes for tree-maps
------------------------------------------------------------------------------
-- | Helper-class to assist when converting parametric coordinates into "tree
--   coordinates", for the given coordinate of type @t@ .
--   
class TCoord t where
  tcoord :: t -> Vec (DimsOf t) Int


-- * Type-function instances
------------------------------------------------------------------------------
type instance DimsOf (TreeMap d) = d


-- * Tree-map data structures
------------------------------------------------------------------------------
newtype TreeMap (d :: Nat) = TreeMap { _treeMap :: ZMap d TreeID }
  deriving (Generic)

deriving instance Eq   (TreeMap 1)
deriving instance Show (TreeMap 1)

deriving instance Eq   (TreeMap 2)
deriving instance Show (TreeMap 2)

deriving instance Eq   (TreeMap 3)
deriving instance Show (TreeMap 3)


-- * Lenses and instances
------------------------------------------------------------------------------
makeLenses ''TreeMap

-- ** More standard instances
------------------------------------------------------------------------------
instance Semigroup (TreeMap 1) where
  TreeMap ps <> TreeMap qs = TreeMap $ ps <> qs
  {-# INLINE (<>) #-}

instance Monoid (TreeMap 1) where
  mempty = TreeMap mempty
  {-# INLINE mempty #-}

------------------------------------------------------------------------------
instance Semigroup (TreeMap 2) where
  TreeMap ps <> TreeMap qs = TreeMap $ ps <> qs
  {-# INLINE (<>) #-}

instance Monoid (TreeMap 2) where
  mempty = TreeMap mempty
  {-# INLINE mempty #-}

------------------------------------------------------------------------------
instance Semigroup (TreeMap 3) where
  TreeMap ps <> TreeMap qs = TreeMap $ ps <> qs
  {-# INLINE (<>) #-}

instance Monoid (TreeMap 3) where
  mempty = TreeMap mempty
  {-# INLINE mempty #-}

-- ** Strictness instances
------------------------------------------------------------------------------
instance NFData (TreeMap 1)
instance NFData (TreeMap 2)
instance NFData (TreeMap 3)

-- ** Tree-coordinates instances
------------------------------------------------------------------------------
{-- }
instance TCoord (PCoord 1) where
  tcoord (PCoord p) = V1 (p^._x >>% maxDepth (undefined :: PCoord 1))
--}

instance TCoord (PCoord 2) where
  tcoord :: PCoord 2 -> V2 Int
  tcoord (PCoord p) = (>>% maxDepth (undefined :: PCoord 2)) <$> p^._xy
  {-# INLINE tcoord #-}

-- ** MonoTraversable instances
------------------------------------------------------------------------------
type instance Element (TreeMap d) = TreeID

------------------------------------------------------------------------------
instance MonoFunctor (TreeMap 1) where
  omap f = TreeMap . fmap f . _treeMap
  {-# INLINE omap #-}

instance MonoFunctor (TreeMap 2) where
  omap f = TreeMap . fmap f . _treeMap
  {-# INLINE omap #-}

instance MonoFunctor (TreeMap 3) where
  omap f = TreeMap . fmap f . _treeMap
  {-# INLINE omap #-}

------------------------------------------------------------------------------
instance MonoFoldable (TreeMap 1) where
  ofoldMap     f   = ofoldMap     f   . _treeMap
  ofoldr       f x = ofoldr       f x . _treeMap
  ofoldl'      f x = ofoldl'      f x . _treeMap
  ofoldr1Ex    f   = ofoldr1Ex    f   . _treeMap
  ofoldl1Ex'   f   = ofoldl1Ex'   f   . _treeMap
  headEx           = headEx           . _treeMap
  lastEx           = lastEx           . _treeMap
  maximumByEx  f   = maximumByEx  f   . _treeMap
  minimumByEx  f   = minimumByEx  f   . _treeMap
  oelem          x = oelem          x . _treeMap
  onotElem       x = onotElem       x . _treeMap
  {-# INLINE ofoldMap #-}
  {-# INLINE ofoldr   #-}
  {-# INLINE ofoldl'  #-}
  {-# INLINE headEx   #-}
  {-# INLINE lastEx   #-}
  {-# INLINE oelem    #-}
  {-# INLINE onotElem #-}

instance MonoFoldable (TreeMap 2) where
  ofoldMap     f   = ofoldMap     f   . _treeMap
  ofoldr       f x = ofoldr       f x . _treeMap
  ofoldl'      f x = ofoldl'      f x . _treeMap
  ofoldr1Ex    f   = ofoldr1Ex    f   . _treeMap
  ofoldl1Ex'   f   = ofoldl1Ex'   f   . _treeMap
  headEx           = headEx           . _treeMap
  lastEx           = lastEx           . _treeMap
  maximumByEx  f   = maximumByEx  f   . _treeMap
  minimumByEx  f   = minimumByEx  f   . _treeMap
  oelem          x = oelem          x . _treeMap
  onotElem       x = onotElem       x . _treeMap
  {-# INLINE ofoldMap #-}
  {-# INLINE ofoldr   #-}
  {-# INLINE ofoldl'  #-}
  {-# INLINE headEx   #-}
  {-# INLINE lastEx   #-}
  {-# INLINE oelem    #-}
  {-# INLINE onotElem #-}

instance MonoFoldable (TreeMap 3) where
  ofoldMap     f   = ofoldMap     f   . _treeMap
  ofoldr       f x = ofoldr       f x . _treeMap
  ofoldl'      f x = ofoldl'      f x . _treeMap
  ofoldr1Ex    f   = ofoldr1Ex    f   . _treeMap
  ofoldl1Ex'   f   = ofoldl1Ex'   f   . _treeMap
  headEx           = headEx           . _treeMap
  lastEx           = lastEx           . _treeMap
  maximumByEx  f   = maximumByEx  f   . _treeMap
  minimumByEx  f   = minimumByEx  f   . _treeMap
  oelem          x = oelem          x . _treeMap
  onotElem       x = onotElem       x . _treeMap
  {-# INLINE ofoldMap #-}
  {-# INLINE ofoldr   #-}
  {-# INLINE ofoldl'  #-}
  {-# INLINE headEx   #-}
  {-# INLINE lastEx   #-}
  {-# INLINE oelem    #-}
  {-# INLINE onotElem #-}

------------------------------------------------------------------------------
instance GrowingAppend (TreeMap 1)
instance GrowingAppend (TreeMap 2)
instance GrowingAppend (TreeMap 3)

-- ** DataContainers instances
------------------------------------------------------------------------------

-- ** Set-like and Map-like instances
------------------------------------------------------------------------------
{-- }
instance SetElems (PCoord 1) (Maybe TreeID) (TreeMap 1) where
  p <~ TreeMap ts = tcoord p <~ ts
  {-# INLINE (<~) #-}
--}

instance SetElems (PCoord 2) (Maybe TreeID) (TreeMap 2) where
  p <~ TreeMap ts = case catMaybes $ (<~ ts) <$> tcoords p of
    []  -> Nothing
    x:_ -> Just x
  {-# INLINE (<~) #-}

{-- }
instance SetElems (PCoord 3) (Maybe TreeID) (TreeMap 3) where
  PCoord p <~ TreeMap ts = p^._xy <~ ts
  {-# INLINE (<~) #-}
--}

------------------------------------------------------------------------------
instance SetIndex (PCoord 2) (TreeMap 2) where
  p <? TreeMap ts = (<? ts) `any` tcoords p
  p <\ TreeMap ts = TreeMap $ tcoord p <\ ts
  {-# INLINE (<?) #-}
  {-# INLINE (<\) #-}

instance SetInsert (PCoord 2, TreeID) (TreeMap 2) where
  (p, t) ~> TreeMap ts = TreeMap $ (tcoord p, t) ~> ts

------------------------------------------------------------------------------
-- | Construct new maps from @PCoord@s to values.
instance SetBuild (PCoord 2, TreeID) (TreeMap 2) where
  bare = TreeMap bare
  unit = TreeMap . unit . first tcoord
  {-# INLINE bare #-}
  {-# INLINE unit #-}

-- ** JSON converter instances
------------------------------------------------------------------------------
-- | Import/export from/to JSON files.
instance ToJSON   (TreeMap 1)
instance FromJSON (TreeMap 1)

instance ToJSON   (TreeMap 2)
instance FromJSON (TreeMap 2)

instance ToJSON   (TreeMap 3)
instance FromJSON (TreeMap 3)


-- * Helper functions
------------------------------------------------------------------------------
-- | Generates extra tree-coordinates (if required) to handle cases for tree
--   boundaries.
tcoords :: PCoord 2 -> [V2 Int]
tcoords p@(PCoord (V3 x y _)) =
  let rx = if (w-1) .&. abs x == 0 then [u, u-1] else [u]
      ry = if (w-1) .&. abs y == 0 then [v, v-1] else [v]
      V2 u v = tcoord p
      w  = maxWidth (undefined :: PCoord 2)
  in  [V2 s t | s <- rx, t <- ry]
