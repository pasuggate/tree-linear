{-# LANGUAGE PolyKinds, DataKinds, TypeOperators, RankNTypes, TupleSections,
             OverloadedStrings, PatternSynonyms, ViewPatterns,
             GeneralizedNewtypeDeriving, DeriveGeneric, DeriveFunctor,
             StandaloneDeriving, TypeFamilies, FlexibleInstances,
             MultiParamTypeClasses, FlexibleContexts, DefaultSignatures,
             TemplateHaskell, DeriveFoldable, DeriveTraversable,
             ScopedTypeVariables, InstanceSigs
  #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Node
-- Copyright   : (C) Patrick Suggate 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Nodes of meshes built from linear 2^d-forests.
-- 
-- Changelog:
--  + 15/06/2019  --  initial file;
-- 
-- TODO:
--  + separate out the @PCoord@s stuff into a separate module?
-- 
------------------------------------------------------------------------------

module Data.Forest.Node
  ( Node (..)
  , pos
  , lvl
  , tId
    -- Parametric coordinates:
  , Parametric (..)
  , PCoord (..)
  , pcoordVec
  , PCoords (..)
  , pcoordMap
    -- Some conversions:
  , toList2
  , nodesOfLeaf
  , firstNodeOfLeaf
  , oddNodeOfLeaf
  , leavesOfNode
  , leafFromNode
  , unsafeLeafFromNode
  , leafCentreNode
  , unsafeLeafCentreNode
    -- Queries:
  , isNodeOdd
  , isNodeEven
  , atCoord
  , diagPCoords
  , nbrhdOf
  , nbrsOf
    -- Import/export:
  , yamlPCoords
    -- Helpers
  , pcoordFrac
  , pcoordShow
  ) where

import GHC.Generics (Generic)
import GHC.Word
import GHC.Int
import GHC.TypeNats
import GHC.Exts (IsList (..))
import Foreign.Storable
import Foreign.Ptr
import Foreign.C.Types
import Control.Arrow (first, (<<<), (>>>), (&&&))
import Control.Lens (makeLenses, set, (&), (^.), (.~), (%~))
import Control.Monad.IO.Class
import Control.DeepSeq
import Data.Foldable
import Data.MonoTraversable
import Data.Containers
import qualified Data.List as List
import Data.Maybe (mapMaybe)
-- import Data.AffineSpace (AffineSpace (..))
import Data.VectorSpace.Orphans ()
import Data.IntSet.Sets
-- import qualified Data.IntSet as Set
import Data.IntMap.Maps
import qualified Data.IntMap as Map
import Data.Bits.Handy ((>>%), (.|.), (.&.), int, i32, complement)
import Data.Aeson
-- import qualified Data.ByteString.Lazy as Lazy
import qualified Data.ByteString as BS
import qualified Data.Yaml.Pretty as Y
import Linear.Handy hiding (unit)
import Text.Printf

import Data.Index.ZCurve
import Data.IntMap.CoordMap
-- import Data.Tree.Linear.Coords
import Data.Forest.Linear


-- * Type-function instances
------------------------------------------------------------------------------
type instance DimsOf (Node    d  ) = d
type instance DimsOf (PCoord  d  ) = d
type instance DimsOf (PCoords d a) = d


-- * Type-classes for parametric coordinates
------------------------------------------------------------------------------
class Parametric a where
  pcoord :: (d :: Nat) ~ DimsOf a => a -> Node d -> PCoord d


-- ** Meshing-related types
------------------------------------------------------------------------------
-- | Forest-/mesh- nodes.
data Node (d :: Nat) = Node { _pos :: Vec d Int  -- ^ position
                            , _lvl :: Int        -- ^ node level
                            , _tId :: Int        -- ^ tree ID
                            } deriving (Generic)

deriving instance Eq   (Node 2)
deriving instance Show (Node 2)

deriving instance Eq   (Node 3)
deriving instance Show (Node 3)

------------------------------------------------------------------------------
-- | Parametric coordinates data type, for flat (Euclidean) parameter spaces.
--   
--   Representation is fixed-point, for the first @d :: Nat@ integral values,
--   and with @maxDepth (_ :: Node d)@ fractional bits. The final value of the
--   array stores the node-depth, as this allows nodes to be compared without
--   first converting to "canonical" values.
--   
--   NOTE:
--    + in general, it is difficult to invert the conversions to @PCoord@
--      values, since the tree-id is not stored.
--    + storage is @V3 x y z@, where @z@ is the refinement level.
--   
newtype PCoord (d :: Nat) =
  PCoord { _pcoordVec :: Vec (d+1) Int }
  deriving (Generic)

deriving instance Storable (PCoord 1)
deriving instance Storable (PCoord 2)
deriving instance Storable (PCoord 3)

deriving instance Eq   (PCoord 1)
deriving instance Eq   (PCoord 2)
deriving instance Eq   (PCoord 3)

deriving instance Show (PCoord 1)
deriving instance Show (PCoord 2)
deriving instance Show (PCoord 3)

------------------------------------------------------------------------------
-- | Maps from parametric coordinates to values.
newtype PCoords (d :: Nat) a =
  PCoords { _pcoordMap :: IntMap (ZMap d a) }
  deriving (Generic)
-- newtype PCoords (d :: Nat) a = PCoords { _pcoordMap :: ZMap (d+1) a }

deriving instance Eq   a => Eq   (PCoords 1 a)
deriving instance Eq   a => Eq   (PCoords 2 a)
deriving instance Eq   a => Eq   (PCoords 3 a)

deriving instance Show a => Show (PCoords 1 a)
deriving instance Show a => Show (PCoords 2 a)
deriving instance Show a => Show (PCoords 3 a)

deriving instance Functor     (PCoords 1)
deriving instance Foldable    (PCoords 1)
deriving instance Traversable (PCoords 1)

deriving instance Functor     (PCoords 2)
deriving instance Foldable    (PCoords 2)
deriving instance Traversable (PCoords 2)

deriving instance Functor     (PCoords 3)
deriving instance Foldable    (PCoords 3)
deriving instance Traversable (PCoords 3)


-- * Lenses and instances
------------------------------------------------------------------------------
makeLenses ''Node
makeLenses ''PCoord
makeLenses ''PCoords

-- ** Strictness instances
------------------------------------------------------------------------------
instance NFData (Node 2)
instance NFData (Node 3)

------------------------------------------------------------------------------
instance             NFData (PCoord 2)
instance             NFData (PCoord 3)

instance NFData a => NFData (PCoords 2 a)
instance NFData a => NFData (PCoords 3 a)

-- ** JSON converter instances
------------------------------------------------------------------------------
-- | Import/export from/to JSON files.
instance ToJSON   (Node 2)
instance FromJSON (Node 2)

instance ToJSON   (Node 3)
instance FromJSON (Node 3)

-- ** Standard instances
------------------------------------------------------------------------------
-- | Define an ordering.
--   
--   NOTE: not a "Z-ordering", since nodes are not required to have Z-ordering
--     compatible locations.
instance Ord (Node 2) where
  Node ab c d `compare` Node ef g h
    | d < h     = LT
    | d > h     = GT
    | c < g     = LT
    | c > g     = GT
    | otherwise = compare ab ef

-- | Define an ordering.
--   
--   NOTE: not a "Z-ordering", since nodes are not required to have Z-ordering
--     compatible locations.
instance Ord (Node 3) where
  Node ab c d `compare` Node ef g h
    | d < h     = LT
    | d > h     = GT
    | c < g     = LT
    | c > g     = GT
    | otherwise = compare ab ef

-- ** Node-within-forest instances
------------------------------------------------------------------------------
instance Layered (Node 2) where
  maxDepth = const $ maxDepth (undefined :: Leaf 2)
  depth    = _lvl
  {-# INLINE maxDepth #-}
  {-# INLINE depth    #-}

instance HasWidth (Node 2) where
  maxWidth = const $ maxWidth (undefined :: Leaf 2)
  width    = (maxWidth (undefined :: Node 2) >>%) . depth
  {-# INLINE maxWidth #-}
  {-# INLINE width    #-}

instance HasTreeID (Node 2) where
  treeId = toEnum . _tId
  {-# INLINE treeId #-}

instance HasCoords (Node 2) where
  type CoordVec (Node 2) = V2 Int
  coords = _pos
  {-# INLINE coords #-}

------------------------------------------------------------------------------
-- | Serialisers for parametric coordinates.
instance ToJSON   (PCoord 2)
instance FromJSON (PCoord 2)

instance ToJSON   a => ToJSON   (PCoords 2 a)
instance FromJSON a => FromJSON (PCoords 2 a)

-- ** Instances for parametric coordinates
------------------------------------------------------------------------------
instance Layered (PCoord 2) where
  maxDepth = const $ maxDepth (undefined :: Leaf 2)
  depth    = (^._z) . _pcoordVec
  {-# INLINE maxDepth #-}
  {-# INLINE depth    #-}

instance HasWidth (PCoord 2) where
  maxWidth = const $ maxWidth (undefined :: Leaf 2)
  width    = (maxWidth (undefined :: Leaf 2) >>%) . depth
  {-# INLINE maxWidth #-}
  {-# INLINE width    #-}

-- ** More standard instances
------------------------------------------------------------------------------
instance Semigroup (PCoords d a) where
  PCoords ps <> PCoords qs = PCoords $ ps <> qs
  {-# INLINE (<>) #-}

instance Monoid (PCoords d a) where
  mempty = PCoords Map.empty
  {-# INLINE mempty #-}

-- ** MonoTraversable instances
------------------------------------------------------------------------------
type instance Element (PCoords d a) = a

instance Functor     (PCoords d) => MonoFunctor     (PCoords d a)
instance Foldable    (PCoords d) => MonoFoldable    (PCoords d a)
instance Traversable (PCoords d) => MonoTraversable (PCoords d a)

instance GrowingAppend (PCoords 1 a)
instance GrowingAppend (PCoords 2 a)
instance GrowingAppend (PCoords 3 a)

-- ** DataContainers instances
------------------------------------------------------------------------------
instance SetContainer (PCoords 1 a) where
  type ContainerKey (PCoords 1 a) = PCoord 1
  member    = (<?)
  notMember = (>?)
  PCoords ps `union`        PCoords qs = PCoords $ ps `union`        qs
  PCoords ps `difference`   PCoords qs = PCoords $ ps `difference`   qs
  PCoords ps `intersection` PCoords qs = PCoords $ ps `intersection` qs
  keys      = fmap fst . flat
  {-# INLINE member       #-}
  {-# INLINE notMember    #-}
  {-# INLINE union        #-}
  {-# INLINE difference   #-}
  {-# INLINE intersection #-}
  {-# INLINE keys         #-}

instance SetContainer (PCoords 2 a) where
  type ContainerKey (PCoords 2 a) = PCoord 2
  member    = (<?)
  notMember = (>?)
  PCoords ps `union`        PCoords qs = PCoords $ ps `union`        qs
  PCoords ps `difference`   PCoords qs = PCoords $ ps `difference`   qs
  PCoords ps `intersection` PCoords qs = PCoords $ ps `intersection` qs
  keys      = fmap fst . flat
  {-# INLINE member       #-}
  {-# INLINE notMember    #-}
  {-# INLINE union        #-}
  {-# INLINE difference   #-}
  {-# INLINE intersection #-}
  {-# INLINE keys         #-}

instance SetContainer (PCoords 3 a) where
  type ContainerKey (PCoords 3 a) = PCoord 3
  member    = (<?)
  notMember = (>?)
  PCoords ps `union`        PCoords qs = PCoords $ ps `union`        qs
  PCoords ps `difference`   PCoords qs = PCoords $ ps `difference`   qs
  PCoords ps `intersection` PCoords qs = PCoords $ ps `intersection` qs
  keys      = fmap fst . flat
  {-# INLINE member       #-}
  {-# INLINE notMember    #-}
  {-# INLINE union        #-}
  {-# INLINE difference   #-}
  {-# INLINE intersection #-}
  {-# INLINE keys         #-}

------------------------------------------------------------------------------
instance IsMap (PCoords 1 a) where
  type MapValue (PCoords 1 a) = a
  lookup              = (<~)
  insertMap       k x = ((k, x) ~>)
  deleteMap           = (<\)
  singletonMap        = curry unit
  mapFromList         = flip (~>) `foldl'` bare
  mapToList           = flat
  insertWith    f k x = alterMap (Just . maybe x (f   x)) k
  insertWithKey f k x = alterMap (Just . maybe x (f k x)) k
  adjustMap     f     = alterMap (fmap f)
  adjustWithKey f k   = alterMap (fmap $ f k) k
  updateMap     f     = alterMap (>>= f)
  updateWithKey f k   = alterMap (>>= f k) k
  alterMap f (PCoord (V2 i j)) (PCoords ps) =
    PCoords $ Map.alter (alterMap f (V1 j) <$>) i ps
  filterMap f (PCoords ps) = PCoords . clean $ filterMap f <$> ps
  {-# INLINE lookup    #-}
  {-# INLINE insertMap #-}
  {-# INLINE deleteMap #-}
  {-# INLINE mapToList #-}
  {-# INLINE alterMap  #-}

instance forall a. IsMap (PCoords 2 a) where
  type MapValue (PCoords 2 a) = a
  lookup              = (<~)
  insertMap       k x = ((k, x) ~>)
  deleteMap           = (<\)
  singletonMap        = curry unit
  mapFromList         = flip (~>) `foldl'` bare
  mapToList           = flat
  insertWith    f k x = alterMap (Just . maybe x (f   x)) k
  insertWithKey f k x = alterMap (Just . maybe x (f k x)) k
  adjustMap     f     = alterMap (fmap f)
  adjustWithKey f k   = alterMap (fmap $ f k) k
  updateMap     f     = alterMap (>>= f)
  updateWithKey f k   = alterMap (>>= f k) k
  alterMap f (PCoord (V3 i j k)) (PCoords ps) =
    let fn :: Maybe (ZMap 2 a) -> Maybe (ZMap 2 a)
        fn  = maybe (unit . (k',) <$> f Nothing) (Just . alterMap f k')
        k'  = V2 j k
    in  PCoords $ Map.alter fn i ps
--     PCoords $ Map.alter (alterMap f (V2 j k) <$>) i ps
  filterMap f (PCoords ps) = PCoords . clean $ filterMap f <$> ps
  {-# INLINE lookup    #-}
  {-# INLINE insertMap #-}
  {-# INLINE deleteMap #-}
  {-# INLINE mapToList #-}
  {-# INLINE alterMap  #-}

instance IsMap (PCoords 3 a) where
  type MapValue (PCoords 3 a) = a
  lookup              = (<~)
  insertMap       k x = ((k, x) ~>)
  deleteMap           = (<\)
  singletonMap        = curry unit
  mapFromList         = flip (~>) `foldl'` bare
  mapToList           = flat
  insertWith    f k x = alterMap (Just . maybe x (f   x)) k
  insertWithKey f k x = alterMap (Just . maybe x (f k x)) k
  adjustMap     f     = alterMap (fmap f)
  adjustWithKey f k   = alterMap (fmap $ f k) k
  updateMap     f     = alterMap (>>= f)
  updateWithKey f k   = alterMap (>>= f k) k
  alterMap f (PCoord (V4 i j k l)) (PCoords ps) =
    let fn :: Maybe (ZMap 3 a) -> Maybe (ZMap 3 a)
        fn  = maybe (unit . (k',) <$> f Nothing) (Just . alterMap f k')
        k'  = V3 j k l
    in  PCoords $ Map.alter fn i ps
--     PCoords $ Map.alter (alterMap f (V3 j k l) <$>) i ps
  filterMap f (PCoords ps) = PCoords . clean $ filterMap f <$> ps
  {-# INLINE lookup    #-}
  {-# INLINE insertMap #-}
  {-# INLINE deleteMap #-}
  {-# INLINE mapToList #-}
  {-# INLINE alterMap  #-}

clean :: SetSize a => IntMap a -> IntMap a
clean  = Map.filter (not . dry)
{-# INLINE clean #-}

-- ** Parametric-coordinates set and map instances
------------------------------------------------------------------------------
-- | Construct new maps from @PCoord@s to values.
instance SetBuild (PCoord 1, a) (PCoords 1 a) where
  bare = PCoords bare
  unit = PCoords . unit . go . first _pcoordVec where
    go = (^._y) . fst &&& unit . first (V1 . (^._x))
  {-# INLINE bare #-}
  {-# INLINE unit #-}

-- | Insert parametric coordinates.
instance SetInsert (PCoord 1, a) (PCoords 1 a) where
  (PCoord p, x) ~> PCoords zs =
    let go (Just qs) = Just $ (V1 (p^._x), x) ~> qs
        go   Nothing = Just $ unit (V1 (p^._x), x)
    in  PCoords $ Map.alter go (p^._y) zs
  {-# INLINE (~>) #-}

-- | Remove and query by element.
instance SetIndex (PCoord 1) (PCoords 1 a) where
  PCoord p <\ PCoords zs =
    PCoords $ Map.alter (fmap (V1 (p^._x) <\)) (p^._y) zs
  PCoord p <? PCoords zs = case p^._y <~ zs of
    Just qs -> V1 (p^._x) <? qs
    Nothing -> False
  {-# INLINE (<\) #-}
  {-# INLINE (<?) #-}

-- | Fetch from @PCoord@-maps.
instance SetElems (PCoord 1) (Maybe a) (PCoords 1 a) where
  PCoord p <~ PCoords zs = p^._y <~ zs >>= (V1 (p^._x) <~)
  {-# INLINE (<~) #-}

-- | Flatten into a coordinate-list
instance SetFlat (PCoord 1, a) (PCoords 1 a) where
  flat = concatMap (uncurry go) . flat . _pcoordMap where
    go :: Int -> ZMap 1 a -> [(PCoord 1, a)]
    go l = let l' = fromIntegral l :: V2 Int
           in  map (first (PCoord <<< set _x `flip` l' <<< (^._x))) . flat
  {-# INLINE flat #-}

------------------------------------------------------------------------------
-- | Construct new maps from @PCoord@s to values.
instance SetBuild (PCoord 2, a) (PCoords 2 a) where
  bare = PCoords bare
  unit = PCoords . unit . go . first _pcoordVec where
    go = (^._z) . fst &&& unit . first (^._xy)
  {-# INLINE bare #-}
  {-# INLINE unit #-}

-- | Insert parametric coordinates.
instance SetInsert (PCoord 2, a) (PCoords 2 a) where
  (PCoord p, x) ~> PCoords zs =
    let go (Just qs) = Just $ (p^._xy, x) ~> qs
        go   Nothing = Just $ unit (p^._xy, x)
    in  PCoords $ Map.alter go (p^._z) zs
  {-# INLINE (~>) #-}

-- | Remove and query by element.
instance SetIndex (PCoord 2) (PCoords 2 a) where
  PCoord p <\ PCoords zs = PCoords $ Map.alter (fmap (p^._xy <\)) (p^._z) zs
  PCoord p <? PCoords zs = case p^._z <~ zs of
    Just qs -> p^._xy <? qs
    Nothing -> False
  {-# INLINE (<\) #-}
  {-# INLINE (<?) #-}

-- | Fetch from @PCoord@-maps.
instance SetElems (PCoord 2) (Maybe a) (PCoords 2 a) where
  PCoord p <~ PCoords zs = p^._z <~ zs >>= (p^._xy <~)
  {-# INLINE (<~) #-}

-- | Flatten into a coordinate-list
instance SetFlat (PCoord 2, a) (PCoords 2 a) where
  flat = concatMap (uncurry go) . flat . _pcoordMap where
    go :: Int -> ZMap 2 a -> [(PCoord 2, a)]
    go l = map (first (PCoord <<< set _xy `flip` (fromIntegral l :: V3 Int))) . flat
  {-# INLINE flat #-}

------------------------------------------------------------------------------
-- | Construct new maps from @PCoord@s to values.
instance SetBuild (PCoord 3, a) (PCoords 3 a) where
  bare = PCoords bare
  unit = PCoords . unit . go . first _pcoordVec where
    go = (^._w) . fst &&& unit . first (^._xyz)
  {-# INLINE bare #-}
  {-# INLINE unit #-}

-- | Insert parametric coordinates.
instance SetInsert (PCoord 3, a) (PCoords 3 a) where
  (PCoord p, x) ~> PCoords zs =
    let go (Just qs) = Just $ (p^._xyz, x) ~> qs
        go   Nothing = Just $ unit (p^._xyz, x)
    in  PCoords $ Map.alter go (p^._w) zs
  {-# INLINE (~>) #-}

-- | Remove and query by element.
instance SetIndex (PCoord 3) (PCoords 3 a) where
  PCoord p <\ PCoords zs = PCoords $ Map.alter (fmap (p^._xyz <\)) (p^._w) zs
  PCoord p <? PCoords zs = case p^._w <~ zs of
    Just qs -> p^._xyz <? qs
    Nothing -> False
  {-# INLINE (<\) #-}
  {-# INLINE (<?) #-}

-- | Fetch from @PCoord@-maps.
instance SetElems (PCoord 3) (Maybe a) (PCoords 3 a) where
  PCoord p <~ PCoords zs = p^._w <~ zs >>= (p^._xyz <~)
  {-# INLINE (<~) #-}

-- | Flatten into a coordinate-list
instance SetFlat (PCoord 3, a) (PCoords 3 a) where
  flat = concatMap (uncurry go) . flat . _pcoordMap where
    go :: Int -> ZMap 3 a -> [(PCoord 3, a)]
    go l = map (first (PCoord <<< set _xyz `flip` (fromIntegral l :: V4 Int))) . flat
  {-# INLINE flat #-}

-- ** Map-like instances
------------------------------------------------------------------------------
instance SetInsert (Node 2, a) (CoordMap 2 a) where
  (~>) (Node p l t, x) =
    let qx = (,x) $ _xy .~ p $ fromIntegral l
    in  coordMap %~ Map.alter (Just . maybe (unit qx) (qx~>)) t
  {-# INLINE (~>) #-}

instance SetElems (Node 2) (Maybe a) (CoordMap 2 a) where
  (<~) (Node p l t) =
    let q = _xy .~ p $ fromIntegral l
    in  maybe Nothing (q<~) . (t<~) . _coordMap
  {-# INLINE (<~) #-}

instance SetIndex (Node 2) (CoordMap 2 a) where
  (<?) (Node p l t) =
    let q = _xy .~ p $ fromIntegral l
    in  maybe False (q<?) . (t<~) . _coordMap
  Node p l t <\ CoordMap cs =
    let q = _xy .~ p $ fromIntegral l
    in  CoordMap $ maybe cs ((~>cs) . (t,) . (q<\)) $ t <~ cs
  {-# INLINE (<?) #-}
  {-# INLINE (<\) #-}

instance SetBuild (Node 2, a) (CoordMap 2 a) where
  bare = CoordMap bare
  unit = (~> bare)
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetFlat (Node 2, a) (CoordMap 2 a) where
  flat = toList2
  {-# INLINE flat #-}

-- ** List-like instances
------------------------------------------------------------------------------
instance IsList (CoordMap 2 a) where
  type Item (CoordMap 2 a) = (Node 2, a)
  fromList    = flip (~>) `foldl'` bare
  fromListN _ = fromList
  toList      = toList2
  {-# INLINE fromList  #-}
  {-# INLINE fromListN #-}
  {-# INLINE toList    #-}

instance IsList (PCoords 2 a) where
  type Item (PCoords 2 a) = (PCoord 2, a)
  fromList    = flip (~>) `foldl'` bare
  fromListN _ = fromList
  toList      = flat
  {-# INLINE fromList  #-}
  {-# INLINE fromListN #-}
  {-# INLINE toList    #-}

-- ** Storable instances
------------------------------------------------------------------------------
instance Storable (Node 2) where
  sizeOf  _ = sizeOf (undefined :: CInt)*4
  alignment = const $ sizeOf (undefined :: Int)
  peek p = do let q = castPtr p :: Ptr (V2 CInt)
                  r = p `plusPtr` sizeOf (undefined :: V2 CInt) :: Ptr Int8
                  s = r `plusPtr` sizeOf (undefined :: CInt) :: Ptr CInt
              Node <$> fmap int `fmap` peek q <*> int `fmap` peek r <*> int `fmap` peek s
  poke p (Node xy l t) = do
    let q = castPtr p :: Ptr (V2 CInt)
        r = p `plusPtr` sizeOf (undefined :: V2 CInt)
        s = r `plusPtr` sizeOf (undefined :: CInt)
    poke q (fromIntegral <$> xy) >> poke r (fromIntegral l :: Word8) >> poke s (int t)
  {-# INLINE sizeOf #-}
  {-# INLINE alignment #-}
  {-# INLINE peek #-}
  {-# INLINE poke #-}

-- ** Custom instances
------------------------------------------------------------------------------
instance Relations (PCoord 2) where
  parent        :: PCoord 2 -> Maybe (PCoord 2)
  parent p@(PCoord (V3 x y l))
    | depth p > 0 = Just $ PCoord (V3 (x .&. r) (y .&. r) (l - 1))
    | otherwise   = Nothing
    where
      r = complement $ width p - 1
  lineage       :: PCoord 2 -> [PCoord 2]
  lineage    p  = go (parent p) where
    go (Just q) = q:go (parent q)
    go       _  = []
  progeny       :: PCoord 2 -> [PCoord 2]
  progeny p@(PCoord (V3 x y l))
    | depth p < m = [PCoord (V3 x' y' l') | y' <- [y, y+h], x' <- [x, x+h]]
    | otherwise   = []
    where
      (l', m) = (l+1, maxDepth (undefined :: PCoord 2))
      (w,  h) = (maxWidth (undefined :: PCoord 2), w >>% l')
  adjacent      :: PCoord 2 -> [PCoord 2]
  adjacent       = nbrsOf
  neighbourhood :: PCoord 2 -> [PCoord 2]
  neighbourhood  = nbrhdOf
  {-# INLINE parent        #-}
  {-# INLINE progeny       #-}
  {-# INLINE lineage       #-}
  {-# INLINE adjacent      #-}
  {-# INLINE neighbourhood #-}

-- * Coordinate-map functions
------------------------------------------------------------------------------
toList2 :: CoordMap 2 a -> [(Node 2, a)]
toList2 (CoordMap cs) = uncurry go `concatMap` flat cs where
  go t = map (\(V3 x y l, a) -> (Node (V2 x y) l t, a)) . flat
  {-# INLINE go #-}


-- * Additional conversions for nodes
------------------------------------------------------------------------------
-- | Generate the four nodes of a leaf-cell, and return them in Z-order.
nodesOfLeaf :: Leaf 2 -> [Node 2]
nodesOfLeaf c =
  let (h, l') = (width c, depth c)
      V2 u v  = fromIntegral <$> c^.coord
      t'      = fromEnum $ c^.ctree
  in  [Node (V2 x y) l' t' | y <- [v, v+h], x <- [u, u+h]]

firstNodeOfLeaf :: Leaf 2 -> Node 2
firstNodeOfLeaf (Leaf uv l t) =
  Node (fromIntegral <$> uv) (fromIntegral l) (fromEnum t)
{-# INLINE firstNodeOfLeaf #-}

------------------------------------------------------------------------------
-- | Every leaf-cell contains just one "odd" node -- where an "odd node" has
--   '1' LSB's in the rightmost position possible, for all of its (parametric)
--   coordinates. E.g., for `Leaf (V2 0 0) 0 t`, this would have its odd node
--   as `Node (V2 w w) 0 t`, where `w` is the `maxWidth` of a cell.
--   
--   NOTE:
--    + so this function returns the "oddest" possible node, with the given
--      leaf.
--    + this function only considers the current tree, not its "or-ness" when
--      considering its absolute parametric coordinates; i.e., depending on
--      where the tree sits within the entire forest.
--    + will not work for leaves with negative (or, unaligned) values for its
--      coordinates.
--   
oddNodeOfLeaf :: Leaf 2 -> Node 2
oddNodeOfLeaf q@(Leaf uv l t) = Node xy (int l) (fromEnum t) where
  xy = (width q .|.) . int <$> uv
{-# INLINE oddNodeOfLeaf #-}

------------------------------------------------------------------------------
-- | Generate the (same-level) leaf-cells that could be incident to the given
--   (independent) node.
leavesOfNode :: Node 2 -> [Leaf 2]
leavesOfNode p = [Leaf (V2 u v) l t | v<-[y-dw, y], u<-[x-dw, x]] where
  -- convert 'Node' fields to approp int-types:
  V2 x y = i32 <$> p^.pos
  (l, t) = (fromIntegral $ p^.lvl, toEnum $ p^.tId)
  -- leaf-dimensions:
  dw = i32 $ width p
{-# INLINABLE leavesOfNode #-}

------------------------------------------------------------------------------
-- | Construct the leaf that has the given node as its "origin-point".
leafFromNode :: Node 2 -> Maybe (Leaf 2)
leafFromNode p@(Node (V2 x y) _ _)
  | x >= 0 && x < w
  , y >= 0 && y < w = Just $ unsafeLeafFromNode p
  | otherwise       = Nothing
  where
    w = maxWidth (undefined :: Leaf 2)

-- | Construct the leaf that has the given node as its "origin-point", and
--   without checking whether the node-coordinates lie within the leaf-cell.
unsafeLeafFromNode :: Node 2 -> Leaf 2
unsafeLeafFromNode (Node xy l t) =
  Leaf (i32 <$> xy) (fromIntegral l) (toEnum t)
{-# INLINE unsafeLeafFromNode #-}

------------------------------------------------------------------------------
-- | Compute the node at the centre of the given leaf-cell, if possible.
leafCentreNode :: Leaf 2 -> Maybe (Node 2)
leafCentreNode l
  | depth l >= m = Nothing
  | otherwise    = Just $ unsafeLeafCentreNode l
  where
    m = maxDepth (undefined :: Leaf 2)
{-# INLINE leafCentreNode #-}

unsafeLeafCentreNode :: Leaf 2 -> Node 2
unsafeLeafCentreNode  = level %~ succ >>> oddNodeOfLeaf
{-# INLINE unsafeLeafCentreNode #-}


-- * Queries
------------------------------------------------------------------------------
-- | (All-)Odd test for the given node.
isNodeOdd :: Node 2 -> Bool
isNodeOdd p@(Node xy _ _) = go xy where
  go = and . fmap ((/= 0) . (width p .&.))
{-# INLINE isNodeOdd #-}

-- | (All-)Even test for the given node.
isNodeEven :: Node 2 -> Bool
isNodeEven p@(Node xy _ _) = go xy where
  go = and . fmap ((== 0) . (width p .&.))
{-# INLINE isNodeEven #-}

------------------------------------------------------------------------------
-- | Search all levels of the coordinates-map for any nodes at the given
--   Euclidean coordinates.
atCoord :: forall a. V2 Int -> PCoords 2 a -> [(PCoord 2, a)]
atCoord q@(V2 x y) (PCoords ps) =
  let go :: Int -> ZMap 2 a -> Maybe (PCoord 2, a)
      go l zs = let p = V3 x y l in (PCoord p,) <$> q <~ zs
  in  uncurry go `mapMaybe` flat ps

------------------------------------------------------------------------------
-- | Find the parametric coordinates that are diagonally outwards from the
--   given point.
diagPCoords :: PCoord 2 -> [PCoord 2]
diagPCoords p = (p &) <$> ds where
  ds = [pcoordVec %~ (^+^ V3 u v 0) | v<-[-h, h], u<-[-h, h]]
  h  = width p
{-# INLINABLE diagPCoords #-}

-- ** Neighbourhood queries
------------------------------------------------------------------------------
nbrhdOf :: PCoord 2 -> [PCoord 2]
nbrhdOf p@(PCoord (V3 x y l)) =
  let dh = [ -width p, 0, -head dh ]
  in  [ PCoord (V3 (x+dx) (y+dy) l) | dy <- dh, dx <- dh ]

nbrsOf :: PCoord 2 -> [PCoord 2]
nbrsOf p = nbrhdOf p List.\\ [p]


-- * JSON & YAML for save/load
------------------------------------------------------------------------------
yamlPCoords :: (MonadIO m, ToJSON a) => FilePath -> PCoords 2 a -> m ()
yamlPCoords fp = liftIO . BS.writeFile fp . Y.encodePretty ycfg
  where
    ycfg = Y.setConfCompare compare Y.defConfig


-- * Helpers
------------------------------------------------------------------------------
-- | Display the fractional positions for the given parametric coordinate.
pcoordFrac :: PCoord 2 -> (Double, Double, Int)
pcoordFrac p@(PCoord (V3 x y l)) =
  let x' = fromIntegral x / fromIntegral (maxWidth p) :: Double
      y' = fromIntegral y / fromIntegral (maxWidth p) :: Double
  in  (x', y', l)
{-# INLINE pcoordFrac #-}

-- | Display the fractional positions for the given parametric coordinate.
pcoordShow :: PCoord 2 -> String
pcoordShow p = printf "(%1.6f, %1.6f, %2d)" x y l where
  (x, y, l) = pcoordFrac p
