{-# LANGUAGE GeneralizedNewtypeDeriving, CPP, TemplateHaskell, PolyKinds,
             OverloadedStrings, PatternSynonyms, ViewPatterns, RankNTypes,
             DeriveGeneric, DeriveFoldable, DeriveTraversable,
             TypeSynonymInstances, FlexibleInstances, ScopedTypeVariables,
             StandaloneDeriving, TypeFamilies, TemplateHaskell, InstanceSigs,
             MultiParamTypeClasses, FlexibleContexts, DefaultSignatures,
             NoMonomorphismRestriction, TupleSections, DataKinds, BangPatterns
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Linear
-- Copyright   : (C) Patrick Suggate 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Linear $2^d$-forests.
-- 
-- Changelog:
--  + 15/06/2019  --  initial file;
-- 
------------------------------------------------------------------------------

module Data.Forest.Linear
  ( Relations (..)
  , Rebaseable (..)
  , Refinable (..)
  , RefinableM (..)
  , Layered (..)
  , Lindex (..)
  , HasWidth (..)
  , HasCoords (..)
  , HasTreeID (..)
  --   type-families:
  , DimsOf
  , Vec
  -- Helper data-types:
  ---------------------
  --   keys:
  , Fkey (..)
  , ktree
  , kindx
  --   cells:
  , Leaf (..)
  , TreeID (..)
  , level
  , coord
  , ctree
  -- Linear -trees & -forests:
  ----------------------------
  , Lforest (..)
  , Xforest (..)
  , ltrees
  , xtrees
  , lforest
  --   import/export:
  , loadLforest
  , saveLforest
  , loadXforest
  , saveXforest
  --   additional queries:
  , minLeaf
  , maxLeaf
  , minFkey
  , maxFkey
  ) where

import Foreign.Storable
import Foreign.Ptr
import GHC.Generics (Generic)
import GHC.Int
import GHC.TypeNats
import GHC.Types (SPEC(..))
import Control.Arrow (first, (***), (&&&))
import Control.Lens (makeLenses, (^.), (%~))
import Control.Monad.IO.Class
import Control.DeepSeq
import Data.Foldable
import Data.MonoTraversable
import Data.Containers
import Data.Maybe (fromMaybe)
import Data.AffineSpace (AffineSpace (..))
import Data.VectorSpace.Orphans ()
import Data.IntSet.Sets
import qualified Data.IntSet as Set
import Data.IntMap.Maps
import qualified Data.IntMap as Map
import Data.Bits.Handy ((>>%), (<<%), int, i32, i64)
import Data.Aeson
import qualified Data.ByteString.Lazy as Lazy
import Linear.Handy hiding (unit)
-- import Text.Printf

import Data.Index.ZCurve
import Data.Tree.Linear (Ltree (..), Refinable (..))
import Data.Tree.Linear.Xtree (Xtree (..), RefinableM (..))


-- * Function-families for common operations
------------------------------------------------------------------------------
-- class HasTreeID t where
--   treeId :: t -> Int

{--}
class HasTreeID t where
  treeId :: t -> TreeID
  default treeId :: Integral t => t -> TreeID
  treeId  = fromIntegral
--}

------------------------------------------------------------------------------
-- | Compute new owner/tree for the given node, edge, cell, etc.
class Rebaseable p where
  type MeshOf p :: *
  rebase :: MeshOf p -> p -> Maybe p


-- * Type-function instances
------------------------------------------------------------------------------
type instance DimsOf (Lforest d  ) = d
type instance DimsOf (Xforest d a) = d
type instance DimsOf (Leaf    d  ) = d
type instance DimsOf (Fkey    d  ) = d


-- * Data types for linear forests
------------------------------------------------------------------------------
-- | Local-region-of-forest data-type.
newtype Lforest (d :: Nat)   = Lforest { _ltrees :: IntMap (Ltree d  ) }
  deriving (Eq, Generic)

-- | Local-region-of-forest data-type, with storage in the leaves.
newtype Xforest (d :: Nat) a = Xforest { _xtrees :: IntMap (Xtree d a) }
  deriving (Eq, Generic)

-- ** 2^d-cell data types
------------------------------------------------------------------------------
data Leaf (d :: Nat) = Leaf { _coord :: !(Vec d Int32)
                            , _level :: !Int8
                            , _ctree :: !TreeID
                            } deriving (Generic)

------------------------------------------------------------------------------
-- | Forest "keys" for maps from @Fkey@ to leaf-nodes (of forests).
data Fkey (d :: Nat) = Fkey { _ktree :: !TreeID, _kindx :: !Int64 }
  deriving (Eq, Ord, Show, Generic)

-- | Tree ID's are used to select a tree from the forest.
newtype TreeID = TreeID { _unTreeID :: Int32 }
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic, Num, Real, Integral,
            Storable)


-- * Lenses and instances
------------------------------------------------------------------------------
makeLenses ''Leaf
makeLenses ''Fkey
makeLenses ''Lforest
makeLenses ''Xforest

-- ** Some derived instances
------------------------------------------------------------------------------
deriving instance Eq   (Leaf 2)
deriving instance Show (Leaf 2)

deriving instance           Show (Lforest 2)
deriving instance Show a => Show (Xforest 2 a)

-- ** JSON converter instances
------------------------------------------------------------------------------
-- | Import/export from/to JSON files.
instance ToJSON   (Fkey 2)
instance FromJSON (Fkey 2)

instance ToJSON   (Fkey 3)
instance FromJSON (Fkey 3)

instance ToJSON   (Leaf 2)
instance FromJSON (Leaf 2)

instance ToJSON   (Leaf 3)
instance FromJSON (Leaf 3)

------------------------------------------------------------------------------
instance ToJSON   (Lforest 2)
instance FromJSON (Lforest 2)

instance ToJSON   a => ToJSON   (Xforest 2 a)
instance FromJSON a => FromJSON (Xforest 2 a)

instance ToJSON   TreeID
instance FromJSON TreeID

-- ** Standard instances
------------------------------------------------------------------------------
instance Ord (Leaf 2) where
  compare a b = reify a `compare` reify b
  {-# INLINE compare #-}

------------------------------------------------------------------------------
instance Semigroup (Lforest d) where
  Lforest ks <> Lforest ls = Lforest $ Map.unionWith (<>) ks ls

instance Semigroup (Xforest d a) where
  Xforest ks <> Xforest ls = Xforest $ Map.unionWith (<>) ks ls

instance Monoid (Lforest d) where
  mempty = Lforest mempty

instance Monoid (Xforest d a) where
  mempty = Xforest mempty

------------------------------------------------------------------------------
instance Functor (Xforest d) where
  fmap f = xtrees %~ fmap (fmap f)
  {-# INLINE fmap #-}

deriving instance Foldable    (Xforest d)
deriving instance Traversable (Xforest d)

-- ** MonoTraversable instances
------------------------------------------------------------------------------
type instance Element (Lforest d) = Fkey d
type instance Element (Xforest d a) = a

instance Functor     (Xforest d) => MonoFunctor     (Xforest d a)
instance Foldable    (Xforest d) => MonoFoldable    (Xforest d a)
instance Traversable (Xforest d) => MonoTraversable (Xforest d a)

instance GrowingAppend (Lforest d)
instance GrowingAppend (Xforest d a)

------------------------------------------------------------------------------
instance MonoFunctor (Lforest d) where
  omap :: (Fkey d -> Fkey d) -> Lforest d -> Lforest d
  omap f = foldl' (flip (~>)) bare . fmap f . flat
  {-# INLINE omap #-}

instance MonoFoldable (Lforest d) where
  ofoldMap  f = foldMap  f . otoList where
  ofoldr  f s = foldr  f s . otoList
  ofoldl' f s = foldl' f s . otoList
  otoList = flat
  onull   = dry
  olength = card
  ofoldr1Ex  f = foldr1 f . otoList
  ofoldl1Ex' f = uncurry (foldl' f) . (head &&& tail) . otoList
  {-# INLINE ofoldMap #-}
  {-# INLINE ofoldr   #-}
  {-# INLINE ofoldl'  #-}
  {-# INLINE otoList  #-}
  {-# INLINE onull    #-}
  {-# INLINE olength  #-}

-- ** DataContainers instances
------------------------------------------------------------------------------
instance SetContainer (Lforest d) where
  type ContainerKey (Lforest d) = Fkey d
  member    = (<?)
  notMember = (>?)
  keys      = let go t = fmap (Fkey (toEnum t) . i64) . Set.toList . _leaves
              in  concatMap (uncurry go) . Map.toList . _ltrees
  Lforest xs `union`        Lforest ys =
    let go = Map.unionWith union
    in  Lforest $ xs `go` ys
  Lforest xs `difference`   Lforest ys =
    let go = Map.differenceWith ((fn . ) . difference)
        fn zs | dry zs    = Nothing
              | otherwise = Just zs
    in  Lforest $ xs `go` ys
  Lforest xs `intersection` Lforest ys =
    let go = Map.intersectionWith intersection
    in  Lforest $ xs `go` ys
  {-# INLINE member       #-}
  {-# INLINE notMember    #-}
  {-# INLINE keys         #-}

instance SetContainer (Xforest d a) where
  type ContainerKey (Xforest d a) = Fkey d
  member    = (<?)
  notMember = (>?)
  keys      = let go t = fmap (Fkey (toEnum t) . i64) . Map.keys . _xeaves
              in  concatMap (uncurry go) . Map.toList . _xtrees
  Xforest xs `union`        Xforest ys =
    let go = Map.unionWith union
    in  Xforest $ xs `go` ys
  Xforest xs `difference`   Xforest ys =
    let go = Map.differenceWith ((fn . ) . difference)
        fn zs | dry zs    = Nothing
              | otherwise = Just zs
    in  Xforest $ xs `go` ys
  Xforest xs `intersection` Xforest ys =
    let go = Map.intersectionWith intersection
    in  Xforest $ xs `go` ys
  {-# INLINE member       #-}
  {-# INLINE notMember    #-}
  {-# INLINE keys         #-}

------------------------------------------------------------------------------
instance forall d a. IsMap (Xforest d a) where
  type MapValue (Xforest d a) = a
  lookup              = (<~)
  insertMap       k x = ((k, x) ~>)
  deleteMap           = (<\)
  singletonMap        = curry unit
  mapFromList         = flip (~>) `foldl'` bare
  mapToList           = flat
  insertWith    f k x = alterMap (Just . maybe x (f   x)) k
  insertWithKey f k x = alterMap (Just . maybe x (f k x)) k
  adjustMap     f     = alterMap (fmap f)
  adjustWithKey f k   = alterMap (fmap $ f k) k
  updateMap     f     = alterMap (>>= f)
  updateWithKey f k   = alterMap (>>= f k) k
  -- | Explicit instance for @alterMap@ as this function can be used to
  --   implement many others.
  alterMap :: (Maybe a -> Maybe a) -> Fkey d -> Xforest d a -> Xforest d a
  alterMap f (Fkey t k) (Xforest ts) =
    let k'  = Key (fromEnum k) :: Key d
        fn :: Maybe (Xtree d a) -> Maybe (Xtree d a)
        fn  = maybe (unit . (k',) <$> f Nothing) (Just . alterMap f k')
    in  Xforest $ Map.alter fn (fromEnum t) ts
  filterMap f = Xforest . clean . fmap (filterMap f) . _xtrees
  {-# INLINE lookup    #-}
  {-# INLINE insertMap #-}
  {-# INLINE deleteMap #-}
  {-# INLINE mapToList #-}
  {-# INLINE alterMap  #-}
  {-# INLINE filterMap #-}

clean :: SetSize a => IntMap a -> IntMap a
clean  = Map.filter (not . dry)
{-# INLINE clean #-}

------------------------------------------------------------------------------
-- | Polymorphic differences and intersections of linear forests.
--   
instance PolyMap (Xforest d) where
  differenceMap :: Xforest d a -> Xforest d b -> Xforest d a
  Xforest xs `differenceMap` Xforest ys =
    let go x = Just . differenceMap x
    in  Xforest $ Map.differenceWith go xs ys
  intersectionMap :: Xforest d a -> Xforest d b -> Xforest d a
  Xforest xs `intersectionMap` Xforest ys =
    Xforest $ Map.intersectionWith intersectionMap xs ys
  intersectionWithMap ::
    (a -> b -> c) -> Xforest d a -> Xforest d b -> Xforest d c
  intersectionWithMap f (Xforest xs) (Xforest ys) =
    Xforest $ Map.intersectionWith (intersectionWithMap f) xs ys
  {-# INLINE differenceMap       #-}
  {-# INLINE intersectionMap     #-}
  {-# INLINE intersectionWithMap #-}

-- ** Strictness instances
------------------------------------------------------------------------------
instance             NFData TreeID
instance             NFData (Fkey    d  )
instance             NFData (Leaf    2  )
instance             NFData (Lforest d  )
instance NFData a => NFData (Xforest d a)

-- ** Linear-space instances
------------------------------------------------------------------------------
instance AffineSpace (Leaf 2) where
  type Diff (Leaf 2) = V2 Int32
  Leaf xy1 _ _ .-. Leaf xy0 _ _ = xy1 ^-^ xy0
  Leaf xy0 l t .+^      xy1     = Leaf (xy0 ^+^ xy1) l t
  {-# INLINE (.-.) #-}
  {-# INLINE (.+^) #-}

instance AffineSpace (Leaf 3) where
  type Diff (Leaf 3) = V3 Int32
  Leaf xyz1 _ _ .-. Leaf xyz0 _ _ = xyz1 ^-^ xyz0
  Leaf xyz0 l t .+^      xyz1     = Leaf (xyz0 ^+^ xyz1) l t
  {-# INLINE (.-.) #-}
  {-# INLINE (.+^) #-}

-- ** Key/node conversions
------------------------------------------------------------------------------
instance Layered (Fkey 2) where
  maxDepth = const 30
  depth    = plen2 . int . _kindx
  {-# INLINE maxDepth #-}
  {-# INLINE depth    #-}

instance HasWidth (Fkey 2) where
  maxWidth = (1 <<%) . maxDepth
  width    = (maxWidth (undefined :: Fkey 2) >>%) . depth
  {-# INLINE maxWidth #-}
  {-# INLINE width    #-}

instance HasTreeID (Fkey 2) where
  treeId = _ktree
  {-# INLINE treeId #-}

instance HasCoords (Fkey 2) where
  type CoordVec (Fkey 2) = V2 Int
  coords = fmap int . _coord . lpath
  {-# INLINE coords #-}

------------------------------------------------------------------------------
instance Layered (Leaf 2) where
  maxDepth = const $ maxDepth (undefined :: Fkey 2)
  depth    = int . _level
  {-# INLINE maxDepth #-}
  {-# INLINE depth    #-}

instance HasWidth (Leaf 2) where
  maxWidth = (1 <<%) . maxDepth
  width    = (maxWidth (undefined :: Leaf 2) >>%) . depth
  {-# INLINE maxWidth #-}
  {-# INLINE width    #-}

instance HasTreeID (Leaf 2) where
  treeId = _ctree
  {-# INLINE treeId #-}

instance HasCoords (Leaf 2) where
  type CoordVec (Leaf 2) = V2 Int
  coords = fmap int . _coord
  {-# INLINE coords #-}

------------------------------------------------------------------------------
instance Lindex (Leaf 2) (Fkey 2) where
  reify (Leaf xy l t) = Fkey t k where
    s = maxDepth (undefined :: Fkey 2) - int l
    k = i64 $ int l `topath2` fmap ((>>% s) . int) (xy^._yx)
  lpath (Fkey t k) = Leaf (yx^._yx) l t where
    yx = fmap i32 $ maxDepth (undefined :: Fkey 2) `unpath2` int k
    l  = fromIntegral $ plen2 $ int k
  lroot = Leaf (V2 0 0) 0 $ toEnum (-1)
  {-# INLINE reify #-}
  {-# INLINE lpath #-}
  {-# INLINE lroot #-}

-- ** Relations
------------------------------------------------------------------------------
instance Relations (Fkey 2) where
  parent        (Fkey t k) = Fkey t . fromIntegral <$> parent        (key64 k)
  progeny       (Fkey t k) = Fkey t . fromIntegral <$> progeny       (key64 k)
  lineage       (Fkey t k) = Fkey t . fromIntegral <$> lineage       (key64 k)
  -- TODO: these won't work across tree boundaries:
  adjacent      (Fkey _ _) = error "won't work across tree boundaries"
  neighbourhood (Fkey _ _) = error "won't work across tree boundaries"
--   adjacent      (Fkey t k) = Fkey t . fromIntegral <$> adjacent      (key64 k)
--   neighbourhood (Fkey t k) = Fkey t . fromIntegral <$> neighbourhood (key64 k)

key64 :: Int64 -> Key 2
key64  = fromIntegral
{-# INLINE key64 #-}

-- ** Set-like instances
------------------------------------------------------------------------------
instance SetInsert (Fkey d) (Lforest d) where
  Fkey t k ~> Lforest lf = Lforest $ Map.alter go (int t) lf where
    go Nothing   = Just $ unit (ki2 k)
    go (Just xs) = Just $ ki2 k ~> xs
  {-# INLINE (~>) #-}

instance SetIndex (Fkey d) (Lforest d) where
  Fkey t k <? Lforest lf = fromMaybe False . fmap (ki2 k<?) $ int t <~ lf
  Fkey t k <\ Lforest lf = Lforest $ Map.alter (>>= go) (int t) lf where
    go x | dry x' = Nothing
         | True   = Just x'
         where x' = ki2 k <\ x
  {-# INLINE (<?) #-}
  {-# INLINE (<\) #-}

instance SetBuild (Fkey d) (Lforest d) where
  bare = Lforest bare
  unit = (~>bare)
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetFlat (Fkey d) (Lforest d) where
  flat = concatMap go . flat . _ltrees where
    go (t', ls) = Fkey (toEnum t') . i64 <$> flat ls
  {-# INLINE flat #-}

-- TODO: should the subtrees be tested for emptiness?
instance SetSize (Lforest d) where
  dry  = oall dry . _ltrees
  card = osum . fmap card . _ltrees
  {-# INLINE dry  #-}
  {-# INLINE card #-}

instance SetOps (Lforest d) where
  Lforest xs \/ Lforest ys = Lforest $ Map.unionWith (\/) xs ys
  Lforest xs /\ Lforest ys = Lforest $ Map.intersectionWith (/\) xs ys
  Lforest xs \\ Lforest ys = Lforest $ Map.differenceWith go xs ys where
    go x y | dry z     = Nothing
           | otherwise = Just z
           where z = x \\ y
  {-# INLINE (\/) #-}
  {-# INLINE (/\) #-}
  {-# INLINE (\\) #-}

-- ** Map-like instances
------------------------------------------------------------------------------
instance SetInsert (Fkey d, a) (Xforest d a) where
  (Fkey t k, x) ~> Xforest xf = Xforest $ Map.alter go (int t) xf where
    go Nothing   = Just $ unit (ki2 k, x)
    go (Just xs) = Just $ (ki2 k, x) ~> xs
  {-# INLINE (~>) #-}

instance SetElems (Fkey d) (Maybe a) (Xforest d a) where
  Fkey t k <~ Xforest xf = maybe Nothing (ki2 k<~) $ int t <~ xf
  {-# INLINE (<~) #-}

instance SetIndex (Fkey d) (Xforest d a) where
  Fkey t k <? Xforest xf = fromMaybe False . fmap (ki2 k<?) $ int t <~ xf
  Fkey t k <\ Xforest xf = Xforest $ Map.alter (>>= go) (int t) xf where
    go x | dry x' = Nothing
         | True   = Just x'
         where x' = ki2 k <\ x
  {-# INLINE (<?) #-}
  {-# INLINE (<\) #-}

-- TODO: should the subtrees be tested for emptiness?
instance SetSize (Xforest d a) where
  dry  = oall dry . _xtrees
  card = osum . fmap card . _xtrees
  {-# INLINE dry  #-}
  {-# INLINE card #-}

instance SetBuild (Fkey d, a) (Xforest d a) where
  bare = Xforest bare
  unit = (~>bare)
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetFlat (Fkey d, a) (Xforest d a) where
  flat = concatMap go . flat . _xtrees where
    go (t', xs) = first (Fkey (toEnum t') . i64) <$> flat xs
  {-# INLINE flat #-}

-- ** Storable instances
------------------------------------------------------------------------------
instance Storable (Fkey d) where
  sizeOf  _ = sizeOf (undefined :: Int64) * 2
  alignment = const $ sizeOf (undefined :: Int64)
  peek p = do let q = castPtr p
                  r = p `plusPtr` sizeOf (undefined :: Int64)
              Fkey <$> peek q <*> peek r
  poke p (Fkey t k) = do
    let q = castPtr p
        r = p `plusPtr` sizeOf (undefined :: Int64)
    poke q t >> poke r k
  {-# INLINE sizeOf #-}
  {-# INLINE alignment #-}
  {-# INLINE peek #-}
  {-# INLINE poke #-}

instance Storable (Leaf 2) where
  sizeOf  _ = sizeOf (undefined :: Int32)*4
  alignment = const $ sizeOf (undefined :: Int64)
  peek p = do let q = castPtr p :: Ptr (V2 Int32)
                  r = p `plusPtr` sizeOf (undefined :: Int64)
                  s = r `plusPtr` sizeOf (undefined :: Int32)
              Leaf <$> peek q <*> peek r <*> peek s
  poke p (Leaf xy l t) = do
    let q = castPtr p :: Ptr (V2 Int32)
        r = p `plusPtr` sizeOf (undefined :: Int64)
        s = r `plusPtr` sizeOf (undefined :: Int32)
    poke q xy >> poke r l >> poke s t
  {-# INLINE sizeOf #-}
  {-# INLINE alignment #-}
  {-# INLINE peek #-}
  {-# INLINE poke #-}

-- ** Refinement instances
------------------------------------------------------------------------------
instance Layered (Lforest 2) where
  maxDepth :: Lforest 2 -> Int
  maxDepth  = const $ maxDepth (undefined :: Ltree 2)
  depth    :: Lforest 2 -> Int
  depth     = maximum . map depth . Map.elems . _ltrees
  {-# INLINE maxDepth #-}

instance Refinable (Lforest 2) where
  refine :: (Fkey 2 -> Bool) -> Lforest 2 -> Lforest 2
  refine f = Lforest . Map.mapWithKey (refine . g) . _ltrees where
    g t = f . Fkey (toEnum t) . fromIntegral
  subdivide :: Lforest 2 -> Lforest 2
  subdivide lf
    | depth lf < maxDepth lf = refine (<?lf) lf
    | otherwise              = error "subdivide: subdivided tree too deep"

------------------------------------------------------------------------------
instance RefinableM (Xforest d a) where
  refineM ::
    forall m. Monad m =>
    (Fkey d -> a -> m [(Fkey d, a)]) -> Xforest d a -> m (Xforest d a)
  refineM f = go SPEC bare . flat where
    go :: SPEC -> Xforest d a -> [(Fkey d, a)] -> m (Xforest d a)
    go !_ r ((k, x):xs) = do
      r' <- f k x >>= \ys -> case ys of
        [ly] -> pure $ ly ~> r
        _    -> go SPEC r ys
      r' `seq` go SPEC r' xs
    go !_ r          _  = pure r


-- * Import/export
------------------------------------------------------------------------------
saveLforest :: (MonadIO m, ToJSON (Lforest d)) =>
  FilePath -> Lforest d -> m ()
saveLforest fp = liftIO . Lazy.writeFile fp . encode

loadLforest :: (MonadIO m, FromJSON (Lforest d)) =>
  FilePath -> m (Maybe (Lforest d))
loadLforest  = liftIO . fmap decode . Lazy.readFile

saveXforest :: (MonadIO m, ToJSON (Xforest d a)) =>
  FilePath -> Xforest d a -> m ()
saveXforest fp = liftIO . Lazy.writeFile fp . encode

loadXforest :: (MonadIO m, FromJSON (Xforest d a)) =>
  FilePath -> m (Maybe (Xforest d a))
loadXforest  = liftIO . fmap decode . Lazy.readFile


-- * Conversions
------------------------------------------------------------------------------
-- | Strip the leaf-data to give an 'Lforest'.
lforest :: Xforest d a -> Lforest d
lforest  = Lforest . (Ltree . Map.keysSet . _xeaves <$>) . _xtrees
{-# INLINE lforest #-}


-- * Additional queries
------------------------------------------------------------------------------
minLeaf :: Lindex (Leaf d) (Fkey d) => Lforest d -> Leaf d
minLeaf  =
  mkLeaf . (tid *** i64 . Set.findMin . _leaves) . Map.findMin . _ltrees
{-# INLINE minLeaf #-}

maxLeaf :: Lindex (Leaf d) (Fkey d) => Lforest d -> Leaf d
maxLeaf  =
  mkLeaf . (tid *** i64 . Set.findMax . _leaves) . Map.findMax . _ltrees
{-# INLINE maxLeaf #-}

------------------------------------------------------------------------------
minFkey :: Lforest d -> Fkey d
minFkey  =
  uncurry Fkey . (tid *** i64 . Set.findMin . _leaves) . Map.findMin . _ltrees
{-# INLINE minFkey #-}

maxFkey :: Lforest d -> Fkey d
maxFkey  =
  uncurry Fkey . (tid *** i64 . Set.findMax . _leaves) . Map.findMax . _ltrees
{-# INLINE maxFkey #-}


-- * Helper functions
------------------------------------------------------------------------------
ki2 :: Int64 -> Key d
ki2  = Key . int
{-# INLINE ki2 #-}

tid :: Int -> TreeID
tid  = TreeID . i32
{-# INLINE tid #-}

------------------------------------------------------------------------------
mkLeaf :: Lindex (Leaf d) (Fkey d) => (TreeID, Int64) -> Leaf d
mkLeaf  = lpath . uncurry Fkey
{-# INLINE mkLeaf #-}
