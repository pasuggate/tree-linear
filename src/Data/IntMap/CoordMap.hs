{-# LANGUAGE GeneralizedNewtypeDeriving, CPP, TemplateHaskell, PolyKinds,
             OverloadedStrings, PatternSynonyms, ViewPatterns, RankNTypes,
             DeriveGeneric, TypeSynonymInstances, FlexibleInstances,
             StandaloneDeriving, TypeFamilies, TemplateHaskell, DeriveFunctor,
             MultiParamTypeClasses, TypeOperators, FlexibleContexts,
             NoMonomorphismRestriction, TupleSections, DataKinds,
             DeriveFoldable
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.IntMap.CoordMap
-- Copyright   : (C) Patrick Suggate 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Maps from (integral) coordinates to arbitrary data.
-- 
-- == Changelog:
--  - 2019-05-26  --  initial file;
-- 
-- == TODO:
--  - additional queries for working with boundaries and corners;
-- 
------------------------------------------------------------------------------

module Data.IntMap.CoordMap
  ( CoordMap (..)
  , coordMap
  -- Export and import:
  -- ------------------
  , saveCoordMap
  , loadCoordMap
  , yamlCoordMap
  , module Data.IntMap.ZMaps
  ) where

import GHC.Generics (Generic)
import GHC.TypeNats
import Control.Monad.IO.Class
import Control.Lens (makeLenses)
import Control.DeepSeq
import Data.Foldable (foldl')
import Data.IntMap.ZMaps
import Data.Aeson
import qualified Data.ByteString.Lazy as Lazy
import qualified Data.ByteString as BS
import qualified Data.Yaml.Pretty as Y


-- * Coordinate-map data types
------------------------------------------------------------------------------
-- | Used for mapping @Node d -> a@, where:
--   
--   * first index to find the tree (or 'Nothing');
--   * 'ZMap' is queried by coordinate & level; and
--   * map contents are reached (or not).
--   
--   TODO: 'Functor', 'Foldable', 'Traversable' instances?
newtype CoordMap d a = CoordMap { _coordMap :: IntMap (ZMap (d+1) a) }
  deriving (Generic)


-- * Lenses and instances
------------------------------------------------------------------------------
makeLenses ''CoordMap

deriving instance Functor  (CoordMap 2)
deriving instance Functor  (CoordMap 3)

deriving instance Foldable (CoordMap 2)
deriving instance Foldable (CoordMap 3)

deriving instance Eq   a => Eq   (CoordMap 2 a)
deriving instance Show a => Show (CoordMap 2 a)

deriving instance Eq   a => Eq   (CoordMap 3 a)
deriving instance Show a => Show (CoordMap 3 a)

instance ToJSON   a => ToJSON   (CoordMap 2 a)
instance FromJSON a => FromJSON (CoordMap 2 a)

instance ToJSON   a => ToJSON   (CoordMap 3 a)
instance FromJSON a => FromJSON (CoordMap 3 a)

-- ** Strictness instances
------------------------------------------------------------------------------
instance NFData a => NFData (CoordMap 2 a)
instance NFData a => NFData (CoordMap 3 a)


-- ** Set-like instances
------------------------------------------------------------------------------
instance SetSize (CoordMap 2 a) where
  card = foldl' (+) 0 . fmap card . _coordMap
  dry  = or . fmap dry . _coordMap
  {-# INLINE card #-}
  {-# INLINE dry  #-}


-- * JSON import/export
------------------------------------------------------------------------------
saveCoordMap ::
     MonadIO m
  => ToJSON (CoordMap d a)
  => FilePath
  -> CoordMap d a
  -> m ()
saveCoordMap fp = liftIO . Lazy.writeFile fp . encode

loadCoordMap ::
     MonadIO m
  => FromJSON (CoordMap d a)
  => FilePath
  -> m (Maybe (CoordMap d a))
loadCoordMap  = liftIO . fmap decode . Lazy.readFile

yamlCoordMap ::
     MonadIO m
  => ToJSON (CoordMap d a)
  => FilePath
  -> CoordMap d a
  -> m ()
yamlCoordMap fp =
  let ycfg = Y.setConfCompare compare Y.defConfig
  in  liftIO . BS.writeFile fp . Y.encodePretty ycfg
