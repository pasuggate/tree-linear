{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs,
             TypeOperators, TupleSections, OverloadedLists, InstanceSigs,
             MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
             DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
             ViewPatterns, PatternSynonyms, ScopedTypeVariables,
             StandaloneDeriving, TypeSynonymInstances, TemplateHaskell,
             NoMonomorphismRestriction
  #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.IntMap.ZMaps
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Boundary-nodes data-structures and functions, for linear 2^d-trees
-- 
-- Changelog:
--  + 29/07/2018  --  initial file;
-- 
-- TODO:
--  + instance for `SetSize`, and maybe `SetOps`;
-- 
------------------------------------------------------------------------------

module Data.IntMap.ZMaps
  ( module Data.IntMap.Maps
  , ZMap (..)
  , ZVec
  ) where

import GHC.Generics (Generic)
import GHC.Exts (IsList (..))
import GHC.TypeNats
import Control.Arrow (first)
import Control.DeepSeq
import Control.Lens ((^.))
import Data.Foldable (foldl')
import Data.MonoTraversable
import Data.Containers
import Data.IntMap.Maps
import qualified Data.IntMap as Map
import Data.Aeson
import Linear (V1 (..), V2 (..), V3 (..), V4 (..), _x)


-- * ZMap data types
--
-- $zmapTypes
--
-- A data type of @ZMap@ \(d \alpha\) stores items of type \(\alpha\) and
-- these are indexed by coordinates \(x \in \mathbb{Z^d}\) .
------------------------------------------------------------------------------
-- | Use data type families so that the API can be generalised to nested
--   @IntMap@'s of any depth.
data family   ZMap (d :: Nat) a :: *
data instance ZMap 4 a = ZMap4 (IntMap (IntMap (IntMap (IntMap a))))
data instance ZMap 3 a = ZMap3 (IntMap (IntMap (IntMap a)))
data instance ZMap 2 a = ZMap2 (IntMap (IntMap a))
data instance ZMap 1 a = ZMap1 (IntMap a)

type family   ZVec (d :: Nat) :: *
type instance ZVec 4 = V4 Int
type instance ZVec 3 = V3 Int
type instance ZVec 2 = V2 Int
type instance ZVec 1 = V1 Int

-- ** Derived instances
------------------------------------------------------------------------------
-- | Auto-derive equality tests.
deriving instance Eq   a => Eq   (ZMap 1 a)
deriving instance Eq   a => Eq   (ZMap 2 a)
deriving instance Eq   a => Eq   (ZMap 3 a)
deriving instance Eq   a => Eq   (ZMap 4 a)

-- | Auto-derive instances for showing ZMap's.
deriving instance Show a => Show (ZMap 1 a)
deriving instance Show a => Show (ZMap 2 a)
deriving instance Show a => Show (ZMap 3 a)
deriving instance Show a => Show (ZMap 4 a)

-- | Construct 'Generic' instances for 'ZMap' types.
deriving instance Generic (ZMap 1 a)
deriving instance Generic (ZMap 2 a)
deriving instance Generic (ZMap 3 a)
deriving instance Generic (ZMap 4 a)

-- | Derive the ability to fold over @a@ elements.
deriving instance Foldable (ZMap 1)
deriving instance Foldable (ZMap 2)
deriving instance Foldable (ZMap 3)
deriving instance Foldable (ZMap 4)

-- | Derive the ability to traverse @ZMap@'s of @a@ elements.
deriving instance Traversable (ZMap 1)
deriving instance Traversable (ZMap 2)
deriving instance Traversable (ZMap 3)
deriving instance Traversable (ZMap 4)


-- * Type-class instances for @ZMap@'s
------------------------------------------------------------------------------
instance Functor (ZMap 1) where
  fmap f (ZMap1 cs) = ZMap1 $ fmap f cs
  {-# INLINE[2] fmap #-}

instance Functor (ZMap 2) where
  fmap f (ZMap2 cs) = ZMap2 $ map2d f cs
  {-# INLINE[2] fmap #-}

instance Functor (ZMap 3) where
  fmap f (ZMap3 cs) = ZMap3 $ map3d f cs
  {-# INLINE[2] fmap #-}

instance Functor (ZMap 4) where
  fmap f (ZMap4 cs) = ZMap4 $ map4d f cs
  {-# INLINE[2] fmap #-}

------------------------------------------------------------------------------
instance Semigroup (ZMap 1 a) where
  ZMap1 ys <> ZMap1 zs = ZMap1 $ ys <> zs

instance Semigroup (ZMap 2 a) where
  ZMap2 ys <> ZMap2 zs = ZMap2 $ ys <> zs

instance Semigroup (ZMap 3 a) where
  ZMap3 ys <> ZMap3 zs = ZMap3 $ ys <> zs

instance Semigroup (ZMap 4 a) where
  ZMap4 ys <> ZMap4 zs = ZMap4 $ ys <> zs

------------------------------------------------------------------------------
instance Monoid (ZMap 1 a) where
  mempty = ZMap1 mempty
  {-# INLINE mempty #-}

instance Monoid (ZMap 2 a) where
  mempty = ZMap2 mempty
  {-# INLINE mempty #-}

instance Monoid (ZMap 3 a) where
  mempty = ZMap3 mempty
  {-# INLINE mempty #-}

instance Monoid (ZMap 4 a) where
  mempty = ZMap4 mempty
  {-# INLINE mempty #-}

-- ** MonoTraversable instances for @ZMap@'s
------------------------------------------------------------------------------
type instance Element (ZMap d a) = a

instance Functor     (ZMap d) => MonoFunctor     (ZMap d a)
instance Foldable    (ZMap d) => MonoFoldable    (ZMap d a)
instance Traversable (ZMap d) => MonoTraversable (ZMap d a)

instance GrowingAppend (ZMap 1 a)
instance GrowingAppend (ZMap 2 a)
instance GrowingAppend (ZMap 3 a)
instance GrowingAppend (ZMap 4 a)

------------------------------------------------------------------------------
instance SetContainer (ZMap 1 a) where
  type ContainerKey (ZMap 1 a) = V1 Int
  V1     j `member`       ZMap1 zs = j <? zs
  V1     j `notMember`    ZMap1 zs = j >? zs
  ZMap1 ys `union`        ZMap1 zs = ZMap1 $ ys `union`        zs
  ZMap1 ys `difference`   ZMap1 zs = ZMap1 $ ys `difference`   zs
  ZMap1 ys `intersection` ZMap1 zs = ZMap1 $ ys `intersection` zs
  keys (ZMap1 zs) = V1 <$> Map.keys zs
  {-# INLINE member       #-}
  {-# INLINE notMember    #-}
  {-# INLINE union        #-}
  {-# INLINE difference   #-}
  {-# INLINE intersection #-}
  {-# INLINE keys         #-}

instance SetContainer (ZMap 2 a) where
  type ContainerKey (ZMap 2 a) = V2 Int
  member    = (<?)
  notMember = (>?)
  keys (ZMap2 zs) = fst <$> zmap2list zs
  ZMap2 ys `union`        ZMap2 zs = ZMap2 $ ys `zmap2union` zs
  ZMap2 ys `difference`   ZMap2 zs = ZMap2 $ ys `zmap2diff`  zs
  ZMap2 ys `intersection` ZMap2 zs = ZMap2 $ ys `zmap2isect` zs
  {-# INLINE member       #-}
  {-# INLINE notMember    #-}
  {-# INLINE keys         #-}
  {-# INLINE union        #-}
  {-# INLINE difference   #-}
  {-# INLINE intersection #-}

instance SetContainer (ZMap 3 a) where
  type ContainerKey (ZMap 3 a) = V3 Int
  member    = (<?)
  notMember = (>?)
  keys (ZMap3 zs) = fst <$> zmap3list zs
  ZMap3 ys `union`        ZMap3 zs = ZMap3 $ ys `zmap3union` zs
  ZMap3 ys `difference`   ZMap3 zs = ZMap3 $ ys `zmap3diff`  zs
  ZMap3 ys `intersection` ZMap3 zs = ZMap3 $ ys `zmap3isect` zs
  {-# INLINE member       #-}
  {-# INLINE notMember    #-}
  {-# INLINE keys         #-}
  {-# INLINE union        #-}
  {-# INLINE difference   #-}
  {-# INLINE intersection #-}

instance SetContainer (ZMap 4 a) where
  type ContainerKey (ZMap 4 a) = V4 Int
  member    = (<?)
  notMember = (>?)
  keys (ZMap4 zs) = fst <$> zmap4list zs
  ZMap4 ys `union`        ZMap4 zs = ZMap4 $ ys `zmap4union` zs
  ZMap4 ys `difference`   ZMap4 zs = ZMap4 $ ys `zmap4diff`  zs
  ZMap4 ys `intersection` ZMap4 zs = ZMap4 $ ys `zmap4isect` zs
  {-# INLINE member       #-}
  {-# INLINE notMember    #-}
  {-# INLINE keys         #-}
  {-# INLINE union        #-}
  {-# INLINE difference   #-}
  {-# INLINE intersection #-}

------------------------------------------------------------------------------
instance IsMap (ZMap 1 a) where
  type MapValue (ZMap 1 a) = a
  lookup = (<~)
  insertMap k x = ((k, x) ~>)
  deleteMap = (<\)
  singletonMap = curry unit
  mapFromList = flip (~>) `foldl'` bare
  mapToList = flat
--  findWithDefault x =
  insertWith    f (V1 k) x (ZMap1 zs) = ZMap1 $ Map.insertWith f k x zs
  insertWithKey f (V1 k) x (ZMap1 zs) =
    ZMap1 $ Map.insertWithKey (f . V1) k x zs
  adjustMap     f (V1 k) (ZMap1 zs) = ZMap1 $ Map.adjust f k zs
  adjustWithKey f (V1 k) (ZMap1 zs) = ZMap1 $ Map.adjustWithKey (f . V1) k zs
  updateMap     f (V1 k) (ZMap1 zs) = ZMap1 $ Map.update f k zs
  updateWithKey f (V1 k) (ZMap1 zs) = ZMap1 $ Map.updateWithKey (f . V1) k zs
  updateLookupWithKey f (V1 k) (ZMap1 zs) =
    let (m, zs') = Map.updateLookupWithKey (f . V1) k zs
    in  (m, ZMap1 zs')
  alterMap f (V1 k) (ZMap1 zs) = ZMap1 $ Map.alter f k zs
  unionWith f (ZMap1 ys) (ZMap1 zs) = ZMap1 $ Map.unionWith f ys zs
--   unionWithKey = undefined
--   unionsWith = undefined
  mapWithKey f (ZMap1 zs) = ZMap1 $ Map.mapWithKey (f . V1) zs
  omapKeysWith f g (ZMap1 zs) = ZMap1 $ Map.mapKeysWith f ((^._x) . g . V1) zs
  filterMap f (ZMap1 zs) = ZMap1 $ Map.filter f zs
  {-# INLINE lookup    #-}
  {-# INLINE insertMap #-}
  {-# INLINE deleteMap #-}
  {-# INLINE alterMap  #-}
  {-# INLINE filterMap #-}

instance IsMap (ZMap 2 a) where
  type MapValue (ZMap 2 a) = a
  lookup = (<~)
  insertMap k x = ((k, x) ~>)
  deleteMap = (<\)
  singletonMap = curry unit
  mapFromList = flip (~>) `foldl'` bare
  mapToList = flat
--  findWithDefault x =
  insertWith    f k x = alterMap (Just . maybe x (f   x)) k
  insertWithKey f k x = alterMap (Just . maybe x (f k x)) k
  adjustMap     f   = alterMap (fmap f)
  adjustWithKey f k = alterMap (fmap $ f k) k
  updateMap     f   = alterMap (>>= f)
  updateWithKey f k = alterMap (>>= f k) k
--   updateLookupWithKey = undefined
  alterMap f (V2 j k) (ZMap2 zs) =
    let fn = maybe (unit . (k,) <$> f Nothing) (Just . Map.alter f k)
    in  ZMap2 $ Map.alter fn j zs
--     ZMap2 $ Map.alter (Map.alter f k <$>) j zs
--   unionWith f (ZMap2 ys) (ZMap2 zs) = ZMap2 $ Map.unionWith f ys zs
--   unionWithKey = undefined
--   unionsWith = undefined
--   mapWithKey f (ZMap2 zs) = ZMap2 $ Map.mapWithKey (f . V2) zs
--   omapKeysWith f g (ZMap2 zs) = ZMap2 $ Map.mapKeysWith f ((^._x) . g . V2) zs
  filterMap f (ZMap2 zs) = ZMap2 . clean $ Map.filter f <$> zs
  {-# INLINE lookup    #-}
  {-# INLINE insertMap #-}
  {-# INLINE deleteMap #-}
  {-# INLINE alterMap  #-}
  {-# INLINE filterMap #-}

instance IsMap (ZMap 3 a) where
  type MapValue (ZMap 3 a) = a
  lookup = (<~)
  insertMap k x = ((k, x) ~>)
  deleteMap = (<\)
  singletonMap = curry unit
  mapFromList = flip (~>) `foldl'` bare
  mapToList = flat
  insertWith    f k x = alterMap (Just . maybe x (f   x)) k
  insertWithKey f k x = alterMap (Just . maybe x (f k x)) k
  adjustMap     f   = alterMap (fmap f)
  adjustWithKey f k = alterMap (fmap $ f k) k
  updateMap     f   = alterMap (>>= f)
  updateWithKey f k = alterMap (>>= f k) k
  alterMap f (V3 i j k) (ZMap3 zs) =
    let fn = maybe (unit . (k',) <$> f Nothing) (Just . alterMap f k' . ZMap2)
        sc (ZMap2 xs) = xs
        k' = V2 j k
    in  ZMap3 $ Map.alter (fmap sc . fn) i zs
--     ZMap3 $ Map.alter (Map.alter (Map.alter f k <$>) j <$>) i zs
  filterMap f (ZMap3 zs) = ZMap3 . clean $ clean . (Map.filter f <$>) <$> zs
  {-# INLINE lookup    #-}
  {-# INLINE insertMap #-}
  {-# INLINE deleteMap #-}
  {-# INLINE alterMap  #-}
  {-# INLINE filterMap #-}

instance IsMap (ZMap 4 a) where
  type MapValue (ZMap 4 a) = a
  lookup = (<~)
  insertMap k x = ((k, x) ~>)
  deleteMap = (<\)
  singletonMap = curry unit
  mapFromList = flip (~>) `foldl'` bare
  mapToList = flat
  insertWith    f k x = alterMap (Just . maybe x (f   x)) k
  insertWithKey f k x = alterMap (Just . maybe x (f k x)) k
  adjustMap     f   = alterMap (fmap f)
  adjustWithKey f k = alterMap (fmap $ f k) k
  updateMap     f   = alterMap (>>= f)
  updateWithKey f k = alterMap (>>= f k) k
  alterMap f (V4 i j k l) (ZMap4 zs) =
    let fn = maybe (unit . (k',) <$> f Nothing) (Just . alterMap f k' . ZMap3)
        sc (ZMap3 xs) = xs
        k' = V3 j k l
    in  ZMap4 $ Map.alter (fmap sc . fn) i zs
--     ZMap4 $ Map.alter (Map.alter (Map.alter (Map.alter f l <$>) k <$>) j <$>) i zs
  filterMap f (ZMap4 zs) =
    ZMap4 . clean $ clean . (clean . (Map.filter f <$>) <$>) <$> zs

clean :: IntMap (IntMap a) -> IntMap (IntMap a)
clean  = Map.filter (not . Map.null)
{-# INLINE clean #-}

-- ** Strictness instances
------------------------------------------------------------------------------
instance NFData a => NFData (ZMap 1 a)
instance NFData a => NFData (ZMap 2 a)
instance NFData a => NFData (ZMap 3 a)
instance NFData a => NFData (ZMap 4 a)

-- ** @Map@-like instances
------------------------------------------------------------------------------
instance IsList (ZMap 1 a) where
  type Item (ZMap 1 a) = (V1 Int, a)
  fromList    = flip (~>) `foldl'` bare
  fromListN _ = fromList
  toList      = flat
  {-# INLINE[2] fromList  #-}
  {-# INLINE[2] fromListN #-}
  {-# INLINE[2] toList    #-}

--   TODO: can I make this more generic?
instance IsList (ZMap 2 a) where
  type Item (ZMap 2 a) = (V2 Int, a)
  fromList    = flip (~>) `foldl'` bare
  fromListN _ = fromList
  toList      = flat
  {-# INLINE[2] fromList  #-}
  {-# INLINE[2] fromListN #-}
  {-# INLINE[2] toList    #-}

instance IsList (ZMap 3 a) where
  type Item (ZMap 3 a) = (V3 Int, a)
  fromList    = flip (~>) `foldl'` bare
  fromListN _ = fromList
  toList      = flat
  {-# INLINE[2] fromList  #-}
  {-# INLINE[2] fromListN #-}
  {-# INLINE[2] toList    #-}

instance IsList (ZMap 4 a) where
  type Item (ZMap 4 a) = (V4 Int, a)
  fromList    = flip (~>) `foldl'` bare
  fromListN _ = fromList
  toList      = flat
  {-# INLINE[2] fromList  #-}
  {-# INLINE[2] fromListN #-}
  {-# INLINE[2] toList    #-}

-- ** Set-like instances
------------------------------------------------------------------------------
-- | Insert elements into ZMaps
instance SetInsert (V1 Int, a) (ZMap 1 a) where
  (V1 p, x) ~> ZMap1 ps = ZMap1 $ (p, x) ~> ps
  {-# INLINE[2] (~>) #-}

instance SetSize (ZMap 1 a) where
  card (ZMap1 ps) = Map.size ps
  dry  (ZMap1 ps) = Map.null ps
  {-# INLINE card #-}
  {-# INLINE dry  #-}

-- | Find/delete from ZMaps
instance SetIndex (V1 Int) (ZMap 1 a) where
  V1 p <? ZMap1 ps = p `Map.member` ps
  V1 p <\ ZMap1 ps = ZMap1 $ p `Map.delete` ps
  {-# INLINE[2] (<?) #-}
  {-# INLINE[2] (<\) #-}

-- | Construct initial ZMaps
instance SetBuild (V1 Int, a) (ZMap 1 a) where
  bare = ZMap1 Map.empty
  unit = ZMap1 . uncurry Map.singleton . first (^._x)
  {-# INLINE[2] bare #-}
  {-# INLINE[2] unit #-}

-- | Fetch from ZMaps
instance SetElems (V1 Int) (Maybe a) (ZMap 1 a) where
  V1 p <~ ZMap1 cs = p `Map.lookup` cs
  {-# INLINE[2] (<~) #-}

-- | Flatten into a coordinate-list
instance SetFlat (V1 Int, a) (ZMap 1 a) where
  flat (ZMap1 cs) = first V1 `map` Map.toList cs
  {-# INLINE[2] flat #-}

------------------------------------------------------------------------------
-- | Insert elements into ZMaps
instance SetInsert (V2 Int, a) (ZMap 2 a) where
  (p, x) ~> ZMap2 ps = ZMap2 $ ins2d x p ps
  {-# INLINE[2] (~>) #-}

instance SetSize (ZMap 2 a) where
  card (ZMap2 ps) = foldl' (+) 0 $ fmap card ps
  dry  (ZMap2 ps) = or           $ fmap dry  ps
  {-# INLINE card #-}
  {-# INLINE dry  #-}

-- | Find/delete from ZMaps
instance SetIndex (V2 Int) (ZMap 2 a) where
  p <? ZMap2 ps = loc2d p ps
  p <\ ZMap2 ps = ZMap2 $ del2d p ps
  {-# INLINE[2] (<?) #-}
  {-# INLINE[2] (<\) #-}

-- | Construct initial ZMaps
instance SetBuild (V2 Int, a) (ZMap 2 a) where
  bare = ZMap2 bare
  unit = (~> bare)
  {-# INLINE[2] bare #-}
  {-# INLINE[2] unit #-}

-- | Fetch from ZMaps
instance SetElems (V2 Int) (Maybe a) (ZMap 2 a) where
  p <~ ZMap2 cs = p `get2d` cs
  {-# INLINE[2] (<~) #-}

-- | Flatten into a coordinate-list
instance SetFlat (V2 Int, a) (ZMap 2 a) where
  flat (ZMap2 cs) = zmap2list cs
  {-# INLINE[2] flat #-}

------------------------------------------------------------------------------
-- | Insert elements into ZMaps
instance SetInsert (V3 Int, a) (ZMap 3 a) where
  (p, x) ~> ZMap3 ps = ZMap3 $ ins3d x p ps
  {-# INLINE[2] (~>) #-}

instance SetSize (ZMap 3 a) where
  card (ZMap3 ps) = foldl' (+) 0 $ card . ZMap2 <$> ps
  dry  (ZMap3 ps) = or           $ dry  . ZMap2 <$> ps
  {-# INLINE card #-}
  {-# INLINE dry  #-}

-- | Find/delete from ZMaps
instance SetIndex (V3 Int) (ZMap 3 a) where
  p <? ZMap3 ps = loc3d p ps
  p <\ ZMap3 ps = ZMap3 $ del3d p ps
  {-# INLINE[2] (<?) #-}
  {-# INLINE[2] (<\) #-}

-- | Construct initial ZMaps
instance SetBuild (V3 Int, a) (ZMap 3 a) where
  bare = ZMap3 bare
  unit = (~> bare)
  {-# INLINE[2] bare #-}
  {-# INLINE[2] unit #-}

-- | Fetch from ZMaps
instance SetElems (V3 Int) (Maybe a) (ZMap 3 a) where
  p <~ ZMap3 cs = p `get3d` cs
  {-# INLINE[2] (<~) #-}

-- | Flatten into a coordinate-list
instance SetFlat (V3 Int, a) (ZMap 3 a) where
  flat (ZMap3 cs) = zmap3list cs
  {-# INLINE[2] flat #-}

------------------------------------------------------------------------------
-- | Insert elements into ZMaps
instance SetInsert (V4 Int, a) (ZMap 4 a) where
  (p, x) ~> ZMap4 ps = ZMap4 $ ins4d x p ps
  {-# INLINE[2] (~>) #-}

-- | Find/delete from ZMaps
instance SetIndex (V4 Int) (ZMap 4 a) where
  p <? ZMap4 ps = loc4d p ps
  p <\ ZMap4 ps = ZMap4 $ del4d p ps
  {-# INLINE[2] (<?) #-}
  {-# INLINE[2] (<\) #-}

instance SetSize (ZMap 4 a) where
  card (ZMap4 ps) = foldl' (+) 0 $ card . ZMap3 <$> ps
  dry  (ZMap4 ps) = or           $ dry  . ZMap3 <$> ps
  {-# INLINE card #-}
  {-# INLINE dry  #-}

-- | Construct initial ZMaps
instance SetBuild (V4 Int, a) (ZMap 4 a) where
  bare = ZMap4 bare
  unit = (~> bare)
  {-# INLINE[2] bare #-}
  {-# INLINE[2] unit #-}

-- | Fetch from ZMaps
instance SetElems (V4 Int) (Maybe a) (ZMap 4 a) where
  p <~ ZMap4 cs = p `get4d` cs
  {-# INLINE[2] (<~) #-}

-- | Flatten into a coordinate-list
instance SetFlat (V4 Int, a) (ZMap 4 a) where
  flat (ZMap4 cs) = zmap4list cs
  {-# INLINE[2] flat #-}

-- ** Instances for conversions and serialisation
------------------------------------------------------------------------------
instance ToJSON   a => ToJSON   (ZMap 1 a)
instance ToJSON   a => ToJSON   (ZMap 2 a)
instance ToJSON   a => ToJSON   (ZMap 3 a)
instance ToJSON   a => ToJSON   (ZMap 4 a)

instance FromJSON a => FromJSON (ZMap 1 a)
instance FromJSON a => FromJSON (ZMap 2 a)
instance FromJSON a => FromJSON (ZMap 3 a)
instance FromJSON a => FromJSON (ZMap 4 a)


-- * Low-level, `ZMap d` functions
------------------------------------------------------------------------------
ins4d :: a -> V4 Int -> IntMap (IntMap (IntMap (IntMap a))) ->
  IntMap (IntMap (IntMap (IntMap a)))
ins4d x (V4 a b c d) es =
  let go = (~>es) . (a,) . ins3d x (V3 b c d) . maybe bare id
  in  go $ a <~ es

-- | Insert an XYZ node-coordinate into the node-ID map (where Z is the tree
--   depth, of the node).
ins3d :: a -> V3 Int -> IntMap (IntMap (IntMap a)) ->
  IntMap (IntMap (IntMap a))
ins3d x (V3 a b c) cs =
  let ys = maybe bare id (a<~cs)
  in  (a,(b,(c,x)~>maybe bare id (b<~ys)) ~> ys) ~> cs

ins2d :: a -> V2 Int -> IntMap (IntMap a) -> IntMap (IntMap a)
ins2d x (V2 a b) cs = (a,(b,x)~>maybe bare id (a<~cs)) ~> cs

------------------------------------------------------------------------------
-- | Delete the given entry, if it exists.
del4d :: V4 Int -> IntMap (IntMap (IntMap (IntMap a))) ->
         IntMap (IntMap (IntMap (IntMap a)))
del4d (V4 a b c d) es = maybe es ((~>es) . (a,) . del3d (V3 b c d)) $ a <~ es

del3d :: V3 Int -> IntMap (IntMap (IntMap a)) -> IntMap (IntMap (IntMap a))
del3d (V3 a b c) ps = maybe ps ((~>ps) . (a,) . del2d (V2 b c)) $ a <~ ps

del2d :: V2 Int -> IntMap (IntMap a) -> IntMap (IntMap a)
del2d (V2 b c) qs = maybe qs ((~>qs) . (b,) . (c<\)) $ b <~ qs
{-# INLINE[2] del2d #-}

------------------------------------------------------------------------------
-- | Can the given coordinate be found, within the given node-ID map?
loc4d :: V4 Int -> IntMap (IntMap (IntMap (IntMap a))) -> Bool
loc4d (V4 a b c d) es = maybe False (d<?) $ maybe Nothing (c<~) $ maybe Nothing (b<~) (a<~es)

loc3d :: V3 Int -> IntMap (IntMap (IntMap a)) -> Bool
loc3d (V3 a b c) cs = maybe False (c<?) $ maybe Nothing (b<~) (a<~cs)

loc2d :: V2 Int -> IntMap (IntMap a) -> Bool
loc2d (V2 a b) cs = maybe False (b<?) $ a<~cs

-- | Can the given coordinate be found, within the given node-ID map?
get4d :: V4 Int -> IntMap (IntMap (IntMap (IntMap a))) -> Maybe a
get4d (V4 a b c d) es = maybe Nothing (d<~) $ maybe Nothing (c<~) $ maybe Nothing (b<~) (a<~es)

get3d :: V3 Int -> IntMap (IntMap (IntMap a)) -> Maybe a
get3d (V3 a b c) cs = maybe Nothing (c<~) $ maybe Nothing (b<~) (a<~cs)

get2d :: V2 Int -> IntMap (IntMap a) -> Maybe a
get2d (V2 a b) cs = maybe Nothing (b<~) (a<~cs)

------------------------------------------------------------------------------
-- | Apply @f@ to every element of the @PosMap@.
map4d :: (a -> b) -> IntMap (IntMap (IntMap (IntMap a))) ->
         IntMap (IntMap (IntMap (IntMap b)))
map4d f = Map.map (Map.map (Map.map (Map.map f)))

map3d :: (a -> b) -> IntMap (IntMap (IntMap a)) -> IntMap (IntMap (IntMap b))
map3d f = Map.map (Map.map (Map.map f))

map2d :: (a -> b) -> IntMap (IntMap a) -> IntMap (IntMap b)
map2d f = Map.map (Map.map f)

-- ** Constructor helpers
------------------------------------------------------------------------------
zmap4list :: IntMap (IntMap (IntMap (IntMap a))) -> [(V4 Int, a)]
zmap4list  = concat . map f . flat . Map.map zmap3list
  where
    f       (w, xyza) = map (g w) xyza
    g w (V3 x y z, a) = (V4 w x y z, a)

zmap3list :: IntMap (IntMap (IntMap a)) -> [(V3 Int, a)]
zmap3list  = concat . concat . map f . flat
  where
    f   (x, yza) = map (g x) $ flat yza
    g x  (y, za) = map (h x y) $ flat za
    h x y (z, a) = (V3 x y z, a)

zmap2list :: IntMap (IntMap a) -> [(V2 Int, a)]
zmap2list  = concat . map f . flat
  where
    f  (x, ya) = map (g x) $ flat ya
    g x (y, a) = (V2 x y, a)

------------------------------------------------------------------------------
zmap4union :: IntMap (IntMap (IntMap (IntMap a))) ->
              IntMap (IntMap (IntMap (IntMap a))) ->
              IntMap (IntMap (IntMap (IntMap a)))
zmap4union  = Map.unionWith zmap3union
{-# INLINE zmap4union #-}

zmap3union :: IntMap (IntMap (IntMap a)) -> IntMap (IntMap (IntMap a)) ->
              IntMap (IntMap (IntMap a))
zmap3union  = Map.unionWith zmap2union
{-# INLINE zmap3union #-}

zmap2union :: IntMap (IntMap a) -> IntMap (IntMap a) -> IntMap (IntMap a)
zmap2union  = Map.unionWith Map.union
{-# INLINE zmap2union #-}

------------------------------------------------------------------------------
zmap4diff :: IntMap (IntMap (IntMap (IntMap a))) ->
             IntMap (IntMap (IntMap (IntMap a))) ->
             IntMap (IntMap (IntMap (IntMap a)))
zmap4diff  = Map.differenceWith ((Just .) . zmap3diff)
{-# INLINE zmap4diff #-}

zmap3diff :: IntMap (IntMap (IntMap a)) -> IntMap (IntMap (IntMap a)) ->
             IntMap (IntMap (IntMap a))
zmap3diff  = Map.differenceWith ((Just .) . zmap2diff)
{-# INLINE zmap3diff #-}

zmap2diff :: IntMap (IntMap a) -> IntMap (IntMap a) -> IntMap (IntMap a)
zmap2diff  = Map.differenceWith ((Just .) . Map.difference)
{-# INLINE zmap2diff #-}

------------------------------------------------------------------------------
zmap4isect :: IntMap (IntMap (IntMap (IntMap a))) ->
              IntMap (IntMap (IntMap (IntMap a))) ->
              IntMap (IntMap (IntMap (IntMap a)))
zmap4isect  = Map.intersectionWith zmap3isect
{-# INLINE zmap4isect #-}

zmap3isect :: IntMap (IntMap (IntMap a)) -> IntMap (IntMap (IntMap a)) ->
              IntMap (IntMap (IntMap a))
zmap3isect  = Map.intersectionWith zmap2isect
{-# INLINE zmap3isect #-}

zmap2isect :: IntMap (IntMap a) -> IntMap (IntMap a) -> IntMap (IntMap a)
zmap2isect  = Map.intersectionWith Map.intersection
{-# INLINE zmap2isect #-}
