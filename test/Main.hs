{-# LANGUAGE DataKinds #-}
module Main where

-- import GHC.TypeNats
import Data.List (intersect)
import Data.Maybe (catMaybes)
import Test.Framework (defaultMain, testGroup)
import Test.Framework.Providers.QuickCheck2 (testProperty)
import Test.QuickCheck
import Data.Index.Arbitrary
import Linear


-- * Parent/Child tests
------------------------------------------------------------------------------
prop_chpa :: Key2 -> Bool
prop_chpa (Key2 c) = (==1) . length $ filter (==k) cs where
  cs = progeny p
  k  = Key c :: Key 2
  Just p = parent k

prop_pach :: Key2 -> Bool
prop_pach (Key2 p) = go k where
  go = all (==k) . catMaybes . map parent . progeny
  k  = Key p :: Key 2

prop_line :: Key2 -> Bool
prop_line (Key2 p) = let ls = lineage $ Key p
                         r  = reify (lroot :: V3 Int)
                         go q | q' == r = pure q'
                              | otherwise = q':go q'
                           where Just q' = parent q
                     in  reverse (go (Key p :: Key 2)) == ls

parentChild =
  [ testProperty "children . parent   " prop_chpa
  , testProperty "parent . children   " prop_pach
  , testProperty "lineage == parents  " prop_line
  ]


-- * Path/Coordinate tests
------------------------------------------------------------------------------
prop_plen :: Key2 -> Bool
prop_plen (Key2 p) = let V3 _ _ d = lpos2 p in d == plen2 p

prop_ppos :: Key2 -> Bool
prop_ppos (Key2 p) = k == reify (lpath k :: V3 Int) where
  k = Key p :: Key 2

prop_nbrs :: Key2 -> Bool
prop_nbrs (Key2 p) = go k where
  go = (==4) . length . intersect sq . neighbourhood
  k  = Key p :: Key 2
  Just sq = progeny <$> parent k

pathsCoords =
  [ testProperty "plen                " prop_plen
  , testProperty "reify . lpath       " prop_ppos
  , testProperty "nbrs/\\progeny.parent" prop_ppos
  ]

main :: IO ()
main  =
  defaultMain [ testGroup "Parent/Child" parentChild
              , testGroup "Paths/Coords" pathsCoords
              ]
