{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Data.Index.Arbitrary
  ( module Data.Index.ZCurve
  , module Data.Index.Arbitrary
  , module Data.Tree.Linear.Coords
  ) where

import GHC.Word
import Data.Bits.Handy
import Data.Index.ZCurve
import Test.QuickCheck hiding ((.&.))
import Data.Tree.Linear.Coords
import Linear (V3 (..))
import Text.Printf

newtype Key2 = Key2 { _unKey2 :: Int }
  deriving (Eq, Ord, Enum, Bounded)

instance Show Key2 where
  showsPrec n (Key2 p) = showParen (n>=11) $ showString "Key2 " .
    showString (printf "0x%016x " (w64 p)) . showsPrec 11 (lpos2 p)

instance Arbitrary Key2 where
  arbitrary = do
    d <- max 1 . (.&. 7) <$> arbitrary
    x <- (<<%(maxDepth2-d)) . (.&. (1<<%d)) <$> arbitrary
    y <- (<<%(maxDepth2-d)) . (.&. (1<<%d)) <$> arbitrary
    return $ Key2 $ tkey2 $ V3 x y d

w64 :: Int -> Word
w64  = fromIntegral
