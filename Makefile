# Source Markdown files:
MD	:= $(wildcard *.md) $(wildcard rubs-base/doc/*.md)

SVG	:= $(wildcard doc/images/*.svg)
PDF	:= $(MD:.md=.pdf)
PIC	:= $(SVG:.svg=.pdf)

# Pandoc settings:
REF	:= rubs-base/tex/rubrefs.bib
TMP	?= rubs-base/tex/_layout.latex
FLT	?= --filter=pandoc-include --filter=pandoc-fignos --filter=pandoc-citeproc --filter=pandoc-filter-graphviz
OPT	?= --number-sections --bibliography=$(REF)

.PHONY:	all out doc test clean

all:	$(PDF) $(PIC) doc

out:
	+make -C out all

doc:
	+make -C doc all

test:
	cabal new-build -j tree-test
	dist-newstyle/build/x86_64-linux/ghc-8.6.4/tree-linear-0.1.0.0/t/tree-test/build/tree-test/tree-test

clean:
	+rm -f $(PDF) $(PIC)
	+make -C doc clean
	+make -C out clean
	+find . -iname '*~' -delete


# Implicit rules:
%.pdf: %.md $(PIC) out
	+pandoc -s --template=$(TMP) $(FLT) $(OPT) -f markdown -t latex -V papersize:a4 $< -o $@

%.pdf: %.svg
	+inkscape --export-area-drawing --export-text-to-path --export-pdf=$@ $<
